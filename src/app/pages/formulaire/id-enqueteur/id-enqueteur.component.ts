import { FormControl } from '@angular/forms';
import { Component, HostListener, Input, OnInit } from '@angular/core';
//import { map, startWith } from 'rxjs/operators';
import { UserDataModel } from 'src/app/Models/DataModels';
import { getUserStorageJson } from 'src/app/Services/ApiServices/storage.services';
//import { testDataValidity } from 'src/app/Services/ApiServices/Comment.sercice';
import {
  getScreenSize,
  getScreenValue,
} from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-id-enqueteur',
  templateUrl: './id-enqueteur.component.html',
  styleUrls: ['./id-enqueteur.component.css'],
})
export class IdEnqueteurComponent implements OnInit {
  screen = { x: 0, y: 0 };
  bpObs = 'L';

  @Input() formData: any;
  @Input() disableNext?: (value: boolean) => void;
  observer?: IntersectionObserver;
  user_data: UserDataModel | null | undefined = getUserStorageJson().user_data;
  myControl = new FormControl();
  minDate?: Date;
  maxDate?: Date;
  startDateSelectDisabled: boolean = false;
  startDateSelectSpinnerShow: boolean = true;
  autheDateSelectOption?: {
    showSeconds?: boolean;
    touchUi?: boolean;
    enableMeridian?: boolean;
    disableMinute?: boolean;
    hideTime?: boolean;
    stepHour?: number;
    stepMinute?: number;
    stepSecond?: number;
  };
  starteDate?: Date;
  inputStartData?: Date;

  constructor() {}

  ngOnInit(): void {
    this.intiScreendata();
    this.autheDateSelectOption = {
      showSeconds: false,
      touchUi: true,
      enableMeridian: true,
      disableMinute: false,
      hideTime: false,
      stepHour: 1,
      stepMinute: 5,
      stepSecond: 10,
    };

    if (this.user_data) {
      this.formData.name = this.user_data.name;
      this.formData.prename = this.user_data.prename;
      this.formData.cin = this.user_data.cin;
      this.formData.id = this.user_data.id;
      if (this.user_data.superviseur) {
        this.formData.sup_name = this.user_data.superviseur.name;
        this.formData.sup_prename = this.user_data.superviseur.prename;
      }
    }
    console.log("main selector ===> ", document.querySelector('#stepcontainer-selector'))
    this.observer = new IntersectionObserver(
      (entries, _observer) => {
          entries.forEach(entry => {
            console.log('Element is fully visible in screen', entry);
            if(entry.isIntersecting === true) {
              if(this.disableNext) this.disableNext(true);
            }else{
              if(this.disableNext) this.disableNext(false);
            }
          });
        },
        { threshold: 1.0 , root:document.querySelector("#stepcontainer-selector") }
      );

    this.dataChange();
  }

  ngAfterViewInit(): void {
    console.log('Element is fully visible in screen');
    const monitelement = document.querySelector('#id-last-element-for-next');
    if (monitelement && this.observer) {
      this.observer.observe(monitelement);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    const monitelement = document.querySelector('#id-last-element-for-next');
    if (monitelement && this.observer) {
      this.observer.unobserve(monitelement);
    }
  }

  dataChange() {
    // const is_valid = testDataValidity(this.formData);
    // if (is_valid) this.disableNext = false;
    // else this.disableNext = true;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.intiScreendata();
  }

  intiScreendata() {
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
    //console.log(this.bpObs, this.screen)
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdEnqueteurComponent } from './id-enqueteur.component';

describe('IdEnqueteurComponent', () => {
  let component: IdEnqueteurComponent;
  let fixture: ComponentFixture<IdEnqueteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdEnqueteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdEnqueteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

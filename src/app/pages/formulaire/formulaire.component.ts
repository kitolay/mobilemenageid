/* eslint-disable max-len */
import PouchDB from 'pouchdb';
import { testConnextion } from './../../Services/ApiServices/sync.services';
/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
import { ToastrService } from 'ngx-toastr';
import {
  HostListener,
  Component,
  OnInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { saveSurveyData } from 'src/app/Services/ApiServices/form.services';
import { ALL_TEXT, ROUT_PATH } from 'src/app/Services/locals';
import { Router } from '@angular/router';
import {
  getScreenSize,
  getScreenValue,
} from 'src/app/Services/ApiServices/screen.service';
import { off_db_name } from 'src/app/Services/offline/card.services';
//import { NguCarouselConfig } from '@ngu/carousel';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css'],
})
export class FormulaireComponent implements OnInit {
  @ViewChild('stepcontainer') stepcontainer?: ElementRef;
  @ViewChild('stepcontainerMob') stepContainerMob?: ElementRef;
  all_text: any = ALL_TEXT;
  curentIndex = 1;
  disableNext = false;
  formdata: any = all_formulaire_data;
  savingData = false;
  bpObs = 'L';
  step_list = stepList;
  screen = { x: 0, y: 0 };
  observer?: IntersectionObserver;

  constructor(private toast: ToastrService, public router: Router) {}

  ngOnInit(): void {
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
  }

  handleNext(): void {
    this.curentIndex++;
    if (this.stepcontainer && this.stepcontainer.nativeElement) {
      this.stepcontainer.nativeElement.scrollTop = 0;
    }
    if (this.stepContainerMob && this.stepContainerMob.nativeElement) {
      this.stepContainerMob.nativeElement.scrollTop = 0;
    }
  }

  handlePreviouse(): void {
    this.curentIndex--;
    if (this.stepcontainer && this.stepcontainer.nativeElement) {
      this.stepcontainer.nativeElement.scrollTop = 0;
    }
    if (this.stepContainerMob && this.stepContainerMob.nativeElement) {
      this.stepContainerMob.nativeElement.scrollTop = 0;
    }
  }

  handdleScrool = (state: boolean) => {
    this.disableNext = !state;
  };

  handleSetPage = (idex: number) => {
    //console.log('change curent indexd');
    this.curentIndex = idex;
    if (this.stepcontainer && this.stepcontainer.nativeElement) {
      this.stepcontainer.nativeElement.scrollTop = 0;
    }
    if (this.stepContainerMob && this.stepContainerMob.nativeElement) {
      this.stepContainerMob.nativeElement.scrollTop = 0;
    }
  };

  handleSaveData(): void {
    this.savingData = true;
    testConnextion().then((conn) => {
      if (conn.state) {
        saveSurveyData(all_formulaire_data)
          .then((rep) => {
            if (rep.error) {
              this.toast.error(rep.message);
            } else if (rep && !rep.error) {
              const card_data = { ...rep };
              delete card_data.details;
              const base_detail = { ...rep.details };
              delete base_detail.id1_data;
              const id1Data =
                rep.details.id1_data && rep.details.id1_data.length > 0
                  ? [...rep.details.id1_data]
                  : null;
              const all_data = { ...base_detail, card_item: card_data };
              const table_name_kay = Object.keys(all_data);
              table_name_kay.forEach((tName, idx) => {
                const table_data = new PouchDB(tName);
                table_data.bulkDocs([all_data[tName]]);
                if (idx >= table_name_kay.length - 1) {
                  if (id1Data) {
                    const table_id = new PouchDB('id1_data');
                    table_id.bulkDocs(id1Data);
                  }
                  this.savingData = false;
                  this.toast.success('Menage Ajouter!');
                  this.router.navigate([ROUT_PATH.listes]);
                }
              });
            } else {
              this.toast.error(rep as any);
            }
            this.savingData = false;
          })
          .catch((err) => {
            this.savingData = false;
            this.toast.error(err);
          });
      } else {
        const all_formulaire_data_to_sate = all_formulaire_data;
        const PMT = Math.floor(Math.random() * 97 + 1) / 100;
        const card_item = {
          nom: all_formulaire_data.id_data.nom,
          prenom: all_formulaire_data.id_data.prenom,
          nis: all_formulaire_data.id_data.cin,
          cdf: all_formulaire_data.id_data.cdfm ? 1 : 0,
          adress:
            (all_formulaire_data.ig_data.lotis ?? '') +
            '/' +
            (all_formulaire_data.ig_data.fokontany
              ? all_formulaire_data.ig_data.fokontany.lib
              : '') +
            '/' +
            (all_formulaire_data.ig_data.commune
              ? all_formulaire_data.ig_data.commune.lib
              : '') +
            '/' +
            (all_formulaire_data.ig_data.district
              ? all_formulaire_data.ig_data.district.lib
              : '') +
            '/' +
            (all_formulaire_data.ig_data.region
              ? all_formulaire_data.ig_data.region.lib
              : '') +
            '/' +
            (all_formulaire_data.ig_data.province
              ? all_formulaire_data.ig_data.province.lib
              : ''),
          pict: all_formulaire_data.id_data.pict,
          coderef: PMT + '-' + all_formulaire_data.id_data.cin,
          barref: PMT + '-' + all_formulaire_data.id_data.cin,
          offline: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        };
        const backup_db = new PouchDB(off_db_name);
        backup_db
          .put({
            ...all_formulaire_data_to_sate,
            _id: card_item.coderef + '-' + card_item.adress,
            card_item,
          })
          .then((res) => {
            // backup_db
            // .putAttachment(
            //   all_formulaire_data.id_data.nom +
            //     all_formulaire_data.id_data.prenom,
            //   all_formulaire_data.id_data.pict,
            //   all_formulaire_data.id_data.pict,
            //   'image/png'
            // )
            // .then(() => {
            //   this.savingData = false;
            //   this.toast.success('Menage Ajouter!');
            //   this.router.navigate([ROUT_PATH.listes]);
            // })
            // .catch((err) => {
            //   console.log(err);
            //   this.toast.error(err);
            //   this.savingData = false;
            // });
            console.log('add-result ===>', res);
            this.savingData = false;
            this.toast.success('Menage Ajouter!');
            this.router.navigate([ROUT_PATH.listes]);
          })
          .catch((err) => {
            console.log(err);
            this.toast.error(err);
            this.savingData = false;
          });
      }
    });
  }

  getbuttonStatue() {
    return this.disableNext;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
  }
}

var all_formulaire_data = {
  enqueteur_data: {
    name: undefined,
    prename: undefined,
    cin: undefined,
    id: undefined,
    sup_name: undefined,
    sup_prename: undefined,
    start_date: new Date(),
  },
  ig_data: {
    lotis: undefined,
    province: undefined,
    region: undefined,
    district: undefined,
    commune: undefined,
    fokontany: undefined,
    milieu: undefined,
    numero: undefined,
  },
  id_data: {
    cdfm: true,
    pict: undefined,
    nom: undefined,
    prenom: undefined,
    surnom: undefined,
    cin: undefined,
    datedelcin: undefined,
    cincon: undefined,
    datedelcincon: undefined,
  },
  ch_data: {
    housetype: undefined,
    extwall: { au: false, lib: undefined },
    plaf: { au: false, lib: undefined },
    npiech: undefined,
    npiecedor: undefined,
    piecsurf: undefined,
    p1: undefined,
    p2: undefined,
    p3: undefined,
    p4: undefined,
    plancer: { au: false, lib: undefined },
    elect: { au: false, lib: undefined },
    eau: { au: false, lib: undefined },
    toilet: { au: false, lib: undefined },
  },
  ap_data: {
    'Chaise (avec pied et dossier)': { state: false, qte: undefined },
    'Table (avec pied, bien droit)': { state: false, qte: undefined },
    Natte: { state: false, qte: undefined },
    'Lampe pétrole': { state: false, qte: undefined },
    'Poste radio': { state: false, qte: undefined },
    'Radio avec cassette, cd, mp3..': { state: false, qte: undefined },
    Téléviseur: { state: false, qte: undefined },
    'Lecteur CD DVD': { state: false, qte: undefined },
    Bicyclette: { state: false, qte: undefined },
    Puits: { state: false, qte: undefined },
    Irrigation: { state: false, qte: undefined },
    'Téléphone portable': { state: false, qte: undefined },
    'Charrue (traction animale)': { state: false, qte: undefined },
    'Charrette (traction animale)': { state: false, qte: undefined },
    'Herse (traction animale)': { state: false, qte: undefined },
    Bèche: { state: false, qte: undefined },
    Stockage: { state: false, qte: undefined },
    'Champ ou rizière (non métayage)': { state: false, qte: undefined },
    Zébu: { state: false, qte: undefined },
    Porc: { state: false, qte: undefined },
    'Chèvre/mouton': { state: false, qte: undefined },
    'Poulet gasy': { state: false, qte: undefined },
    Rûche: { state: false, qte: undefined },
    'Matériel de pêche: pirogue, filet': { state: false, qte: undefined },
  },
  srpt_data: {
    sr: {
      'Salaire de la fonction publique': undefined,
      'Salaire du secteur privé': undefined,
      'Autre emploi': undefined,
      'Revenus agricoles': undefined,
      'Revenus d\'artisanat': undefined,
      'Revenus du petit commerce': undefined,
      'Transfert social': undefined,
      'Envoi de fonds': undefined,
      Don: undefined,
    },
    qcmrar: undefined,
    pt: {
      'Superficie possédée': undefined,
      'Superficie louée ou empruntée': undefined,
      'Superficie cultivée': undefined,
    },
    tc1: { type: undefined, qte: undefined },
    tc2: { type: undefined, qte: undefined },
    tc3: { type: undefined, qte: undefined },
  },
  da_data: {
    main: {
      Semences: { mantant: undefined, fourniseur: undefined },
      'Main d’œuvre (0 si main d\'œuvre gratuit)': {
        mantant: undefined,
        fourniseur: undefined,
      },
      Pesticides: { mantant: undefined, fourniseur: undefined },
      Fertilisants: { mantant: undefined, fourniseur: undefined },
      'Achat de matériel et équipements': {
        mantant: undefined,
        fourniseur: undefined,
      },
      'Location de matériel et équipements': {
        mantant: undefined,
        fourniseur: undefined,
      },
    },
    autre: [{ name: undefined, mantant: undefined, fourniseur: undefined }],
  },
  acaa_data: {
    aac: false,
    aacname: { AA02: undefined, AA03: undefined, AA04: undefined },
    afil: false,
    imf: false,
    imfname: undefined,
    imfmontant: undefined,
    imfprop: undefined,
    notimf: undefined,
    authercred: undefined,
    commercant: false,
    autre: undefined,
  },
  id1_data: [],
  educres_data: {
    primary: undefined,
    nombredanner: undefined,
    goscool: undefined,
    malagasy: undefined,
    malagasyecrit: undefined,
    sixmouth: undefined,
    annee: undefined,
  },
  ep_data: {
    principa_empl: undefined,
  },
  hmc_data: {
    lunette: undefined,
    audition: undefined,
    walking: undefined,
    brain: undefined,
    selfcare: undefined,
    maternallng: undefined,
    caring: undefined,
    chronicdeaseas: undefined,
    chronicdeaseasauth: undefined,
    diffic: undefined,
  },
  validatiaon_data: {
    chef: undefined,
    persone: undefined,
    quality: undefined,
  },
};

const stepList = [
  'Identification Enqueteur',
  'IDENTIFICATION GEOGRAPHIQUE',
  'IDENTIFICATION MENAGE',
  'CONDITIONS D\'HABITATIONS',
  'Biens possédés',
  'Sources de revenus et possession de terres',
  'Dépenses liées à l’agriculture',
  'Accès au crédit',
  'IDENTIFICATION MENAGE',
  'EDUCATION ET RESIDENCE',
  'EMPLOI PRINCIPAL',
  'HANDICAP ET MALADIE CHRONIQUE',
  'VALIDATION',
];

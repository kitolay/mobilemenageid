import { Component, Input, OnInit, HostListener } from '@angular/core';
import { getScreenSize } from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-ep',
  templateUrl: './ep.component.html',
  styleUrls: ['./ep.component.css'],
})
export class EpComponent implements OnInit {
  @Input() formData: any;
  @Input() disableNext?: (value: boolean) => void;
  observer?: IntersectionObserver;
  bpObs = 'L';

  constructor() {}

  ngOnInit(): void {
    this.bpObs = getScreenSize();
    this.observer = new IntersectionObserver(
      (entries, _observer) => {
        entries.forEach((entry) => {
          console.log('Element is fully visible in screen', entry);
          if (entry.isIntersecting === true) {
            if (this.disableNext) this.disableNext(true);
          } else {
            if (this.disableNext) this.disableNext(false);
          }
        });
      },
      {
        threshold: 1.0,
        root: document.querySelector('#stepcontainer-selector'),
      }
    );
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.bpObs = getScreenSize();
  }

  ngAfterViewInit(): void {
    //console.log('Element is fully visible in screen');
    const monitelement = document.querySelector('#id-last-element-for-next-ep');
    if (monitelement && this.observer) {
      this.observer.observe(monitelement);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    const monitelement = document.querySelector('#id-last-element-for-next-ep');
    if (monitelement && this.observer) {
      this.observer.unobserve(monitelement);
    }
  }
}

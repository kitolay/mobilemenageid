"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.IgComponent = void 0;
var core_1 = require("@angular/core");
var place_servicers_1 = require("src/app/Services/ApiServices/place.servicers");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var IgComponent = /** @class */ (function () {
    function IgComponent(toast) {
        this.toast = toast;
        this.isLoading = false;
        this.bpObs = 'L';
        this.province_list = [];
        this.region_list = [];
        this.district_list = [];
        this.commune_list = [];
        this.fokontany_list = [];
    }
    IgComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.bpObs = screen_service_1.getScreenSize();
        this.observer = new IntersectionObserver(function (entries, _observer) {
            entries.forEach(function (entry) {
                console.log('Element is fully visible in screen', entry);
                if (entry.isIntersecting === true) {
                    if (_this.disableNext) {
                        _this.disableNext(true);
                    }
                }
                else {
                    if (_this.disableNext) {
                        _this.disableNext(false);
                    }
                }
            });
        }, { threshold: 1.0, root: document.querySelector('#stepcontainer-selector') });
        if (this.contryListes) {
            if (this.contryListes.province_list) {
                this.province_list = this.contryListes.province_list;
            }
            else {
                this.getProvinceListe();
            }
            if (this.contryListes.region_list) {
                this.region_list = this.contryListes.region_list;
            }
            if (this.contryListes.district_list) {
                this.district_list = this.contryListes.district_list;
            }
            if (this.contryListes.commune_list) {
                this.commune_list = this.contryListes.commune_list;
            }
            if (this.contryListes.fokontany_list) {
                this.fokontany_list = this.contryListes.fokontany_list;
            }
        }
        else {
            this.getProvinceListe();
        }
    };
    IgComponent.prototype.onResize = function () {
        this.bpObs = screen_service_1.getScreenSize();
    };
    IgComponent.prototype.getProvinceListe = function () {
        var _this = this;
        this.isLoading = true;
        place_servicers_1.getProvince().then(function (resp_data) {
            var resp = resp_data;
            if (!resp || resp.error) {
                _this.toast.error(resp.message);
            }
            else if (resp && resp.length > 0) {
                _this.province_list = resp;
                _this.contryListes.province_list = resp;
                if (_this.formData.province) {
                    _this.province = _this.formData.province;
                    if (_this.formData.region) {
                        _this.getRegionListe(_this.province);
                    }
                }
            }
            else {
                _this.toast.error(resp);
            }
            _this.isLoading = false;
        });
    };
    IgComponent.prototype.getRegionListe = function (province) {
        var _this = this;
        if (province) {
            this.isLoading = true;
            place_servicers_1.getRegion({ prov_id: province.refId }).then(function (resp_data) {
                var resp = resp_data;
                if (resp.error) {
                    _this.toast.error(resp.message);
                }
                else if (resp.length > 0) {
                    _this.region_list = resp;
                    _this.contryListes.region_list = resp;
                    if (_this.formData.region) {
                        _this.region = _this.formData.region;
                        if (_this.formData.district) {
                            _this.getRegionListe(_this.region);
                        }
                    }
                }
                else {
                    _this.toast.error(resp);
                }
                _this.isLoading = false;
            });
        }
    };
    IgComponent.prototype.getDistListe = function (region) {
        var _this = this;
        if (region) {
            this.isLoading = true;
            place_servicers_1.getDistrict({ prov_id: region.prov_id, reg_id: region.refId }).then(function (resp_data) {
                var resp = resp_data;
                if (resp.error) {
                    _this.toast.error(resp.message);
                }
                else if (resp.length > 0) {
                    _this.district_list = resp;
                    _this.contryListes.district_list = resp;
                    if (_this.formData.district) {
                        _this.district = _this.formData.district;
                        if (_this.formData.commune) {
                            _this.getRegionListe(_this.district);
                        }
                    }
                }
                else {
                    _this.toast.error(resp);
                }
                _this.isLoading = false;
            });
        }
    };
    IgComponent.prototype.getCommListe = function (district) {
        var _this = this;
        if (district) {
            this.isLoading = true;
            place_servicers_1.getComunne({ prov_id: district.prov_id, reg_id: district.reg_id, dis_id: district.refId }).then(function (resp_data) {
                var resp = resp_data;
                if (resp.error) {
                    _this.toast.error(resp.message);
                }
                else if (resp.length > 0) {
                    _this.commune_list = resp;
                    _this.contryListes.commune_list = resp;
                    if (_this.formData.commune) {
                        _this.commune = _this.formData.commune;
                        if (_this.formData.fokontany) {
                            _this.getRegionListe(_this.commune);
                        }
                    }
                }
                else {
                    _this.toast.error(resp);
                }
                _this.isLoading = false;
            });
        }
    };
    IgComponent.prototype.getFoktanyList = function (commun) {
        var _this = this;
        if (commun) {
            this.isLoading = true;
            place_servicers_1.getFokontany({ prov_id: commun.prov_id, reg_id: commun.reg_id, dis_id: commun.dis_id, com_id: commun.refId }).then(function (resp_data) {
                var resp = resp_data;
                if (resp.error) {
                    _this.toast.error(resp.message);
                }
                else if (resp.length > 0) {
                    _this.fokontany_list = resp;
                    _this.contryListes.fokontany_list = resp;
                    if (_this.formData.fokontany) {
                        _this.fokontany = _this.formData.fokontany;
                    }
                }
                else {
                    _this.toast.error(resp);
                }
                _this.isLoading = false;
            });
        }
    };
    IgComponent.prototype.onProvenceChange = function (event) {
        this.formData.province = event;
        this.getRegionListe(event);
    };
    IgComponent.prototype.onRegienChange = function (event) {
        this.formData.region = event;
        this.getDistListe(event);
    };
    IgComponent.prototype.onDistChange = function (event) {
        this.formData.district = event;
        this.getCommListe(event);
    };
    IgComponent.prototype.onCommuneChange = function (event) {
        this.formData.commune = event;
        this.getFoktanyList(event);
    };
    IgComponent.prototype.onFokontanyChange = function (event) {
        this.formData.fokontany = event;
    };
    IgComponent.prototype.getLib = function (list) {
        return list.map(function (l) { return l.lib; });
    };
    IgComponent.prototype.ngAfterViewInit = function () {
        //console.log('Element is fully visible in screen');
        var monitelement = document.querySelector('#id-last-element-for-next-ig');
        if (monitelement && this.observer) {
            this.observer.observe(monitelement);
        }
    };
    IgComponent.prototype.ngOnDestroy = function () {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        var monitelement = document.querySelector('#id-last-element-for-next-ig');
        if (monitelement && this.observer) {
            this.observer.unobserve(monitelement);
        }
    };
    __decorate([
        core_1.Input()
    ], IgComponent.prototype, "formData");
    __decorate([
        core_1.Input()
    ], IgComponent.prototype, "disableNext");
    __decorate([
        core_1.Input()
    ], IgComponent.prototype, "contryListes");
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], IgComponent.prototype, "onResize");
    IgComponent = __decorate([
        core_1.Component({
            selector: 'app-ig',
            templateUrl: './ig.component.html',
            styleUrls: ['./ig.component.css']
        })
    ], IgComponent);
    return IgComponent;
}());
exports.IgComponent = IgComponent;

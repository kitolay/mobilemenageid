/* eslint-disable @angular-eslint/use-lifecycle-interface */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/member-ordering */
import { ToastrService } from 'ngx-toastr';
import { Component, Input, OnInit, HostListener } from '@angular/core';
import { CommuneData, DistrictData, FokontanyData, ProvinceData, RegionData } from 'src/app/Models/AllResponsModels';
import { getComunne, getDistrict, getFokontany, getProvince, getRegion } from 'src/app/Services/ApiServices/place.servicers';
import { getScreenSize } from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-ig',
  templateUrl: './ig.component.html',
  styleUrls: ['./ig.component.css']
})
export class IgComponent implements OnInit {
  @Input() formData: any;
    @Input() disableNext?: (value: boolean) => void;
  observer?: IntersectionObserver;
  @Input() contryListes: any;
  isLoading = false;
  province?: any;
  region?: any;
  district?: any;
  commune?: any;
  fokontany?: any;
  bpObs = 'L';

  province_list: ProvinceData[] = [];
  region_list: RegionData[] = [];
  district_list: DistrictData[] = [];
  commune_list: CommuneData[] = [];
  fokontany_list: FokontanyData[] = [];

  constructor(private toast: ToastrService) { }

  ngOnInit(): void {
    this.bpObs = getScreenSize();
    this.observer = new IntersectionObserver(
      (entries, _observer) => {
          entries.forEach(entry => {
            console.log('Element is fully visible in screen', entry);
            if(entry.isIntersecting === true) {
              if(this.disableNext) {this.disableNext(true);}
            }else{
              if(this.disableNext) {this.disableNext(false);}
            }
          });
        },
        { threshold: 1.0, root:document.querySelector('#stepcontainer-selector') }
      );

    if(this.contryListes){
      if(this.contryListes.province_list) {this.province_list = this.contryListes.province_list;}
      else {this.getProvinceListe();}
      if(this.contryListes.region_list) {this.region_list = this.contryListes.region_list;}
      if(this.contryListes.district_list) {this.district_list = this.contryListes.district_list;}
      if(this.contryListes.commune_list) {this.commune_list = this.contryListes.commune_list;}
      if(this.contryListes.fokontany_list) {this.fokontany_list = this.contryListes.fokontany_list;}
    }else
    {this.getProvinceListe();}
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.bpObs = getScreenSize();
  }

  getProvinceListe(){
    this.isLoading = true;
    getProvince().then(resp_data=>{
      const resp = resp_data;
      if(!resp || resp.error){
        this.toast.error(resp.message);
      }else if(resp && resp.length > 0){
        this.province_list = resp;
        this.contryListes.province_list = resp;
        if(this.formData.province){
          this.province = this.formData.province;
          if(this.formData.region){
            this.getRegionListe(this.province);
          }
        }
      }else {this.toast.error(resp);}
      this.isLoading = false;
    });
  }

  getRegionListe(province: ProvinceData){
    if(province){
      this.isLoading = true;
      getRegion({prov_id: province.refId}).then(resp_data=>{
        const resp = resp_data;
        if(resp.error){
          this.toast.error(resp.message);
        }else if(resp.length > 0){
          this.region_list = resp;
          this.contryListes.region_list = resp;
          if(this.formData.region){
            this.region = this.formData.region;
            if(this.formData.district){
              this.getRegionListe(this.region);
            }
          }
        }else {this.toast.error(resp);}
        this.isLoading = false;
      });
    }
  }

  getDistListe(region: RegionData){
    if(region){
      this.isLoading = true;
      getDistrict({prov_id: region.prov_id, reg_id: region.refId}).then(resp_data=>{
        const resp = resp_data;
        if(resp.error){
          this.toast.error(resp.message);
        }else if(resp.length > 0){
          this.district_list = resp;
          this.contryListes.district_list = resp;
          if(this.formData.district){
            this.district = this.formData.district;
            if(this.formData.commune){
              this.getRegionListe(this.district);
            }
          }
        }else {this.toast.error(resp);}
        this.isLoading = false;
      });
    }
  }

  getCommListe(district: DistrictData){
    if(district){
      this.isLoading = true;
      getComunne({prov_id: district.prov_id, reg_id: district.reg_id, dis_id: district.refId}).then(resp_data=>{
        const resp = resp_data;
        if(resp.error){
          this.toast.error(resp.message);
        }else if(resp.length > 0){
          this.commune_list = resp;
          this.contryListes.commune_list = resp;
          if(this.formData.commune){
            this.commune = this.formData.commune;
            if(this.formData.fokontany){
              this.getRegionListe(this.commune);
            }
          }
        }else {this.toast.error(resp);}
        this.isLoading = false;
      });
    }
  }

  getFoktanyList(commun: CommuneData){
    if(commun){
      this.isLoading = true;
      getFokontany({prov_id: commun.prov_id, reg_id: commun.reg_id, dis_id: commun.dis_id, com_id: commun.refId}).then(resp_data=>{
        const resp = resp_data;
        if(resp.error){
          this.toast.error(resp.message);
        }else if(resp.length > 0){
          this.fokontany_list = resp;
          this.contryListes.fokontany_list = resp;
          if(this.formData.fokontany){
            this.fokontany = this.formData.fokontany;
          }
        }else {this.toast.error(resp);}
        this.isLoading = false;
      });
    }
  }

  onProvenceChange(event: ProvinceData){
    this.formData.province = event;
    this.getRegionListe(event);
  }

  onRegienChange(event: RegionData){
    this.formData.region = event;
    this.getDistListe(event);
  }

  onDistChange(event: DistrictData){
    this.formData.district = event;
    this.getCommListe(event);
  }

  onCommuneChange(event: CommuneData){
    this.formData.commune = event;
    this.getFoktanyList(event);
  }

  onFokontanyChange(event: FokontanyData){
    this.formData.fokontany = event;
  }

  getLib(list: any){
    return list.map((l: any)=>l.lib);
  }

  ngAfterViewInit(): void {
    //console.log('Element is fully visible in screen');
    const monitelement = document.querySelector('#id-last-element-for-next-ig');
    if (monitelement && this.observer) {
      this.observer.observe(monitelement);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    const monitelement = document.querySelector('#id-last-element-for-next-ig');
    if (monitelement && this.observer) {
      this.observer.unobserve(monitelement);
    }
  }

}

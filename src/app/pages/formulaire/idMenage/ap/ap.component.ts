import { Component, Input, OnInit, HostListener } from '@angular/core';
import { getScreenSize, getScreenValue } from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-ap',
  templateUrl: './ap.component.html',
  styleUrls: ['./ap.component.css']
})

export class ApComponent implements OnInit {

  screen = {x:0,y:0};
  bpObs = 'L';

  @Input() formData: any;
    @Input() disableNext?: (value: boolean) => void;
  observer?: IntersectionObserver;
  item_list:any = [];
  constructor() { }

  ngOnInit(): void {
    this.observer = new IntersectionObserver(
      (entries, _observer) => {
          entries.forEach(entry => {
            console.log('Element is fully visible in screen', entry);
            if(entry.isIntersecting === true) {
              if(this.disableNext) this.disableNext(true);
            }else{
              if(this.disableNext) this.disableNext(false);
            }
          });
        },
        { threshold: 1.0, root:document.querySelector("#stepcontainer-selector") }
      );
    this.intiScreendata()
    this.item_list = Object.keys(this.formData)
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.intiScreendata()
  }

  intiScreendata(){
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
    //console.log(this.bpObs, this.screen)
  }

  ngAfterViewInit(): void {
    //console.log('Element is fully visible in screen');
    const monitelement = document.querySelector('#id-last-element-for-next-ap');
    if (monitelement && this.observer) {
      this.observer.observe(monitelement);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    const monitelement = document.querySelector('#id-last-element-for-next-ap');
    if (monitelement && this.observer) {
      this.observer.unobserve(monitelement);
    }
  }
}


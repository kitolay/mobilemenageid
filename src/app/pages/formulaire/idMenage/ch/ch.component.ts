import { Component, Input, OnInit, HostListener } from '@angular/core';
import { getScreenSize, getScreenValue } from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-ch',
  templateUrl: './ch.component.html',
  styleUrls: ['./ch.component.css'],
})
export class ChComponent implements OnInit {

  screen = {x:0,y:0};
  bpObs = 'L';


  @Input() formData: any;
    @Input() disableNext?: (value: boolean) => void;
  observer?: IntersectionObserver;
  showmur:boolean = false;
  shoplaf:boolean = false;
  showplanch:boolean = false;
  showelec:boolean = false;
  showeau:boolean = false;
  showtoil:boolean = false;
  vmur?:string;
  vplaf?:string;
  vlanch?:string;
  velec?:string;
  veau?:string;
  vtoil?:string;
  constructor() {}

  ngOnInit(): void {
    this.observer = new IntersectionObserver(
      (entries, _observer) => {
          entries.forEach(entry => {
            console.log('Element is fully visible in screen', entry);
            if(entry.isIntersecting === true) {
              if(this.disableNext) this.disableNext(true);
            }else{
              if(this.disableNext) this.disableNext(false);
            }
          });
        },
        { threshold: 1.0, root:document.querySelector("#stepcontainer-selector") }
      );
    this.intiScreendata()
    if(this.formData.extwall.au) this.vmur = 'au'
    else this.vmur = this.formData.extwall.lib

    if(this.formData.plaf.au) this.vplaf = 'au'
    else this.vplaf = this.formData.plaf.lib

    if(this.formData.plancer.au) this.vlanch = 'au'
    else this.vlanch = this.formData.plancer.lib

    if(this.formData.elect.au) this.velec = 'au'
    else this.velec = this.formData.elect.lib

    if(this.formData.eau.au) this.veau = 'au'
    else this.veau = this.formData.eau.lib

    if(this.formData.toilet.au) this.vtoil = 'au'
    else this.vtoil = this.formData.toilet.lib

    this.showmur = this.formData.extwall.au
    this.shoplaf = this.formData.plaf.au
    this.showplanch = this.formData.plancer.au
    this.showelec = this.formData.elect.au
    this.showeau = this.formData.eau.au
    this.showtoil = this.formData.toilet.au
  }

  onValueChange(ref: string, event: string) {
    switch (ref) {
      case 'mur':
        if(event === "au"){
          this.showmur = true
          this.formData.extwall.au = true;
          this.formData.extwall.lib = '';
        }else {
          this.formData.extwall.lib = event;
          this.formData.extwall.au = false;
          this.showmur = false;
        }
        break;
      case 'plaf':
        if(event === "au"){
          this.shoplaf = true
          this.formData.plaf.au = true;
          this.formData.plaf.lib = "";
        }else {
          this.formData.plaf.lib = event;
          this.formData.plaf.au = false;
          this.shoplaf = false;
        }
        break;
      case 'planch':
        if(event === "au"){
          this.showplanch = true
          this.formData.plancer.au = true;
          this.formData.plancer.lib = "";
        }else {
          this.formData.plancer.lib = event;
          this.formData.plancer.au = false;
          this.showplanch = false;
        }
        break;
      case 'elec':
        if(event === "au"){
          this.showelec = true
          this.formData.elect.au = true;
          this.formData.elect.lib = "";
        }else {
          this.formData.elect.lib = event;
          this.formData.elect.au = false;
          this.showelec = false;
        }
        break;
      case 'eau':
        if(event === "au"){
          this.showeau = true
          this.formData.eau.au = true;
          this.formData.eau.lib = "";
        }else {
          this.formData.eau.lib = event;
          this.formData.eau.au = false;
          this.showeau = false;
        }
        break;
      case 'toil':
        if(event === "au"){
          this.showtoil = true
          this.formData.toilet.au = true;
          this.formData.toilet.lib = "";
        }else {
          this.formData.toilet.lib = event;
          this.formData.toilet.au = false;
          this.showtoil = false;
        }
        break;

      default:
        break;
    }
  }
  getDisplayBystate(state: boolean){
    return state ? "inline" : "none"
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.intiScreendata()
  }

  intiScreendata(){
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
    //console.log(this.bpObs, this.screen)
  }
  ngAfterViewInit(): void {
    //console.log('Element is fully visible in screen');
    const monitelement = document.querySelector('#id-last-element-for-next-ch');
    if (monitelement && this.observer) {
      this.observer.observe(monitelement);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    const monitelement = document.querySelector('#id-last-element-for-next-ch');
    if (monitelement && this.observer) {
      this.observer.unobserve(monitelement);
    }
  }
}

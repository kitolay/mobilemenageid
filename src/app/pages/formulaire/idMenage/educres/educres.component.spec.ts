import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EducresComponent } from './educres.component';

describe('EducresComponent', () => {
  let component: EducresComponent;
  let fixture: ComponentFixture<EducresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EducresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EducresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

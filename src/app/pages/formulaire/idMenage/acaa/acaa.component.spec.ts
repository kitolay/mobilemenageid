import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcaaComponent } from './acaa.component';

describe('AcaaComponent', () => {
  let component: AcaaComponent;
  let fixture: ComponentFixture<AcaaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcaaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcaaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

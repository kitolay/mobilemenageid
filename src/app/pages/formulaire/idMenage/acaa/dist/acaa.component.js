"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AcaaComponent = void 0;
/* eslint-disable @angular-eslint/use-lifecycle-interface */
/* eslint-disable @typescript-eslint/member-ordering */
var core_1 = require("@angular/core");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var AcaaComponent = /** @class */ (function () {
    function AcaaComponent() {
        this.bpObs = 'L';
    }
    AcaaComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.bpObs = screen_service_1.getScreenSize();
        this.observer = new IntersectionObserver(function (entries, _observer) {
            entries.forEach(function (entry) {
                console.log('Element is fully visible in screen', entry);
                if (entry.isIntersecting === true) {
                    if (_this.disableNext) {
                        _this.disableNext(true);
                    }
                }
                else {
                    if (_this.disableNext) {
                        _this.disableNext(false);
                    }
                }
            });
        }, {
            threshold: 1.0,
            root: document.querySelector('#stepcontainer-selector')
        });
    };
    AcaaComponent.prototype.onResize = function () {
        this.bpObs = screen_service_1.getScreenSize();
    };
    AcaaComponent.prototype.ngAfterViewInit = function () {
        //console.log('Element is fully visible in screen');
        var monitelement = document.querySelector('#id-last-element-for-next-acaa');
        if (monitelement && this.observer) {
            this.observer.observe(monitelement);
        }
    };
    AcaaComponent.prototype.ngOnDestroy = function () {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        var monitelement = document.querySelector('#id-last-element-for-next-acaa');
        if (monitelement && this.observer) {
            this.observer.unobserve(monitelement);
        }
    };
    __decorate([
        core_1.Input()
    ], AcaaComponent.prototype, "formData");
    __decorate([
        core_1.Input()
    ], AcaaComponent.prototype, "disableNext");
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], AcaaComponent.prototype, "onResize");
    AcaaComponent = __decorate([
        core_1.Component({
            selector: 'app-acaa',
            templateUrl: './acaa.component.html',
            styleUrls: ['./acaa.component.css']
        })
    ], AcaaComponent);
    return AcaaComponent;
}());
exports.AcaaComponent = AcaaComponent;

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SrptComponent } from './srpt.component';

describe('SrptComponent', () => {
  let component: SrptComponent;
  let fixture: ComponentFixture<SrptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SrptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SrptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

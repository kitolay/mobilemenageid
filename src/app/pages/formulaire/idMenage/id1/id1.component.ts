/* eslint-disable @angular-eslint/use-lifecycle-interface */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, Input, OnInit, HostListener } from '@angular/core';
import { getScreenSize } from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-id1',
  templateUrl: './id1.component.html',
  styleUrls: ['./id1.component.css'],
})
export class Id1Component implements OnInit {
  @Input() formData: any;
  @Input() disableNext?: (value: boolean) => void;
  observer?: IntersectionObserver;
  curentIndex = -1;
  bpObs = 'L';

  constructor() {}

  ngOnInit(): void {
    this.bpObs = getScreenSize();
    this.observer = new IntersectionObserver(
      (entries, _observer) => {
        entries.forEach((entry) => {
          console.log('Element is fully visible in screen', entry);
          if (entry.isIntersecting === true) {
            if (this.disableNext) {this.disableNext(true);}
          } else {
            if (this.disableNext) {this.disableNext(false);}
          }
        });
      },
      {
        threshold: 1.0,
        root: document.querySelector('#stepcontainer-selector'),
      }
    );
    if (this.formData && this.formData.length > 0) {this.curentIndex = 0;}
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.bpObs = getScreenSize();
  }

  handleChipsClick(index: number) {
    this.curentIndex = index;
  }

  handleDeletElement(index: number) {
    if (index >= 0) {
      this.formData.splice(index, 1);
      this.curentIndex = this.formData.length - 1;
    }
  }

  handleAddMenage = () => {
    this.formData.push({
      nom: undefined,
      prenom: undefined,
      surnom: undefined,
      resstat: undefined,
      sex: undefined,
      age: undefined,
      cpan: undefined,
      cin: { statue: undefined, value: undefined },
      enfu: undefined,
      idenfu: undefined,
      lienCDM: undefined,
      statueM: undefined,
      menageMere: undefined,
      menageMereId: undefined,
      orphan: undefined,
      childMenage: undefined,
      pregnante: undefined,
    });
    this.curentIndex = this.formData.length - 1;
  };
  ngAfterViewInit(): void {
    //console.log('Element is fully visible in screen');
    const monitelement = document.querySelector(
      '#id-last-element-for-next-id1'
    );
    if (monitelement && this.observer) {
      this.observer.observe(monitelement);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    const monitelement = document.querySelector(
      '#id-last-element-for-next-id1'
    );
    if (monitelement && this.observer) {
      this.observer.unobserve(monitelement);
    }
  }
}

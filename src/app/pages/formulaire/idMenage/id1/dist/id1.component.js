"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.Id1Component = void 0;
/* eslint-disable @angular-eslint/use-lifecycle-interface */
/* eslint-disable @typescript-eslint/member-ordering */
var core_1 = require("@angular/core");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var Id1Component = /** @class */ (function () {
    function Id1Component() {
        var _this = this;
        this.curentIndex = -1;
        this.bpObs = 'L';
        this.handleAddMenage = function () {
            _this.formData.push({
                nom: undefined,
                prenom: undefined,
                surnom: undefined,
                resstat: undefined,
                sex: undefined,
                age: undefined,
                cpan: undefined,
                cin: { statue: undefined, value: undefined },
                enfu: undefined,
                idenfu: undefined,
                lienCDM: undefined,
                statueM: undefined,
                menageMere: undefined,
                menageMereId: undefined,
                orphan: undefined,
                childMenage: undefined,
                pregnante: undefined
            });
            _this.curentIndex = _this.formData.length - 1;
        };
    }
    Id1Component.prototype.ngOnInit = function () {
        var _this = this;
        this.bpObs = screen_service_1.getScreenSize();
        this.observer = new IntersectionObserver(function (entries, _observer) {
            entries.forEach(function (entry) {
                console.log('Element is fully visible in screen', entry);
                if (entry.isIntersecting === true) {
                    if (_this.disableNext) {
                        _this.disableNext(true);
                    }
                }
                else {
                    if (_this.disableNext) {
                        _this.disableNext(false);
                    }
                }
            });
        }, {
            threshold: 1.0,
            root: document.querySelector('#stepcontainer-selector')
        });
        if (this.formData && this.formData.length > 0) {
            this.curentIndex = 0;
        }
    };
    Id1Component.prototype.onResize = function () {
        this.bpObs = screen_service_1.getScreenSize();
    };
    Id1Component.prototype.handleChipsClick = function (index) {
        this.curentIndex = index;
    };
    Id1Component.prototype.handleDeletElement = function (index) {
        if (index >= 0) {
            this.formData.splice(index, 1);
            this.curentIndex = this.formData.length - 1;
        }
    };
    Id1Component.prototype.ngAfterViewInit = function () {
        //console.log('Element is fully visible in screen');
        var monitelement = document.querySelector('#id-last-element-for-next-id1');
        if (monitelement && this.observer) {
            this.observer.observe(monitelement);
        }
    };
    Id1Component.prototype.ngOnDestroy = function () {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        var monitelement = document.querySelector('#id-last-element-for-next-id1');
        if (monitelement && this.observer) {
            this.observer.unobserve(monitelement);
        }
    };
    __decorate([
        core_1.Input()
    ], Id1Component.prototype, "formData");
    __decorate([
        core_1.Input()
    ], Id1Component.prototype, "disableNext");
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], Id1Component.prototype, "onResize");
    Id1Component = __decorate([
        core_1.Component({
            selector: 'app-id1',
            templateUrl: './id1.component.html',
            styleUrls: ['./id1.component.css']
        })
    ], Id1Component);
    return Id1Component;
}());
exports.Id1Component = Id1Component;

import { Component, Input, OnInit, HostListener } from '@angular/core';
import { getScreenSize, getScreenValue } from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-da',
  templateUrl: './da.component.html',
  styleUrls: ['./da.component.css']
})
export class DaComponent implements OnInit {

  screen = {x:0,y:0};
  bpObs = 'L';


  @Input() formData: any;
    @Input() disableNext?: (value: boolean) => void;
  observer?: IntersectionObserver;
  autre_liste: any = [];
  data_value: any = [];
  constructor() { }

  ngOnInit(): void {
    this.observer = new IntersectionObserver(
      (entries, _observer) => {
          entries.forEach(entry => {
            console.log('Element is fully visible in screen', entry);
            if(entry.isIntersecting === true) {
              if(this.disableNext) this.disableNext(true);
            }else{
              if(this.disableNext) this.disableNext(false);
            }
          });
        },
        { threshold: 1.0, root:document.querySelector("#stepcontainer-selector") }
      );
    this.intiScreendata()
    this.autre_liste = this.formData.autre
    this.data_value = Object.keys(this.formData.main)
  }

  onAdd = () => {
    this.autre_liste.push({name: '', mantant: '', fourniseur: ''})
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.intiScreendata()
  }

  intiScreendata(){
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
    //console.log(this.bpObs, this.screen)
  }

  ngAfterViewInit(): void {
    //console.log('Element is fully visible in screen');
    const monitelement = document.querySelector('#id-last-element-for-next-da');
    if (monitelement && this.observer) {
      this.observer.observe(monitelement);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    const monitelement = document.querySelector('#id-last-element-for-next-da');
    if (monitelement && this.observer) {
      this.observer.unobserve(monitelement);
    }
  }
}

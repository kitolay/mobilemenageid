/* eslint-disable @angular-eslint/use-lifecycle-interface */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, Input, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import {
  getScreenSize,
  getScreenValue,
} from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-id',
  templateUrl: './id.component.html',
  styleUrls: ['./id.component.css'],
})
export class IdComponent implements OnInit {
  screen = { x: 0, y: 0 };
  bpObs = 'L';

  @Input() formData: any;
  @Input() disableNext?: (value: boolean) => void;
  @ViewChild('id_last_element_for_next_id') input?: ElementRef;
  observer?: IntersectionObserver;
  pictureImage = '';
  fileUploaded?: File;
  constructor() {

  }

  ngOnInit(): void {
    this.observer = new IntersectionObserver(
      (entries, _observer) => {
        entries.forEach((entry) => {
          console.log('Element is fully visible in screen', entry);
          if (entry.isIntersecting === true) {
            console.log('this.disableNext',this.disableNext);
            if (this.disableNext) {this.disableNext(true);}
          } else {
            if (this.disableNext) {this.disableNext(false);}
          }
        });
      },
      {
        threshold: 1.0,
        root: document.querySelector('#stepcontainer-selector'),
      }
    );

    this.intiScreendata();
    if (this.formData.pict) {
      this.pictureImage = URL.createObjectURL(this.formData.pict);
    }
  }

  onpictureChange(event: any) {
    this.fileUploaded = event.target.files[0];
    console.log('UploadedFile ==> ', this.fileUploaded);
    this.formData.pict = this.fileUploaded;
    this.pictureImage = URL.createObjectURL(this.fileUploaded);
  }

  onPictureClick(element: HTMLInputElement) {
    element.click();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.intiScreendata();
  }

  intiScreendata() {
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
    //console.log(this.bpObs, this.screen)
  }

  ngAfterViewInit(): void {
    //console.log('Element is fully visible in screen',this.input);
    const monitelement = document.querySelector('#id-last-element-for-next-id');
    //console.log('Element is fully visible in screen', monitelement);
    if (monitelement && this.observer) {
      this.observer.observe(monitelement);
    }
  }
  testClick(value: any){
    console.log('Element is fully visible in screen',value);
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    const monitelement = document.querySelector('#id-last-element-for-next-id');
    if (monitelement && this.observer) {
      this.observer.unobserve(monitelement);
    }
  }
}

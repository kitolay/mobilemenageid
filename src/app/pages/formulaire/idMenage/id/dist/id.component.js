"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.IdComponent = void 0;
/* eslint-disable @angular-eslint/use-lifecycle-interface */
/* eslint-disable @typescript-eslint/member-ordering */
var core_1 = require("@angular/core");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var IdComponent = /** @class */ (function () {
    function IdComponent() {
        this.screen = { x: 0, y: 0 };
        this.bpObs = 'L';
        this.pictureImage = '';
    }
    IdComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.observer = new IntersectionObserver(function (entries, _observer) {
            entries.forEach(function (entry) {
                console.log('Element is fully visible in screen', entry);
                if (entry.isIntersecting === true) {
                    console.log('this.disableNext', _this.disableNext);
                    if (_this.disableNext) {
                        _this.disableNext(true);
                    }
                }
                else {
                    if (_this.disableNext) {
                        _this.disableNext(false);
                    }
                }
            });
        }, {
            threshold: 1.0,
            root: document.querySelector('#stepcontainer-selector')
        });
        this.intiScreendata();
        if (this.formData.pict) {
            this.pictureImage = URL.createObjectURL(this.formData.pict);
        }
    };
    IdComponent.prototype.onpictureChange = function (event) {
        this.fileUploaded = event.target.files[0];
        console.log('UploadedFile ==> ', this.fileUploaded);
        this.formData.pict = this.fileUploaded;
        this.pictureImage = URL.createObjectURL(this.fileUploaded);
    };
    IdComponent.prototype.onPictureClick = function (element) {
        element.click();
    };
    IdComponent.prototype.onResize = function () {
        this.intiScreendata();
    };
    IdComponent.prototype.intiScreendata = function () {
        this.bpObs = screen_service_1.getScreenSize();
        this.screen = screen_service_1.getScreenValue();
        //console.log(this.bpObs, this.screen)
    };
    IdComponent.prototype.ngAfterViewInit = function () {
        //console.log('Element is fully visible in screen',this.input);
        var monitelement = document.querySelector('#id-last-element-for-next-id');
        //console.log('Element is fully visible in screen', monitelement);
        if (monitelement && this.observer) {
            this.observer.observe(monitelement);
        }
    };
    IdComponent.prototype.testClick = function (value) {
        console.log('Element is fully visible in screen', value);
    };
    IdComponent.prototype.ngOnDestroy = function () {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        var monitelement = document.querySelector('#id-last-element-for-next-id');
        if (monitelement && this.observer) {
            this.observer.unobserve(monitelement);
        }
    };
    __decorate([
        core_1.Input()
    ], IdComponent.prototype, "formData");
    __decorate([
        core_1.Input()
    ], IdComponent.prototype, "disableNext");
    __decorate([
        core_1.ViewChild('id_last_element_for_next_id')
    ], IdComponent.prototype, "input");
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], IdComponent.prototype, "onResize");
    IdComponent = __decorate([
        core_1.Component({
            selector: 'app-id',
            templateUrl: './id.component.html',
            styleUrls: ['./id.component.css']
        })
    ], IdComponent);
    return IdComponent;
}());
exports.IdComponent = IdComponent;

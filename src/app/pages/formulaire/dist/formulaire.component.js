"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.FormulaireComponent = void 0;
/* eslint-disable max-len */
var pouchdb_1 = require("pouchdb");
var sync_services_1 = require("./../../Services/ApiServices/sync.services");
var core_1 = require("@angular/core");
var form_services_1 = require("src/app/Services/ApiServices/form.services");
var locals_1 = require("src/app/Services/locals");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var card_services_1 = require("src/app/Services/offline/card.services");
//import { NguCarouselConfig } from '@ngu/carousel';
var FormulaireComponent = /** @class */ (function () {
    function FormulaireComponent(toast, router) {
        var _this = this;
        this.toast = toast;
        this.router = router;
        this.all_text = locals_1.ALL_TEXT;
        this.curentIndex = 1;
        this.disableNext = false;
        this.formdata = all_formulaire_data;
        this.savingData = false;
        this.bpObs = 'L';
        this.step_list = stepList;
        this.screen = { x: 0, y: 0 };
        this.handdleScrool = function (state) {
            _this.disableNext = !state;
        };
        this.handleSetPage = function (idex) {
            //console.log('change curent indexd');
            _this.curentIndex = idex;
            if (_this.stepcontainer && _this.stepcontainer.nativeElement) {
                _this.stepcontainer.nativeElement.scrollTop = 0;
            }
            if (_this.stepContainerMob && _this.stepContainerMob.nativeElement) {
                _this.stepContainerMob.nativeElement.scrollTop = 0;
            }
        };
    }
    FormulaireComponent.prototype.ngOnInit = function () {
        this.bpObs = screen_service_1.getScreenSize();
        this.screen = screen_service_1.getScreenValue();
    };
    FormulaireComponent.prototype.handleNext = function () {
        this.curentIndex++;
        if (this.stepcontainer && this.stepcontainer.nativeElement) {
            this.stepcontainer.nativeElement.scrollTop = 0;
        }
        if (this.stepContainerMob && this.stepContainerMob.nativeElement) {
            this.stepContainerMob.nativeElement.scrollTop = 0;
        }
    };
    FormulaireComponent.prototype.handlePreviouse = function () {
        this.curentIndex--;
        if (this.stepcontainer && this.stepcontainer.nativeElement) {
            this.stepcontainer.nativeElement.scrollTop = 0;
        }
        if (this.stepContainerMob && this.stepContainerMob.nativeElement) {
            this.stepContainerMob.nativeElement.scrollTop = 0;
        }
    };
    FormulaireComponent.prototype.handleSaveData = function () {
        var _this = this;
        this.savingData = true;
        sync_services_1.testConnextion().then(function (conn) {
            var _a;
            if (conn.state) {
                form_services_1.saveSurveyData(all_formulaire_data)
                    .then(function (rep) {
                    if (rep.error) {
                        _this.toast.error(rep.message);
                    }
                    else if (rep && !rep.error) {
                        var card_data = __assign({}, rep);
                        delete card_data.details;
                        var base_detail = __assign({}, rep.details);
                        delete base_detail.id1_data;
                        var id1Data_1 = rep.details.id1_data && rep.details.id1_data.length > 0
                            ? __spreadArrays(rep.details.id1_data) : null;
                        var all_data_1 = __assign(__assign({}, base_detail), { card_item: card_data });
                        var table_name_kay_1 = Object.keys(all_data_1);
                        table_name_kay_1.forEach(function (tName, idx) {
                            var table_data = new pouchdb_1["default"](tName);
                            table_data.bulkDocs([all_data_1[tName]]);
                            if (idx >= table_name_kay_1.length - 1) {
                                if (id1Data_1) {
                                    var table_id = new pouchdb_1["default"]('id1_data');
                                    table_id.bulkDocs(id1Data_1);
                                }
                                _this.savingData = false;
                                _this.toast.success('Menage Ajouter!');
                                _this.router.navigate([locals_1.ROUT_PATH.listes]);
                            }
                        });
                    }
                    else {
                        _this.toast.error(rep);
                    }
                    _this.savingData = false;
                })["catch"](function (err) {
                    _this.savingData = false;
                    _this.toast.error(err);
                });
            }
            else {
                var all_formulaire_data_to_sate = all_formulaire_data;
                var PMT = Math.floor(Math.random() * 97 + 1) / 100;
                var card_item = {
                    nom: all_formulaire_data.id_data.nom,
                    prenom: all_formulaire_data.id_data.prenom,
                    nis: all_formulaire_data.id_data.cin,
                    cdf: all_formulaire_data.id_data.cdfm ? 1 : 0,
                    adress: ((_a = all_formulaire_data.ig_data.lotis) !== null && _a !== void 0 ? _a : '') +
                        '/' +
                        (all_formulaire_data.ig_data.fokontany
                            ? all_formulaire_data.ig_data.fokontany.lib
                            : '') +
                        '/' +
                        (all_formulaire_data.ig_data.commune
                            ? all_formulaire_data.ig_data.commune.lib
                            : '') +
                        '/' +
                        (all_formulaire_data.ig_data.district
                            ? all_formulaire_data.ig_data.district.lib
                            : '') +
                        '/' +
                        (all_formulaire_data.ig_data.region
                            ? all_formulaire_data.ig_data.region.lib
                            : '') +
                        '/' +
                        (all_formulaire_data.ig_data.province
                            ? all_formulaire_data.ig_data.province.lib
                            : ''),
                    pict: all_formulaire_data.id_data.pict,
                    coderef: PMT + '-' + all_formulaire_data.id_data.cin,
                    barref: PMT + '-' + all_formulaire_data.id_data.cin,
                    offline: true,
                    createdAt: new Date(),
                    updatedAt: new Date()
                };
                var backup_db = new pouchdb_1["default"](card_services_1.off_db_name);
                backup_db
                    .put(__assign(__assign({}, all_formulaire_data_to_sate), { _id: card_item.coderef + '-' + card_item.adress, card_item: card_item }))
                    .then(function (res) {
                    // backup_db
                    // .putAttachment(
                    //   all_formulaire_data.id_data.nom +
                    //     all_formulaire_data.id_data.prenom,
                    //   all_formulaire_data.id_data.pict,
                    //   all_formulaire_data.id_data.pict,
                    //   'image/png'
                    // )
                    // .then(() => {
                    //   this.savingData = false;
                    //   this.toast.success('Menage Ajouter!');
                    //   this.router.navigate([ROUT_PATH.listes]);
                    // })
                    // .catch((err) => {
                    //   console.log(err);
                    //   this.toast.error(err);
                    //   this.savingData = false;
                    // });
                    console.log('add-result ===>', res);
                    _this.savingData = false;
                    _this.toast.success('Menage Ajouter!');
                    _this.router.navigate([locals_1.ROUT_PATH.listes]);
                })["catch"](function (err) {
                    console.log(err);
                    _this.toast.error(err);
                    _this.savingData = false;
                });
            }
        });
    };
    FormulaireComponent.prototype.getbuttonStatue = function () {
        return this.disableNext;
    };
    FormulaireComponent.prototype.onResize = function () {
        this.bpObs = screen_service_1.getScreenSize();
        this.screen = screen_service_1.getScreenValue();
    };
    __decorate([
        core_1.ViewChild('stepcontainer')
    ], FormulaireComponent.prototype, "stepcontainer");
    __decorate([
        core_1.ViewChild('stepcontainerMob')
    ], FormulaireComponent.prototype, "stepContainerMob");
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], FormulaireComponent.prototype, "onResize");
    FormulaireComponent = __decorate([
        core_1.Component({
            selector: 'app-formulaire',
            templateUrl: './formulaire.component.html',
            styleUrls: ['./formulaire.component.css']
        })
    ], FormulaireComponent);
    return FormulaireComponent;
}());
exports.FormulaireComponent = FormulaireComponent;
var all_formulaire_data = {
    enqueteur_data: {
        name: undefined,
        prename: undefined,
        cin: undefined,
        id: undefined,
        sup_name: undefined,
        sup_prename: undefined,
        start_date: new Date()
    },
    ig_data: {
        lotis: undefined,
        province: undefined,
        region: undefined,
        district: undefined,
        commune: undefined,
        fokontany: undefined,
        milieu: undefined,
        numero: undefined
    },
    id_data: {
        cdfm: true,
        pict: undefined,
        nom: undefined,
        prenom: undefined,
        surnom: undefined,
        cin: undefined,
        datedelcin: undefined,
        cincon: undefined,
        datedelcincon: undefined
    },
    ch_data: {
        housetype: undefined,
        extwall: { au: false, lib: undefined },
        plaf: { au: false, lib: undefined },
        npiech: undefined,
        npiecedor: undefined,
        piecsurf: undefined,
        p1: undefined,
        p2: undefined,
        p3: undefined,
        p4: undefined,
        plancer: { au: false, lib: undefined },
        elect: { au: false, lib: undefined },
        eau: { au: false, lib: undefined },
        toilet: { au: false, lib: undefined }
    },
    ap_data: {
        'Chaise (avec pied et dossier)': { state: false, qte: undefined },
        'Table (avec pied, bien droit)': { state: false, qte: undefined },
        Natte: { state: false, qte: undefined },
        'Lampe pétrole': { state: false, qte: undefined },
        'Poste radio': { state: false, qte: undefined },
        'Radio avec cassette, cd, mp3..': { state: false, qte: undefined },
        Téléviseur: { state: false, qte: undefined },
        'Lecteur CD DVD': { state: false, qte: undefined },
        Bicyclette: { state: false, qte: undefined },
        Puits: { state: false, qte: undefined },
        Irrigation: { state: false, qte: undefined },
        'Téléphone portable': { state: false, qte: undefined },
        'Charrue (traction animale)': { state: false, qte: undefined },
        'Charrette (traction animale)': { state: false, qte: undefined },
        'Herse (traction animale)': { state: false, qte: undefined },
        Bèche: { state: false, qte: undefined },
        Stockage: { state: false, qte: undefined },
        'Champ ou rizière (non métayage)': { state: false, qte: undefined },
        Zébu: { state: false, qte: undefined },
        Porc: { state: false, qte: undefined },
        'Chèvre/mouton': { state: false, qte: undefined },
        'Poulet gasy': { state: false, qte: undefined },
        Rûche: { state: false, qte: undefined },
        'Matériel de pêche: pirogue, filet': { state: false, qte: undefined }
    },
    srpt_data: {
        sr: {
            'Salaire de la fonction publique': undefined,
            'Salaire du secteur privé': undefined,
            'Autre emploi': undefined,
            'Revenus agricoles': undefined,
            'Revenus d\'artisanat': undefined,
            'Revenus du petit commerce': undefined,
            'Transfert social': undefined,
            'Envoi de fonds': undefined,
            Don: undefined
        },
        qcmrar: undefined,
        pt: {
            'Superficie possédée': undefined,
            'Superficie louée ou empruntée': undefined,
            'Superficie cultivée': undefined
        },
        tc1: { type: undefined, qte: undefined },
        tc2: { type: undefined, qte: undefined },
        tc3: { type: undefined, qte: undefined }
    },
    da_data: {
        main: {
            Semences: { mantant: undefined, fourniseur: undefined },
            'Main d’œuvre (0 si main d\'œuvre gratuit)': {
                mantant: undefined,
                fourniseur: undefined
            },
            Pesticides: { mantant: undefined, fourniseur: undefined },
            Fertilisants: { mantant: undefined, fourniseur: undefined },
            'Achat de matériel et équipements': {
                mantant: undefined,
                fourniseur: undefined
            },
            'Location de matériel et équipements': {
                mantant: undefined,
                fourniseur: undefined
            }
        },
        autre: [{ name: undefined, mantant: undefined, fourniseur: undefined }]
    },
    acaa_data: {
        aac: false,
        aacname: { AA02: undefined, AA03: undefined, AA04: undefined },
        afil: false,
        imf: false,
        imfname: undefined,
        imfmontant: undefined,
        imfprop: undefined,
        notimf: undefined,
        authercred: undefined,
        commercant: false,
        autre: undefined
    },
    id1_data: [],
    educres_data: {
        primary: undefined,
        nombredanner: undefined,
        goscool: undefined,
        malagasy: undefined,
        malagasyecrit: undefined,
        sixmouth: undefined,
        annee: undefined
    },
    ep_data: {
        principa_empl: undefined
    },
    hmc_data: {
        lunette: undefined,
        audition: undefined,
        walking: undefined,
        brain: undefined,
        selfcare: undefined,
        maternallng: undefined,
        caring: undefined,
        chronicdeaseas: undefined,
        chronicdeaseasauth: undefined,
        diffic: undefined
    },
    validatiaon_data: {
        chef: undefined,
        persone: undefined,
        quality: undefined
    }
};
var stepList = [
    'Identification Enqueteur',
    'IDENTIFICATION GEOGRAPHIQUE',
    'IDENTIFICATION MENAGE',
    'CONDITIONS D\'HABITATIONS',
    'Biens possédés',
    'Sources de revenus et possession de terres',
    'Dépenses liées à l’agriculture',
    'Accès au crédit',
    'IDENTIFICATION MENAGE',
    'EDUCATION ET RESIDENCE',
    'EMPLOI PRINCIPAL',
    'HANDICAP ET MALADIE CHRONIQUE',
    'VALIDATION',
];

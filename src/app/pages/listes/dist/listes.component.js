"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ListesComponent = void 0;
var emit_event_service_1 = require("./../../Services/emit-event.service");
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
var sync_services_1 = require("./../../Services/ApiServices/sync.services");
var pouchdb_1 = require("pouchdb");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var card_services_1 = require("src/app/Services/ApiServices/card.services");
var locals_1 = require("src/app/Services/locals");
var storage_services_1 = require("src/app/Services/ApiServices/storage.services");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var Statistics_sercives_1 = require("src/app/Services/ApiServices/Statistics.sercives");
var ListesComponent = /** @class */ (function () {
    function ListesComponent(toast, router, connectionService, myEventEmiter) {
        var _this = this;
        this.toast = toast;
        this.router = router;
        this.connectionService = connectionService;
        this.myEventEmiter = myEventEmiter;
        this.text_data = locals_1.ALL_TEXT;
        this.uname = new forms_1.FormControl();
        this.filter_keyword = '';
        this.filter_option = filterData;
        this.card_list = [];
        this.selected_filter = 0;
        this.curent_page = 1;
        this.page_limit = 10;
        this.page_index = 0;
        this.element_count = 0;
        this.isLoading = false;
        this.screen = { x: 0, y: 0 };
        this.bpObs = 'L';
        this.sload1 = false;
        this.sload2 = false;
        this.sload3 = false;
        this.sload4 = false;
        this.sload5 = false;
        this.sload6 = false;
        this.sload7 = false;
        this.mobileTabIndex = 1;
        this.isConnected = false;
        this.cardDb = new pouchdb_1["default"]('card_item');
        this.pass_hide = true;
        this.card_service = new card_services_1.CardDataService();
        this.connectionService.monitor().subscribe(function (isConnected) {
            _this.isConnected = isConnected;
            if (_this.isConnected) {
                _this.noInternetConnection = false;
            }
            else {
                _this.noInternetConnection = true;
            }
        });
    }
    ListesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.intiScreendata();
        this.myEventEmiter.eventEmitter.subscribe(function (event) {
            console.log(event);
            if (event === emit_event_service_1.SYNC_SUCCESS) {
                _this.getDatas();
            }
        });
        this.getDatas();
        sync_services_1.testConnextion().then(function (connex) {
            if (connex.state) {
                _this.isConnected = true;
            }
            else {
                _this.isConnected = false;
            }
        });
        this.sload1 = true;
        Statistics_sercives_1.getStateById(4).then(function (rep) {
            _this.op1 = rep;
            _this.sload1 = false;
        });
        this.sload2 = true;
        Statistics_sercives_1.getStateById(5).then(function (rep) {
            _this.op1a = rep;
            _this.sload2 = false;
        });
        this.sload3 = true;
        Statistics_sercives_1.getStateById(6).then(function (rep) {
            _this.op1b = rep;
            _this.sload3 = false;
        });
        this.sload4 = true;
        Statistics_sercives_1.getStateById(7).then(function (rep) {
            _this.op1c = rep;
            _this.sload4 = false;
        });
        this.sload5 = true;
        Statistics_sercives_1.getStateById(8).then(function (rep) {
            _this.op1d = rep;
            _this.sload5 = false;
        });
        this.sload6 = true;
        Statistics_sercives_1.getStateById(9).then(function (rep) {
            _this.op2 = rep;
            _this.sload6 = false;
        });
        this.sload7 = true;
        Statistics_sercives_1.getStateById(10).then(function (rep) {
            _this.op3 = rep;
            _this.sload7 = false;
        });
    };
    ListesComponent.prototype.onPageChange = function (page) {
        if (page) {
            this.curent_page = page.pageIndex + 1;
            if (page.pageSize !== this.page_limit) {
                this.curent_page = 1;
            }
            this.page_limit = page.pageSize;
            this.getDatas();
        }
        return page;
    };
    ListesComponent.prototype.serachByKeyword = function () {
        this.getDatas();
    };
    ListesComponent.prototype.getQRdata = function (param) {
        return param.id + "\\" + param.detailId + "\\" + param.nis;
    };
    ListesComponent.prototype.onFilterChange = function (newVal) {
        this.selected_filter = newVal;
        this.getDatas();
    };
    ListesComponent.prototype.handleLogOut = function () {
        storage_services_1.deletUserStorage();
        this.router.navigate([locals_1.ROUT_PATH.login]);
    };
    ListesComponent.prototype.handleOnAdd = function () {
        this.router.navigate([locals_1.ROUT_PATH.formulaire]);
    };
    ListesComponent.prototype.handleTabChange = function (index) {
        this.mobileTabIndex = index;
    };
    ListesComponent.prototype.onResize = function () {
        this.intiScreendata();
    };
    ListesComponent.prototype.intiScreendata = function () {
        this.bpObs = screen_service_1.getScreenSize();
        this.screen = screen_service_1.getScreenValue();
        //console.log(this.bpObs, this.screen)
    };
    ListesComponent.prototype.getDatas = function () {
        var _this = this;
        this.isLoading = true;
        var data_filter = {
            page: this.curent_page,
            lim: this.page_limit,
            filter: {
                prenom: this.filter_keyword
            }
        };
        if (this.filter_keyword.length <= 0) {
            delete data_filter.filter;
        }
        this.card_service
            .getAll({
            page: this.curent_page,
            lim: this.page_limit,
            filter: {
                prenom: this.filter_keyword
            }
        })
            .then(function (rowCardRes) {
            var cardRes = rowCardRes.data;
            //console.log('page datas ===> ',cardRes);
            if (rowCardRes.error || rowCardRes.message) {
                _this.isLoading = false;
                _this.toast.error(rowCardRes.message);
            }
            else {
                var f_cards = _this.fiterData(cardRes.data, _this.selected_filter);
                _this.initImages(0, f_cards.length, f_cards);
                _this.card_list = f_cards;
                _this.curent_page = cardRes.page;
                _this.page_index = cardRes.page - 1;
                _this.page_limit = cardRes.lim;
                _this.element_count = cardRes.count;
                _this.isLoading = false;
            }
        })["catch"](function (err) {
            _this.isLoading = false;
            _this.toast.error(err);
        });
    };
    ListesComponent.prototype.initImages = function (i, lim, cards) {
        var _this = this;
        if (i < lim) {
            if (!cards[i].offline) {
                this.cardDb
                    .getAttachment('profile-picture-' + cards[i].nis + '-' + cards[i].id, cards[i].nis + '-' + cards[i].id)
                    .then(function (blobOrBuffer) {
                    //console.log('Image Blobe ===> ', URL.createObjectURL(blobOrBuffer));
                    cards[i].pict = URL.createObjectURL(blobOrBuffer);
                    _this.initImages(i + 1, lim, cards);
                })["catch"](function (err) {
                    console.log('Image Blobe ERROR ===> ', err);
                });
            }
            else {
                cards[i].pict = URL.createObjectURL(cards[i].pict);
                this.initImages(i + 1, lim, cards);
            }
        }
    };
    ListesComponent.prototype.fiterData = function (data, type) {
        if (type === 1) {
            return data
                .sort(function (a, b) {
                var na = a.nom.toUpperCase();
                var nb = b.nom.toUpperCase();
                return na < nb ? 1 : na > nb ? -1 : 0;
            })
                .reverse();
        }
        else {
            return data
                .sort(function (a, b) {
                var na = new Date(a.updatedAt);
                var nb = new Date(b.updatedAt);
                return na < nb ? -1 : na > nb ? 1 : 0;
            })
                .reverse();
        }
    };
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], ListesComponent.prototype, "onResize");
    ListesComponent = __decorate([
        core_1.Component({
            selector: 'app-listes',
            templateUrl: './listes.component.html',
            styleUrls: ['./listes.component.css']
        })
    ], ListesComponent);
    return ListesComponent;
}());
exports.ListesComponent = ListesComponent;
var filterData = [
    {
        id: 0,
        name: 'Date de création.',
        data: null
    },
    {
        id: 1,
        name: 'Alphabetique.',
        data: null
    },
];

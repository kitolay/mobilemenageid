import {
  EmitEventService,
  SYNC_SUCCESS,
} from './../../Services/emit-event.service';
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
import { testConnextion } from './../../Services/ApiServices/sync.services';
import PouchDB from 'pouchdb';
import { ToastrService } from 'ngx-toastr';
import { HostListener, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { DeFaultItemSelect } from 'src/app/Components/default-select/default-select.component';
import { CardData } from 'src/app/Models/AllResponsModels';
import { CardDataService } from 'src/app/Services/ApiServices/card.services';
import { ALL_TEXT, ROUT_PATH } from 'src/app/Services/locals';
import { deletUserStorage } from 'src/app/Services/ApiServices/storage.services';
import { Router } from '@angular/router';
import {
  getScreenSize,
  getScreenValue,
} from 'src/app/Services/ApiServices/screen.service';
import { getStateById } from 'src/app/Services/ApiServices/Statistics.sercives';
import { ConnectionService } from 'ng-connection-service';

@Component({
  selector: 'app-listes',
  templateUrl: './listes.component.html',
  styleUrls: ['./listes.component.css'],
})
export class ListesComponent implements OnInit {
  text_data: any = ALL_TEXT;
  uname = new FormControl();
  filter_keyword = '';
  filter_option: DeFaultItemSelect[] = filterData;
  card_list: CardData[] = [];
  card_service: CardDataService;
  selected_filter = 0;
  curent_page = 1;
  page_limit = 10;
  page_index = 0;
  element_count = 0;
  pageEvent?: PageEvent;
  isLoading = false;
  screen = { x: 0, y: 0 };
  bpObs = 'L';
  op1: any;
  op1a: any;
  op1b: any;
  op1c: any;
  op1d: any;
  op2: any;
  op3: any;
  sload1 = false;
  sload2 = false;
  sload3 = false;
  sload4 = false;
  sload5 = false;
  sload6 = false;
  sload7 = false;
  mobileTabIndex = 1;
  isConnected = false;
  noInternetConnection: boolean;
  cardDb = new PouchDB('card_item');

  pass_hide = true;
  constructor(
    private toast: ToastrService,
    public router: Router,
    private connectionService: ConnectionService,
    private myEventEmiter: EmitEventService
  ) {
    this.card_service = new CardDataService();
    this.connectionService.monitor().subscribe((isConnected) => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.noInternetConnection = false;
      } else {
        this.noInternetConnection = true;
      }
    });
  }

  ngOnInit(): void {
    this.intiScreendata();
    this.myEventEmiter.eventEmitter.subscribe((event) => {
      console.log(event);
      if (event === SYNC_SUCCESS) {
        this.getDatas();
      }
    });
    this.getDatas();

    testConnextion().then((connex) => {
      if (connex.state) {
        this.isConnected = true;
      } else {
        this.isConnected = false;
      }
    });

    this.sload1 = true;
    getStateById(4).then((rep) => {
      this.op1 = rep;
      this.sload1 = false;
    });
    this.sload2 = true;
    getStateById(5).then((rep) => {
      this.op1a = rep;
      this.sload2 = false;
    });
    this.sload3 = true;
    getStateById(6).then((rep) => {
      this.op1b = rep;
      this.sload3 = false;
    });
    this.sload4 = true;
    getStateById(7).then((rep) => {
      this.op1c = rep;
      this.sload4 = false;
    });
    this.sload5 = true;
    getStateById(8).then((rep) => {
      this.op1d = rep;
      this.sload5 = false;
    });
    this.sload6 = true;
    getStateById(9).then((rep) => {
      this.op2 = rep;
      this.sload6 = false;
    });
    this.sload7 = true;
    getStateById(10).then((rep) => {
      this.op3 = rep;
      this.sload7 = false;
    });
  }

  onPageChange(page?: PageEvent) {
    if (page) {
      this.curent_page = page.pageIndex + 1;
      if (page.pageSize !== this.page_limit) {
        this.curent_page = 1;
      }
      this.page_limit = page.pageSize;
      this.getDatas();
    }
    return page;
  }

  serachByKeyword() {
    this.getDatas();
  }

  getQRdata(param: { id: number; detailId: number; nis: string }) {
    return `${param.id}\\${param.detailId}\\${param.nis}`;
  }

  onFilterChange(newVal: number) {
    this.selected_filter = newVal;
    this.getDatas();
  }

  handleLogOut() {
    deletUserStorage();
    this.router.navigate([ROUT_PATH.login]);
  }

  handleOnAdd() {
    this.router.navigate([ROUT_PATH.formulaire]);
  }

  handleTabChange(index: number) {
    this.mobileTabIndex = index;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.intiScreendata();
  }

  intiScreendata() {
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
    //console.log(this.bpObs, this.screen)
  }

  private getDatas() {
    this.isLoading = true;
    const data_filter: any = {
      page: this.curent_page,
      lim: this.page_limit,
      filter: {
        prenom: this.filter_keyword,
      },
    };
    if (this.filter_keyword.length <= 0) {
      delete data_filter.filter;
    }

    this.card_service
      .getAll({
        page: this.curent_page,
        lim: this.page_limit,
        filter: {
          prenom: this.filter_keyword,
        },
      })
      .then((rowCardRes) => {
        const cardRes = rowCardRes.data;
        //console.log('page datas ===> ',cardRes);
        if (rowCardRes.error || rowCardRes.message) {
          this.isLoading = false;
          this.toast.error(rowCardRes.message);
        } else {
          const f_cards = this.fiterData(cardRes.data, this.selected_filter);
          this.initImages(0, f_cards.length, f_cards);
          this.card_list = f_cards;
          this.curent_page = cardRes.page;
          this.page_index = cardRes.page - 1;
          this.page_limit = cardRes.lim;
          this.element_count = cardRes.count;
          this.isLoading = false;
        }
      })
      .catch((err) => {
        this.isLoading = false;
        this.toast.error(err);
      });
  }

  private initImages(i: number, lim: number, cards: CardData[]) {
    if (i < lim) {
      if (!cards[i].offline) {
        this.cardDb
          .getAttachment(
            'profile-picture-' + cards[i].nis + '-' + cards[i].id,
            cards[i].nis + '-' + cards[i].id
          )
          .then((blobOrBuffer) => {
            //console.log('Image Blobe ===> ', URL.createObjectURL(blobOrBuffer));
            cards[i].pict = URL.createObjectURL(blobOrBuffer);
            this.initImages(i + 1, lim, cards);
          })
          .catch((err) => {
            console.log('Image Blobe ERROR ===> ', err);
          });
      } else {
        cards[i].pict = URL.createObjectURL(cards[i].pict);
        this.initImages(i + 1, lim, cards);
      }
    }
  }

  private fiterData(data: CardData[], type: number): CardData[] {
    if (type === 1) {
      return data
        .sort((a, b) => {
          const na = a.nom.toUpperCase();
          const nb = b.nom.toUpperCase();
          return na < nb ? 1 : na > nb ? -1 : 0;
        })
        .reverse();
    } else {
      return data
        .sort((a, b) => {
          const na = new Date(a.updatedAt);
          const nb = new Date(b.updatedAt);
          return na < nb ? -1 : na > nb ? 1 : 0;
        })
        .reverse();
    }
  }
}

const filterData: DeFaultItemSelect[] = [
  {
    id: 0,
    name: 'Date de création.',
    data: null,
  },
  {
    id: 1,
    name: 'Alphabetique.',
    data: null,
  },
];

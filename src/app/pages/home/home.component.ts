import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { DeFaultItemSelect } from 'src/app/Components/default-select/default-select.component';
import { CardData } from 'src/app/Models/AllResponsModels';
import { CardDataService } from 'src/app/Services/ApiServices/card.services';
import { ALL_TEXT, ROUT_PATH } from 'src/app/Services/locals';
import { deletUserStorage } from 'src/app/Services/ApiServices/storage.services';
import { Router } from '@angular/router';
import { getScreenSize } from 'src/app/Services/ApiServices/screen.service';
import { getStateById } from 'src/app/Services/ApiServices/Statistics.sercives';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  text_data: any = ALL_TEXT;
  uname = new FormControl();
  filter_keyword: string = '';
  filter_option: DeFaultItemSelect[] = filterData;
  card_list: CardData[] = [];
  card_service: CardDataService;
  selected_filter: number = 0;
  curent_page: number = 1;
  page_limit: number = 10;
  page_index: number = 0;
  element_count: number = 0;
  pageEvent?: PageEvent;
  options: any;
  op1: any;
  op2: any;
  op3: any;
  bpObs = 'L';
  oploading = false;
  op1loading = false;
  op2loading = false;
  op3loading = false;

  pass_hide: boolean = true;
  constructor(private toast: ToastrService, public router: Router) {
    this.card_service = new CardDataService();
  }

  ngOnInit(): void {
    this.getDatas();
    this.bpObs = getScreenSize();
    this.oploading = true;
    if(this.bpObs === 'XS'){
      getStateById(11).then((response) => {
        this.options = response;
        this.oploading = false;
      });
    }else{
      getStateById(0).then((response) => {
        this.options = response;
        this.oploading = false;
      });
    }
    this.op1loading = true;
    getStateById(1).then((response) => {
      this.op1 = response;
      this.op1loading = false;
    });
    this.op2loading = true;
    getStateById(2).then((response) => {
      this.op2 = response;
      this.op2loading = false;
    });
    this.op3loading = true;
    getStateById(3).then((response) => {
      this.op3 = response;
      this.op3loading = false;
    });

  }

  onPageChange(page?: PageEvent) {
    if (page) {
      this.curent_page = page.pageIndex + 1;
      this.page_limit = page.pageSize;
      this.getDatas();
    }
    return page;
  }

  serachByKeyword() {
    setTimeout(() => {
      this.getDatas();
    }, 2000);
  }

  getQRdata(param: { id: number; detailId: number; nis: string }) {
    return `${param.id}\\${param.detailId}\\${param.nis}`;
  }

  onFilterChange(newVal: number) {
    this.selected_filter = newVal;
    this.getDatas();
  }

  handleLogOut() {
    deletUserStorage();
    this.router.navigate([ROUT_PATH.login]);
  }

  handleOnAdd() {
    this.router.navigate([ROUT_PATH.formulaire]);
  }

  private getDatas() {
    var data_filter: any = {
      page: this.curent_page,
      lim: this.page_limit,
      filter: {
        prenom: this.filter_keyword,
      },
    };
    if (this.filter_keyword.length <= 0) delete data_filter.filter;

    this.card_service
      .getAll({
        page: this.curent_page,
        lim: this.page_limit,
        filter: {
          prenom: this.filter_keyword,
        },
      })
      .then((rowCardRes) => {
        // console.log("card data res ====> ", cardRes)
        let cardRes = rowCardRes.data;
        if (rowCardRes.error || rowCardRes.message) {
        } else {
          this.card_list = this.fiterData(cardRes.data, this.selected_filter);
          this.curent_page = cardRes.page;
          this.page_limit = cardRes.lim;
          this.element_count = cardRes.count;
        }
      })
      .catch((err) => {
        this.toast.error(err);
      });
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.bpObs = getScreenSize();
  }

  private fiterData(data: CardData[], type: number): CardData[] {
    if (type === 1)
      return data.sort((a, b) => {
        let na = a.nom.toUpperCase();
        let nb = b.nom.toUpperCase();
        return na < nb ? -1 : na > nb ? 1 : 0;
      });
    else
      return data.sort((a, b) => {
        let na = new Date(a.updatedAt);
        let nb = new Date(b.updatedAt);
        return na < nb ? -1 : na > nb ? 1 : 0;
      });
  }
}

const filterData: DeFaultItemSelect[] = [
  {
    id: 0,
    name: 'Date de création.',
    data: null,
  },
  {
    id: 1,
    name: 'Alphabetique.',
    data: null,
  },
];

const optiondata1 = {
  title: {
    text: 'Répartition sphérique',
    subtext: 'pourcentage',
    left: 'center',
  },
  tooltip: {
    trigger: 'item',
    formatter: '{a} <br/>{b} : {c} ({d}%)',
  },
  legend: {
    orient: 'vertical',
    left: 'left',
    data: [
      'Mahajanga',
      'Antsiranana',
      'Toamasina',
      'Toliara',
      'Antananarivo',
      'Antsirabe',
    ],
    top: '35px',
  },
  series: [
    {
      name: 'Effectifs',
      type: 'pie',
      radius: '55%',
      center: ['50%', '60%'],
      data: [
        { value: 335, name: 'Mahajanga' },
        { value: 310, name: 'Antsiranana' },
        { value: 234, name: 'Toamasina' },
        { value: 135, name: 'Toliara' },
        { value: 1248, name: 'Antananarivo' },
        { value: 300, name: 'Antsirabe' },
      ],
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
  ],
};

const option2 = {
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      // 坐标轴指示器，坐标轴触发有效
      type: 'shadow', // 默认为直线，可选为：'line' | 'shadow'
    },
  },
  legend: {
    data: [
      'Mahajanga',
      'Antsiranana',
      'Toamasina',
      'Toliara',
      'Antananarivo',
      'Antsirabe',
    ],
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true,
  },
  xAxis: [
    {
      type: 'category',
      data: ['2015', '2016', '2017', '2018', '2019', '2020', '2021'],
    },
  ],
  yAxis: [
    {
      type: 'value',
    },
  ],
  series: [
    {
      name: 'Mahajanga',
      type: 'bar',
      data: [320, 332, 301, 334, 390, 330, 320],
    },
    {
      name: 'Antsiranana',
      type: 'bar',
      stack: '广告',
      data: [120, 132, 101, 134, 90, 230, 210],
    },
    {
      name: 'Toamasina',
      type: 'bar',
      stack: '广告',
      data: [220, 182, 191, 234, 290, 330, 310],
    },
    {
      name: 'Toliara',
      type: 'bar',
      stack: '广告',
      data: [150, 232, 201, 154, 190, 330, 410],
    },
    {
      name: 'Antananarivo',
      type: 'bar',
      data: [862, 1018, 964, 1026, 1679, 1600, 1570],
      markLine: {
        lineStyle: {
          type: 'dashed',
        },
        data: [[{ type: 'min' }, { type: 'max' }]],
      },
    },
    {
      name: 'Antsirabe',
      type: 'bar',
      barWidth: 5,
      stack: '搜索引擎',
      data: [620, 732, 701, 734, 1090, 1130, 1120],
    },
  ],
};

const option4 = {
  title: {
    text: 'Ménages',
  },
  tooltip: {
    trigger: 'axis',
  },
  legend: {
    data: [
      'Mahajanga',
      'Antsiranana',
      'Toamasina',
      'Toliara',
      'Antananarivo',
      'Antsirabe',
    ],
    top: '35px',
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true,
    top: '100px',
  },
  toolbox: {
    feature: {
      saveAsImage: {},
    },
  },
  xAxis: {
    type: 'category',
    boundaryGap: false,
    data: ['2015', '2016', '2017', '2018', '2019', '2020', '2021'],
  },
  yAxis: {
    type: 'value',
  },
  series: [
    {
      name: 'Mahajanga',
      type: 'line',
      stack: '总量',
      data: [120, 132, 101, 134, 90, 230, 210],
    },
    {
      name: 'Antsiranana',
      type: 'line',
      stack: '总量',
      data: [220, 182, 191, 234, 290, 330, 310],
    },
    {
      name: 'Toamasina',
      type: 'line',
      stack: '总量',
      data: [150, 232, 201, 154, 190, 330, 410],
    },
    {
      name: 'Toliara',
      type: 'line',
      stack: '总量',
      data: [320, 332, 301, 334, 390, 330, 320],
    },
    {
      name: 'Antananarivo',
      type: 'line',
      stack: '总量',
      data: [820, 932, 901, 934, 1290, 1330, 1320],
    },
    {
      name: 'Antsirabe',
      type: 'line',
      stack: '总量',
      data: [820, 932, 901, 934, 1290, 1330, 1320],
    },
  ],
};

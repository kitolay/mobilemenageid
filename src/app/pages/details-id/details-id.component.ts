/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
import { off_db_name } from 'src/app/Services/offline/card.services';
import PouchDB from 'pouchdb';
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HostListener } from '@angular/core';
import { CardDataService } from 'src/app/Services/ApiServices/card.services';
import { CardFormDataDetail } from 'src/app/Models/AllResponsModels';
import { ALL_TEXT } from 'src/app/Services/locals';
import {
  getScreenSize,
  getScreenValue,
} from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-details-id',
  templateUrl: './details-id.component.html',
  styleUrls: ['./details-id.component.css'],
})
export class DetailsIdComponent implements OnInit {
  screen = { x: 0, y: 0 };
  bpObs = 'L';

  all_text = ALL_TEXT;
  isLoading = false;
  offsetFlag = false;
  userId: any;
  formData?: CardFormDataDetail;
  cardService: CardDataService = new CardDataService();
  constructor(
    public activatedRoute: ActivatedRoute,
    public toast: ToastrService
  ) {}

  ngOnInit(): void {
    this.intiScreendata();
    this.userId = this.activatedRoute.snapshot.params.id;
    if (typeof this.userId === 'string' && this.userId.length > 10) {
      const offdb = new PouchDB(off_db_name);
      try {
        this.isLoading = true;
        offdb.get((this.userId as string).replace(/&/g,'\/')).then((data)=>{
          const card_item = data.card_item;
          card_item.pict = URL.createObjectURL(card_item.pict);
          const data_detail = { ...data };
          delete data_detail.card_item;
          this.formData = { ...card_item, details: { ...data_detail } } as CardFormDataDetail;
          this.isLoading = false;
        });
      } catch (error) {
        this.isLoading = false;
        console.log(error);
        this.toast.error(error);
      }
    } else {
      if (this.userId && this.userId > 0) {
        this.isLoading = true;
        this.cardService
          .getDetails(this.userId)
          .then((result) => {
            if (result) {
              this.formData = result as CardFormDataDetail;
              const cardDb = new PouchDB('card_item');
              cardDb
                .getAttachment('profile-picture-'+this.formData.nis +'-'+ this.formData.id,
                this.formData.nis +'-'+ this.formData.id)
                .then((blobOrBuffer) => {
                  //console.log('Image Blobe ===> ', URL.createObjectURL(blobOrBuffer));
                  this.formData.pict = URL.createObjectURL(blobOrBuffer);
                })
                .catch((err) => {
                  console.log('Image Blobe ERROR ===> ','profile-picture-'+this.formData.nis +'-'+ this.formData.id,
                  this.formData.nis +'-'+ this.formData.id, err);
                });
            } else {
              this.toast.error('Erreur de donner.');
            }
            this.isLoading = false;
          })
          .catch((err) => {
            this.toast.error(err);
            this.isLoading = false;
          });
      }
    }
  }

  getPMT(textFrom?: string): string {
    if (textFrom) {
      const divided_text = textFrom.split('-');
      if (divided_text.length > 1) {
        return divided_text[0];
      } else {
        return 'N/A';
      }
    } else {
      return 'N/A';
    }
  }

  getPrifilePicture(value: string | undefined): string {
    if (value) {
      return value;
    } else {
      return 'none';
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.intiScreendata();
  }

  intiScreendata() {
    this.bpObs = getScreenSize();
    this.screen = getScreenValue();
    //console.log(this.bpObs, this.screen)
  }

  @HostListener('window:scroll', ['$event']) getScrollHeight(event: any) {
    if (window.pageYOffset > 500) {
      this.offsetFlag = true;
    } else {
      this.offsetFlag = false;
    }
  }
}

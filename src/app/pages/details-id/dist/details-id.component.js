"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DetailsIdComponent = void 0;
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
var card_services_1 = require("src/app/Services/offline/card.services");
var pouchdb_1 = require("pouchdb");
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var card_services_2 = require("src/app/Services/ApiServices/card.services");
var locals_1 = require("src/app/Services/locals");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var DetailsIdComponent = /** @class */ (function () {
    function DetailsIdComponent(activatedRoute, toast) {
        this.activatedRoute = activatedRoute;
        this.toast = toast;
        this.screen = { x: 0, y: 0 };
        this.bpObs = 'L';
        this.all_text = locals_1.ALL_TEXT;
        this.isLoading = false;
        this.offsetFlag = false;
        this.cardService = new card_services_2.CardDataService();
    }
    DetailsIdComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.intiScreendata();
        this.userId = this.activatedRoute.snapshot.params.id;
        if (typeof this.userId === 'string' && this.userId.length > 10) {
            var offdb = new pouchdb_1["default"](card_services_1.off_db_name);
            try {
                this.isLoading = true;
                offdb.get(this.userId.replace(/&/g, '\/')).then(function (data) {
                    var card_item = data.card_item;
                    card_item.pict = URL.createObjectURL(card_item.pict);
                    var data_detail = __assign({}, data);
                    delete data_detail.card_item;
                    _this.formData = __assign(__assign({}, card_item), { details: __assign({}, data_detail) });
                    _this.isLoading = false;
                });
            }
            catch (error) {
                this.isLoading = false;
                console.log(error);
                this.toast.error(error);
            }
        }
        else {
            if (this.userId && this.userId > 0) {
                this.isLoading = true;
                this.cardService
                    .getDetails(this.userId)
                    .then(function (result) {
                    if (result) {
                        _this.formData = result;
                        var cardDb = new pouchdb_1["default"]('card_item');
                        cardDb
                            .getAttachment('profile-picture-' + _this.formData.nis + '-' + _this.formData.id, _this.formData.nis + '-' + _this.formData.id)
                            .then(function (blobOrBuffer) {
                            //console.log('Image Blobe ===> ', URL.createObjectURL(blobOrBuffer));
                            _this.formData.pict = URL.createObjectURL(blobOrBuffer);
                        })["catch"](function (err) {
                            console.log('Image Blobe ERROR ===> ', 'profile-picture-' + _this.formData.nis + '-' + _this.formData.id, _this.formData.nis + '-' + _this.formData.id, err);
                        });
                    }
                    else {
                        _this.toast.error('Erreur de donner.');
                    }
                    _this.isLoading = false;
                })["catch"](function (err) {
                    _this.toast.error(err);
                    _this.isLoading = false;
                });
            }
        }
    };
    DetailsIdComponent.prototype.getPMT = function (textFrom) {
        if (textFrom) {
            var divided_text = textFrom.split('-');
            if (divided_text.length > 1) {
                return divided_text[0];
            }
            else {
                return 'N/A';
            }
        }
        else {
            return 'N/A';
        }
    };
    DetailsIdComponent.prototype.getPrifilePicture = function (value) {
        if (value) {
            return value;
        }
        else {
            return 'none';
        }
    };
    DetailsIdComponent.prototype.onResize = function () {
        this.intiScreendata();
    };
    DetailsIdComponent.prototype.intiScreendata = function () {
        this.bpObs = screen_service_1.getScreenSize();
        this.screen = screen_service_1.getScreenValue();
        //console.log(this.bpObs, this.screen)
    };
    DetailsIdComponent.prototype.getScrollHeight = function (event) {
        if (window.pageYOffset > 500) {
            this.offsetFlag = true;
        }
        else {
            this.offsetFlag = false;
        }
    };
    __decorate([
        core_2.HostListener('window:resize', ['$event'])
    ], DetailsIdComponent.prototype, "onResize");
    __decorate([
        core_2.HostListener('window:scroll', ['$event'])
    ], DetailsIdComponent.prototype, "getScrollHeight");
    DetailsIdComponent = __decorate([
        core_1.Component({
            selector: 'app-details-id',
            templateUrl: './details-id.component.html',
            styleUrls: ['./details-id.component.css']
        })
    ], DetailsIdComponent);
    return DetailsIdComponent;
}());
exports.DetailsIdComponent = DetailsIdComponent;

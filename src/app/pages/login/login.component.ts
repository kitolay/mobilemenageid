/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
import { HostListener, Component, OnInit, Input } from '@angular/core';
//import { authentUser } from 'src/app/Services/ApiServices/Comment.sercice';
import { ToastrService } from 'ngx-toastr';
import { FormControl, Validators } from '@angular/forms';
import { ALL_TEXT, ROUT_PATH } from 'src/app/Services/locals';
//import { saveUser } from 'src/app/Services/ApiServices/storage.services';
import { Router } from '@angular/router';
import { AuthentUserService } from 'src/app/Services/ApiServices/authent.services';
import { getUserSessionEpired } from 'src/app/Services/ApiServices/storage.services';
import { getScreenSize } from 'src/app/Services/ApiServices/screen.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  @Input() IsConnected?: boolean;
  @Input() IsSyncronising?: number;

  user_service: AuthentUserService;
  uname = new FormControl('', [Validators.required]);
  upass = new FormControl('', [Validators.required]);
  text_data: any = ALL_TEXT;
  pass_hide = true;
  user_name?: string = '';
  pass_text?: string = '';
  is_loading?: boolean = false;
  bpObs = 'XS';

  constructor(
    private toastr: ToastrService,
    public router: Router
  ) {
    this.user_service = new AuthentUserService();
    this.handleLogUser = this.handleLogUser.bind(this);
  }

  ngOnInit(): void {
    this.bpObs = getScreenSize();
    //console.log("Screen sise ===> ", this.bpObs)
    if (!getUserSessionEpired()) {this.router.navigate([ROUT_PATH.home]);}
  }

  getStatueText(state: number){
    if(state === 0) {return 'Vous êtes en ligne';}
    if(state === 1) {return 'Vous êtes en mode "offline"';}
    if(state === 2) {return 'Synchronisation en cours ...';}
  }

  handleLogUser = () => {
    if (this.user_name && this.pass_text) {
      this.is_loading = true;
      this.user_service
        .login(this.user_name, this.pass_text)
        .then((res) => {
          if (res.data && res.data.user && res.data.tokken) {
            this.user_service
              .setSession(res.data.tokken, res.data.user)
              .then((updated) => {
                if (updated) {
                  this.is_loading = false;
                  this.router.navigate([ROUT_PATH.home]);
                }
              });
          } else {
            console.log(res);
            this.toastr.error(ALL_TEXT._login_error);
            this.is_loading = false;
          }
        })
        .catch((err) => {
          console.log(err);
          this.toastr.error(ALL_TEXT._errcon);
          this.is_loading = false;
        });
    } else {
      this.toastr.error(ALL_TEXT._login_error);
    }
  };

  handleRecover() {
    this.router.navigate([ROUT_PATH.recover]);
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    console.log('Screen sise ===> ', this.bpObs);
    this.bpObs = getScreenSize();
  }
}

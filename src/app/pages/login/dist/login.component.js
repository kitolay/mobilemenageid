"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.LoginComponent = void 0;
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var locals_1 = require("src/app/Services/locals");
var authent_services_1 = require("src/app/Services/ApiServices/authent.services");
var storage_services_1 = require("src/app/Services/ApiServices/storage.services");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(toastr, router) {
        var _this = this;
        this.toastr = toastr;
        this.router = router;
        this.uname = new forms_1.FormControl('', [forms_1.Validators.required]);
        this.upass = new forms_1.FormControl('', [forms_1.Validators.required]);
        this.text_data = locals_1.ALL_TEXT;
        this.pass_hide = true;
        this.user_name = '';
        this.pass_text = '';
        this.is_loading = false;
        this.bpObs = 'XS';
        this.handleLogUser = function () {
            if (_this.user_name && _this.pass_text) {
                _this.is_loading = true;
                _this.user_service
                    .login(_this.user_name, _this.pass_text)
                    .then(function (res) {
                    if (res.data && res.data.user && res.data.tokken) {
                        _this.user_service
                            .setSession(res.data.tokken, res.data.user)
                            .then(function (updated) {
                            if (updated) {
                                _this.is_loading = false;
                                _this.router.navigate([locals_1.ROUT_PATH.home]);
                            }
                        });
                    }
                    else {
                        console.log(res);
                        _this.toastr.error(locals_1.ALL_TEXT._login_error);
                        _this.is_loading = false;
                    }
                })["catch"](function (err) {
                    console.log(err);
                    _this.toastr.error(locals_1.ALL_TEXT._errcon);
                    _this.is_loading = false;
                });
            }
            else {
                _this.toastr.error(locals_1.ALL_TEXT._login_error);
            }
        };
        this.user_service = new authent_services_1.AuthentUserService();
        this.handleLogUser = this.handleLogUser.bind(this);
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.bpObs = screen_service_1.getScreenSize();
        //console.log("Screen sise ===> ", this.bpObs)
        if (!storage_services_1.getUserSessionEpired()) {
            this.router.navigate([locals_1.ROUT_PATH.home]);
        }
    };
    LoginComponent.prototype.getStatueText = function (state) {
        if (state === 0) {
            return 'Vous êtes en ligne';
        }
        if (state === 1) {
            return 'Vous êtes en mode "offline"';
        }
        if (state === 2) {
            return 'Synchronisation en cours ...';
        }
    };
    LoginComponent.prototype.handleRecover = function () {
        this.router.navigate([locals_1.ROUT_PATH.recover]);
    };
    LoginComponent.prototype.onResize = function () {
        console.log('Screen sise ===> ', this.bpObs);
        this.bpObs = screen_service_1.getScreenSize();
    };
    __decorate([
        core_1.Input()
    ], LoginComponent.prototype, "IsConnected");
    __decorate([
        core_1.Input()
    ], LoginComponent.prototype, "IsSyncronising");
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], LoginComponent.prototype, "onResize");
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        })
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;

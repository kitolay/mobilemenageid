/* eslint-disable @typescript-eslint/naming-convention */
import PouchDB from 'pouchdb';
export const getProvinceOff = (): Promise<any> | null =>
  new Promise((res, rej) => {
    const place_db = new PouchDB('liste_province');
    if (place_db) {
      try {
        place_db
          .allDocs({
            include_docs: true,
          })
          .then((result) => {
            res(result.rows.map((r) => r.doc));
          });
      } catch (err) {
        console.log('liste_province_error: ', err);
        res(null);
      }
    } else {
      res(null);
    }
  });

export const getRegionOff = (regionfilter: any): Promise<any> | null =>
  new Promise((res, rej) => {
    const place_db = new PouchDB('liste_region');
    if (place_db) {
      try {
        place_db
          .allDocs({
            include_docs: true,
          })
          .then((result) => {
            res(
              result.rows
                .map((r) => r.doc)
                .filter((r) => r.prov_id === regionfilter.prov_id)
            );
          });
      } catch (err) {
        console.log('liste_province_error: ', err);
        res(null);
      }
    } else {
      res(null);
    }
  });

export const getDistrictOff = (districtfilter: any): Promise<any> =>
  new Promise((res, rej) => {
    const place_db = new PouchDB('list_district');
    if (place_db) {
      try {
        place_db
          .allDocs({
            include_docs: true,
          })
          .then((result) => {
            console.log('district result ===> ', result);
            res(
              result.rows
                .map((r) => r.doc)
                .filter(
                  (r) =>
                    r.prov_id === districtfilter.prov_id &&
                    r.reg_id === districtfilter.reg_id
                )
            );
          });
      } catch (err) {
        console.log('liste_province_error: ', err);
        res(null);
      }
    } else {
      res(null);
    }
  });

export const getComunneOff = (comunfilter: any): Promise<any> =>
  new Promise((res, rej) => {
    const place_db = new PouchDB('list_commune');
    if (place_db) {
      try {
        place_db
          .allDocs({
            include_docs: true,
          })
          .then((result) => {
            res(
              result.rows
                .map((r) => r.doc)
                .filter(
                  (r) =>
                    r.prov_id === comunfilter.prov_id &&
                    r.reg_id === comunfilter.reg_id &&
                    r.dis_id === comunfilter.dis_id
                )
            );
          });
      } catch (err) {
        console.log('liste_province_error: ', err);
        res(null);
      }
    } else {
      res(null);
    }
  });

export const getFokontanyOff = (fokotanyfilter: any): Promise<any> =>
  new Promise((res, rej) => {
    const place_db = new PouchDB('list_fokontany');
    if (place_db) {
      try {
        place_db
          .allDocs({
            include_docs: true,
          })
          .then((result) => {
            res(
              result.rows
                .map((r) => r.doc)
                .filter(
                  (r) =>
                    r.prov_id === fokotanyfilter.prov_id &&
                    r.reg_id === fokotanyfilter.reg_id &&
                    r.dis_id === fokotanyfilter.dis_id &&
                    r.com_id === fokotanyfilter.com_id
                )
            );
          });
      } catch (err) {
        console.log('liste_province_error: ', err);
        res(null);
      }
    } else {
      res(null);
    }
  });

"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
exports.authenthUserOff = void 0;
var pouchdb_1 = require("pouchdb");
exports.authenthUserOff = function (pass, userName) {
    return new Promise(function (res, rej) {
        var auth_db = new pouchdb_1["default"]('user_data');
        if (auth_db) {
            auth_db
                .allDocs({
                include_docs: true,
                attachments: true
            })
                .then(function (result) {
                if (result && result.rows) {
                    var user_1 = result.rows.find(function (itm) { return itm.doc.user_name === userName; });
                    if (user_1 && user_1.doc) {
                        if (user_1.doc.pass === pass) {
                            var sup_db = new pouchdb_1["default"]('supervisor_data');
                            if (sup_db) {
                                sup_db
                                    .allDocs({
                                    include_docs: true,
                                    attachments: true
                                })
                                    .then(function (sup) {
                                    if (sup && sup.rows) {
                                        var sup_user = sup.rows.find(function (superviseur) {
                                            return superviseur.doc.id === user_1.doc.superviseur;
                                        });
                                        if (sup_user && sup_user.doc) {
                                            // eslint-disable-next-line prefer-const
                                            var userFinal = __assign({}, user_1.doc);
                                            delete userFinal._id;
                                            delete userFinal._rev;
                                            delete userFinal.pass;
                                            userFinal.superviseur = sup_user.doc;
                                            res({
                                                data: {
                                                    user: userFinal,
                                                    tokken: 'q0vbUaF5PuOazwLfvQQp'
                                                }
                                            });
                                        }
                                        else {
                                            res(null);
                                        }
                                    }
                                    else {
                                        res(null);
                                    }
                                });
                            }
                            else {
                                res(null);
                            }
                        }
                        else {
                            res(null);
                        }
                    }
                    else {
                        res(null);
                    }
                }
                else {
                    res(null);
                }
            })["catch"](function (err) {
                console.log(err);
            });
        }
        else {
            res(null);
        }
    });
};

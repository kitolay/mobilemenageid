"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.getDetailDta = exports.getCardListsOff = exports.off_db_name = void 0;
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
var pouchdb_1 = require("pouchdb");
var tableList = [
    'enqueteur_data',
    'ig_data',
    'id_data',
    'ch_data',
    'ap_data',
    'srpt_data',
    'da_data',
    'acaa_data',
    'educres_data',
    'ep_data',
    'hmc_data',
    'validatiaon_data',
];
var id1_data = 'id1_data';
exports.off_db_name = 'new_offline_data';
exports.getCardListsOff = function (params) {
    return new Promise(function (res, rej) {
        var cardDb = new pouchdb_1["default"]('card_item');
        if (cardDb) {
            cardDb
                .allDocs({
                include_docs: true
            })
                .then(function (cardRows) {
                if (cardRows && cardRows.rows) {
                    var off_db = new pouchdb_1["default"](exports.off_db_name);
                    var offlineCards_1;
                    if (off_db) {
                        off_db
                            .allDocs({
                            include_docs: true
                        })
                            .then(function (datas) {
                            try {
                                offlineCards_1 = datas.rows.map(function (r) { return r.doc.card_item; });
                            }
                            catch (error) {
                                console.log(error);
                            }
                        })["finally"](function () {
                            var allrowData = cardRows.rows.map(function (row) { return row.doc; });
                            if (offlineCards_1 && offlineCards_1.length) {
                                allrowData = __spreadArrays(allrowData, offlineCards_1);
                            }
                            var all_cards = allrowData.filter(function (c) { return c && !c._attachments; });
                            if (all_cards && all_cards.length > 0) {
                                var filtered_data = all_cards.filter(function (card) {
                                    if (card.prenom) {
                                        //console.log('prenom filter', params.filter.prenom);
                                        return card.prenom.includes(params.filter.prenom);
                                    }
                                    else {
                                        return false;
                                    }
                                }).sort(function (a, b) { return b.id - a.id; });
                                res({
                                    data: {
                                        data: paginate(filtered_data, params.lim, params.page).reverse(),
                                        page: params.page,
                                        lim: params.lim,
                                        count: filtered_data.length
                                    }
                                });
                            }
                            else {
                                res(null);
                            }
                        });
                    }
                }
                else {
                    res(null);
                }
            })["catch"](function (err) {
                console.log(err);
                res(err);
            });
        }
        else {
            res(null);
        }
    });
};
exports.getDetailDta = function (cardId) {
    return new Promise(function (res, rej) {
        var cardDb = new pouchdb_1["default"]('card_item');
        if (cardDb) {
            cardDb
                .allDocs({
                include_docs: true
            })
                .then(function (cardRows) {
                if (cardRows && cardRows.rows) {
                    var card_data_1 = cardRows.rows
                        .map(function (c) { return c.doc; })
                        .filter(function (c) { return !c._attachments; })
                        .find(function (c) { return c.id === cardId * 1; });
                    if (card_data_1) {
                        // eslint-disable-next-line prefer-const
                        var container_1 = {};
                        tableList.forEach(function (tName) {
                            var all_db = new pouchdb_1["default"](tName);
                            if (all_db) {
                                all_db
                                    .allDocs({
                                    include_docs: true
                                })
                                    .then(function (tRow) {
                                    if (tRow && tRow.rows) {
                                        var f_row = tRow.rows
                                            .map(function (r) { return r.doc; })
                                            .find(function (r) { return r.refId === cardId * 1; });
                                        if (f_row) {
                                            container_1[tName] = f_row;
                                        }
                                        else {
                                            res(null);
                                        }
                                    }
                                    else {
                                        res(null);
                                    }
                                });
                            }
                            else {
                                res(null);
                            }
                        });
                        var id1data_table = new pouchdb_1["default"](id1_data);
                        if (id1data_table) {
                            id1data_table
                                .allDocs({
                                include_docs: true
                            })
                                .then(function (id1) {
                                try {
                                    var id_row = id1.rows
                                        .map(function (r) { return r.doc; })
                                        .filter(function (r) { return r.refId === cardId * 1; });
                                    container_1[id1_data] = id_row;
                                    res(__assign(__assign({}, card_data_1), { details: container_1 }));
                                }
                                catch (error) {
                                    res(error);
                                }
                            });
                        }
                        else {
                            console.log('card id1 error');
                            res(null);
                        }
                    }
                    else {
                        // eslint-disable-next-line max-len
                        console.log('card data error');
                        res(null);
                    }
                }
                else {
                    console.log('card rows error');
                    res(null);
                }
            });
        }
        else {
            console.log('card db error');
            res(null);
        }
    });
};
var paginate = function (array, page_size, page_number) {
    // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
    return array.slice((page_number - 1) * page_size, page_number * page_size);
};

"use strict";
exports.__esModule = true;
exports.getStateByIdOff = void 0;
/* eslint-disable no-fallthrough */
exports.getStateByIdOff = function (id) { return new Promise(function (res, rej) {
    switch (id) {
        case 0:
            res(options);
        case 1:
            res(optiondata1);
        case 2:
            res(option2);
        case 3:
            res(option4);
        case 4:
            res(option1);
        case 5:
            res(option1a);
        case 6:
            res(option1b);
        case 7:
            res(option1c);
        case 8:
            res(option1d);
        case 9:
            res(option2);
        case 10:
            res(option3);
        case 11:
            res(option5);
        default:
            res(null);
    }
}); };
var options = {
    //legend: {
    //data: ["Taux de pauvreté par région"],
    //align: "left",
    //},
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
            type: 'category',
            axisLabel: { interval: 0 },
            axisTick: {
                alignWithLabel: true
            },
            data: [
                'Itasy',
                '\nAmoron\'i\nMania',
                'Vakinankaratra',
                '\nBongolava',
                'Analamanga',
                '\nAtsimo-\nAtsinanana',
                'Diana',
                '\nAndroy',
                'Vatovavy\nFitovinany',
                '\nHaute\nMatsiatra',
                'Analanjirofo',
                '\nAtsinanana',
                'Sava',
                '\nAnôsy',
                'Ihorombe',
                '\nBetsiboka',
                'Boeny',
                '\nAlaotra\nMangoro',
                'Melaky',
                '\nMenabe',
                'Sofia',
                '\nAtsimo-\nAndrefana',
            ]
        },
    ],
    yAxis: [
        {
            type: 'value'
        },
    ],
    series: [
        {
            name: 'Taux de pauvreté',
            type: 'bar',
            data: [
                75.27, 73.44, 18.52, 46.97, 34.39, 92.30, 71.90, 75.38,
                14.54, 12.31, 10.63, 13.05, 10.07, 69.00, 32.07,
                30.14, 82.13, 10.54, 29.74, 60.81, 12.80, 13.52,
            ]
        },
    ]
};
var optiondata1 = {
    title: {
        text: 'Répartition sphérique',
        left: 'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
        bottom: 5,
        left: 'center',
        data: [
            'Mahajanga',
            'Antsiranana',
            'Toamasina',
            'Toliara',
            'Antananarivo',
            'Antsirabe',
        ]
    },
    series: [
        {
            name: 'Effectifs',
            type: 'pie',
            radius: '85%',
            label: {
                position: 'inner',
                formatter: '{d}%',
                color: '#fff',
                textStyle: {
                    fontSize: 10,
                    fontWeight: 500
                }
            },
            data: [
                { value: 335, name: 'Mahajanga' },
                { value: 310, name: 'Antsiranana' },
                { value: 234, name: 'Toamasina' },
                { value: 135, name: 'Toliara' },
                { value: 1248, name: 'Antananarivo' },
                { value: 300, name: 'Antsirabe' },
            ],
            emphasis: {
                itemStyle: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        },
    ]
};
var option2 = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            // 坐标轴指示器，坐标轴触发有效
            type: 'shadow'
        }
    },
    legend: {
        data: [
            'Mahajanga',
            'Antsiranana',
            'Toamasina',
            'Toliara',
            'Antananarivo',
            'Antsirabe',
        ]
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
            type: 'category',
            data: ['2015', '2016', '2017', '2018', '2019', '2020', '2021']
        },
    ],
    yAxis: [
        {
            type: 'value'
        },
    ],
    series: [
        {
            name: 'Mahajanga',
            type: 'bar',
            data: [320, 332, 301, 334, 390, 330, 320]
        },
        {
            name: 'Antsiranana',
            type: 'bar',
            stack: '广告',
            data: [120, 132, 101, 134, 90, 230, 210]
        },
        {
            name: 'Toamasina',
            type: 'bar',
            stack: '广告',
            data: [220, 182, 191, 234, 290, 330, 310]
        },
        {
            name: 'Toliara',
            type: 'bar',
            stack: '广告',
            data: [150, 232, 201, 154, 190, 330, 410]
        },
        {
            name: 'Antananarivo',
            type: 'bar',
            data: [862, 1018, 964, 1026, 1679, 1600, 1570],
            markLine: {
                lineStyle: {
                    type: 'dashed'
                },
                data: [[{ type: 'min' }, { type: 'max' }]]
            }
        },
        {
            name: 'Antsirabe',
            type: 'bar',
            barWidth: 5,
            stack: '搜索引擎',
            data: [620, 732, 701, 734, 1090, 1130, 1120]
        },
    ]
};
var option4 = {
    title: {
        text: 'Ménages'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: [
            'Mahajanga',
            'Antsiranana',
            'Toamasina',
            'Toliara',
            'Antananarivo',
            'Antsirabe',
        ],
        top: '35px'
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true,
        top: '100px'
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['2015', '2016', '2017', '2018', '2019', '2020', '2021']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name: 'Mahajanga',
            type: 'line',
            stack: '总量',
            data: [120, 132, 101, 134, 90, 230, 210]
        },
        {
            name: 'Antsiranana',
            type: 'line',
            stack: '总量',
            data: [220, 182, 191, 234, 290, 330, 310]
        },
        {
            name: 'Toamasina',
            type: 'line',
            stack: '总量',
            data: [150, 232, 201, 154, 190, 330, 410]
        },
        {
            name: 'Toliara',
            type: 'line',
            stack: '总量',
            data: [320, 332, 301, 334, 390, 330, 320]
        },
        {
            name: 'Antananarivo',
            type: 'line',
            stack: '总量',
            data: [820, 932, 901, 934, 1290, 1330, 1320]
        },
        {
            name: 'Antsirabe',
            type: 'line',
            stack: '总量',
            data: [820, 932, 901, 934, 1290, 1330, 1320]
        },
    ]
};
var option1 = {
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    legend: {
        orient: 'vertical',
        left: 10,
        data: [' ', '']
    },
    series: [
        {
            name: 'Ménage',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                show: false,
                position: 'center'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '30',
                    fontWeight: 'bold'
                }
            },
            labelLine: {
                show: false
            },
            data: [
                { value: 17870, name: 'Enquêtés' },
                { value: 2247, name: 'Reste' },
            ]
        },
    ]
};
var option1a = {
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    legend: {
        orient: 'vertical',
        left: 10,
        data: [' ', '']
    },
    series: [
        {
            name: 'Ménage',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                show: false,
                position: 'center'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '30',
                    fontWeight: 'bold'
                }
            },
            labelLine: {
                show: false
            },
            data: [
                { value: 17161, name: 'Enquêtées' },
                { value: 1651, name: 'Reste' },
            ]
        },
    ]
};
var option1b = {
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    legend: {
        orient: 'vertical',
        left: 10,
        data: [' ', '']
    },
    series: [
        {
            name: 'Ménage',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                show: false,
                position: 'center'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '30',
                    fontWeight: 'bold'
                }
            },
            labelLine: {
                show: false
            },
            data: [
                { value: 7623, name: 'Enquêtés' },
                { value: 1357, name: 'Reste' },
            ]
        },
    ]
};
var option1c = {
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    legend: {
        orient: 'vertical',
        left: 10,
        data: [' ', '']
    },
    series: [
        {
            name: 'Ménage',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                show: false,
                position: 'center'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '30',
                    fontWeight: 'bold'
                }
            },
            labelLine: {
                show: false
            },
            data: [
                { value: 12857, name: 'Enquêtés' },
                { value: 498, name: 'Reste' },
            ]
        },
    ]
};
var option1d = {
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    legend: {
        orient: 'vertical',
        left: 10,
        data: [' ', '']
    },
    series: [
        {
            name: 'Ménage',
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                show: false,
                position: 'center'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '30',
                    fontWeight: 'bold'
                }
            },
            labelLine: {
                show: false
            },
            data: [
                { value: 11977, name: 'Enquêtés' },
                { value: 452, name: 'Reste' },
            ]
        },
    ]
};
var option2a = {
    color: ['#3398DB'],
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            // 坐标轴指示器，坐标轴触发有效
            type: 'shadow'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
            type: 'category',
            data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            axisTick: {
                alignWithLabel: true
            }
        },
    ],
    yAxis: [
        {
            type: 'value'
        },
    ],
    series: [
        {
            name: '直接访问',
            type: 'bar',
            barWidth: '60%',
            data: [10, 52, 200, 334, 390, 330, 220]
        },
    ]
};
var option3 = {
    title: {
        text: '堆叠区域图'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    legend: {
        data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
            type: 'category',
            boundaryGap: false,
            data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        },
    ],
    yAxis: [
        {
            type: 'value'
        },
    ],
    series: [
        {
            name: '邮件营销',
            type: 'line',
            stack: '总量',
            areaStyle: {},
            data: [120, 132, 101, 134, 90, 230, 210]
        },
        {
            name: '联盟广告',
            type: 'line',
            stack: '总量',
            areaStyle: {},
            data: [220, 182, 191, 234, 290, 330, 310]
        },
        {
            name: '视频广告',
            type: 'line',
            stack: '总量',
            areaStyle: {},
            data: [150, 232, 201, 154, 190, 330, 410]
        },
        {
            name: '直接访问',
            type: 'line',
            stack: '总量',
            areaStyle: {},
            data: [320, 332, 301, 334, 390, 330, 320]
        },
        {
            name: '搜索引擎',
            type: 'line',
            stack: '总量',
            label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },
            areaStyle: {},
            data: [820, 932, 901, 934, 1290, 1330, 1320]
        },
    ]
};
var option5 = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: [
            'Itasy',
            'Amoron\'iMania',
            'Vakinankaratra',
            'Bongolava',
            'Analamanga',
            'Atsimo-Atsinanana',
            'Diana',
            'Androy',
            'VatovavyFitovinany',
            'Haute Matsiatra',
            'Analanjirofo',
            'Atsinanana',
            'Sava',
            'Anôsy',
            'Ihorombe',
            'Betsiboka',
            'Boeny',
            'Alaotra Mangoro',
            'Melaky',
            'Menabe',
            'Sofia',
            'Atsimo-Andrefana',
        ]
    },
    series: [
        {
            name: 'Taux de pauvreté',
            type: 'bar',
            data: [
                75.2703, 73.4413, 18.52199, 46.9769, 34.39589, 92.3068, 71.9, 75.3832,
                14.54863, 12.31696, 10.63197, 13.05132, 10.07399, 69.0019, 32.0775,
                30.148, 82.1356, 10.54958, 29.7446, 60.8166, 12.80847, 13.52456,
            ]
        }
    ]
};

"use strict";
exports.__esModule = true;
exports.getFokontanyOff = exports.getComunneOff = exports.getDistrictOff = exports.getRegionOff = exports.getProvinceOff = void 0;
/* eslint-disable @typescript-eslint/naming-convention */
var pouchdb_1 = require("pouchdb");
exports.getProvinceOff = function () {
    return new Promise(function (res, rej) {
        var place_db = new pouchdb_1["default"]('liste_province');
        if (place_db) {
            try {
                place_db
                    .allDocs({
                    include_docs: true
                })
                    .then(function (result) {
                    res(result.rows.map(function (r) { return r.doc; }));
                });
            }
            catch (err) {
                console.log('liste_province_error: ', err);
                res(null);
            }
        }
        else {
            res(null);
        }
    });
};
exports.getRegionOff = function (regionfilter) {
    return new Promise(function (res, rej) {
        var place_db = new pouchdb_1["default"]('liste_region');
        if (place_db) {
            try {
                place_db
                    .allDocs({
                    include_docs: true
                })
                    .then(function (result) {
                    res(result.rows
                        .map(function (r) { return r.doc; })
                        .filter(function (r) { return r.prov_id === regionfilter.prov_id; }));
                });
            }
            catch (err) {
                console.log('liste_province_error: ', err);
                res(null);
            }
        }
        else {
            res(null);
        }
    });
};
exports.getDistrictOff = function (districtfilter) {
    return new Promise(function (res, rej) {
        var place_db = new pouchdb_1["default"]('list_district');
        if (place_db) {
            try {
                place_db
                    .allDocs({
                    include_docs: true
                })
                    .then(function (result) {
                    console.log('district result ===> ', result);
                    res(result.rows
                        .map(function (r) { return r.doc; })
                        .filter(function (r) {
                        return r.prov_id === districtfilter.prov_id &&
                            r.reg_id === districtfilter.reg_id;
                    }));
                });
            }
            catch (err) {
                console.log('liste_province_error: ', err);
                res(null);
            }
        }
        else {
            res(null);
        }
    });
};
exports.getComunneOff = function (comunfilter) {
    return new Promise(function (res, rej) {
        var place_db = new pouchdb_1["default"]('list_commune');
        if (place_db) {
            try {
                place_db
                    .allDocs({
                    include_docs: true
                })
                    .then(function (result) {
                    res(result.rows
                        .map(function (r) { return r.doc; })
                        .filter(function (r) {
                        return r.prov_id === comunfilter.prov_id &&
                            r.reg_id === comunfilter.reg_id &&
                            r.dis_id === comunfilter.dis_id;
                    }));
                });
            }
            catch (err) {
                console.log('liste_province_error: ', err);
                res(null);
            }
        }
        else {
            res(null);
        }
    });
};
exports.getFokontanyOff = function (fokotanyfilter) {
    return new Promise(function (res, rej) {
        var place_db = new pouchdb_1["default"]('list_fokontany');
        if (place_db) {
            try {
                place_db
                    .allDocs({
                    include_docs: true
                })
                    .then(function (result) {
                    res(result.rows
                        .map(function (r) { return r.doc; })
                        .filter(function (r) {
                        return r.prov_id === fokotanyfilter.prov_id &&
                            r.reg_id === fokotanyfilter.reg_id &&
                            r.dis_id === fokotanyfilter.dis_id &&
                            r.com_id === fokotanyfilter.com_id;
                    }));
                });
            }
            catch (err) {
                console.log('liste_province_error: ', err);
                res(null);
            }
        }
        else {
            res(null);
        }
    });
};

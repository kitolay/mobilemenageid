/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
import PouchDB from 'pouchdb';
import { CardDataResponse } from 'src/app/Models/AllResponsModels';

const tableList = [
  'enqueteur_data',
  'ig_data',
  'id_data',
  'ch_data',
  'ap_data',
  'srpt_data',
  'da_data',
  'acaa_data',
  'educres_data',
  'ep_data',
  'hmc_data',
  'validatiaon_data',
];
const id1_data = 'id1_data';
export const off_db_name = 'new_offline_data';

export const getCardListsOff = (params: {
  page: number;
  lim: number;
  filter: any;
}): Promise<CardDataResponse> =>
  new Promise((res, rej) => {
    const cardDb = new PouchDB('card_item');
    if (cardDb) {
      cardDb
        .allDocs({
          include_docs: true,
        })
        .then((cardRows) => {
          if (cardRows && cardRows.rows) {
            const off_db = new PouchDB(off_db_name);
            let offlineCards: any[];
            if (off_db) {
              off_db
                .allDocs({
                  include_docs: true,
                })
                .then((datas) => {
                  try {
                    offlineCards = datas.rows.map((r) => r.doc.card_item);
                  } catch (error) {
                    console.log(error);
                  }
                })
                .finally(() => {
                  let allrowData = (cardRows.rows as any[]).map(
                    (row) => row.doc
                  );
                  if (offlineCards && offlineCards.length) {
                    allrowData = [...allrowData, ...offlineCards];
                  }
                  const all_cards = allrowData.filter(
                    (c) => c && !c._attachments
                  );
                  if (all_cards && all_cards.length > 0) {
                    const filtered_data = all_cards.filter((card) => {
                      if (card.prenom) {
                        //console.log('prenom filter', params.filter.prenom);
                        return card.prenom.includes(params.filter.prenom);
                      } else {
                        return false;
                      }
                    }).sort((a,b)=>b.id - a.id);
                    res({
                      data: {
                        data: paginate(filtered_data,params.lim,params.page).reverse(),
                        page: params.page,
                        lim: params.lim,
                        count: filtered_data.length,
                      },
                    });
                  } else {
                    res(null);
                  }
                });
            }
          } else {
            res(null);
          }
        })
        .catch((err) => {
          console.log(err);
          res(err);
        });
    } else {
      res(null);
    }
  });

export const getDetailDta = (cardId: number): Promise<any> =>
  new Promise((res, rej) => {
    const cardDb = new PouchDB('card_item');
    if (cardDb) {
      cardDb
        .allDocs({
          include_docs: true,
        })
        .then((cardRows) => {
          if (cardRows && cardRows.rows) {
            const card_data = cardRows.rows
              .map((c) => c.doc)
              .filter((c) => !c._attachments)
              .find((c) => c.id === cardId * 1);
            if (card_data) {
              // eslint-disable-next-line prefer-const
              let container: any = {};
              tableList.forEach((tName) => {
                const all_db = new PouchDB(tName);
                if (all_db) {
                  all_db
                    .allDocs({
                      include_docs: true,
                    })
                    .then((tRow) => {
                      if (tRow && tRow.rows) {
                        const f_row = tRow.rows
                          .map((r) => r.doc)
                          .find((r) => r.refId === cardId * 1);
                        if (f_row) {
                          container[tName] = f_row;
                        } else {
                          res(null);
                        }
                      } else {
                        res(null);
                      }
                    });
                } else {
                  res(null);
                }
              });
              const id1data_table = new PouchDB(id1_data);
              if (id1data_table) {
                id1data_table
                  .allDocs({
                    include_docs: true,
                  })
                  .then((id1) => {
                    try {
                      const id_row = id1.rows
                        .map((r) => r.doc)
                        .filter((r) => r.refId === cardId * 1);
                      container[id1_data] = id_row;
                      res({ ...card_data, details: container });
                    } catch (error) {
                      res(error);
                    }
                  });
              } else {
                console.log('card id1 error');
                res(null);
              }
            } else {
              // eslint-disable-next-line max-len
              console.log('card data error');
              res(null);
            }
          } else {
            console.log('card rows error');
            res(null);
          }
        });
    } else {
      console.log('card db error');
      res(null);
    }
  });

const paginate = (array: any[], page_size: number, page_number: number) =>
  // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
  array.slice((page_number - 1) * page_size, page_number * page_size);

/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
import { UserDataModel } from './../../Models/DataModels';
import { AhthentResponse } from '../../Models/AllResponsModels';
import PouchDB from 'pouchdb';

export const authenthUserOff = (
  pass: string,
  userName: string
): Promise<AhthentResponse> =>
  new Promise((res, rej) => {
    const auth_db = new PouchDB('user_data');
    if (auth_db) {
      auth_db
        .allDocs({
          include_docs: true,
          attachments: true,
        })
        .then((result) => {
          if (result && result.rows) {
            const user = (result.rows as any[]).find(
              (itm) => itm.doc.user_name === userName
            );
            if (user && user.doc) {
              if (user.doc.pass === pass) {
                const sup_db = new PouchDB('supervisor_data');
                if (sup_db) {
                  sup_db
                    .allDocs({
                      include_docs: true,
                      attachments: true,
                    })
                    .then((sup) => {
                      if (sup && sup.rows) {
                        const sup_user = (sup.rows as any[]).find(
                          (superviseur) =>
                            superviseur.doc.id === user.doc.superviseur
                        );
                        if (sup_user && sup_user.doc) {
                          // eslint-disable-next-line prefer-const
                          let userFinal = {...user.doc} as any;
                          delete userFinal._id;
                          delete userFinal._rev;
                          delete userFinal.pass;
                          userFinal.superviseur = sup_user.doc;
                          res({
                            data: {
                              user: userFinal as UserDataModel,
                              tokken: 'q0vbUaF5PuOazwLfvQQp',
                            },
                          } as any);
                        } else {
                          res(null);
                        }
                      } else {
                        res(null);
                      }
                    });
                } else {
                  res(null);
                }
              } else {
                res(null);
              }
            } else {
              res(null);
            }
          } else {
            res(null);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      res(null);
    }
  });

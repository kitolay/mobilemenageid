import { saveSurveyData } from 'src/app/Services/ApiServices/form.services';
/* eslint-disable no-underscore-dangle */
import { map } from 'rxjs/operators';
import { off_db_name } from 'src/app/Services/offline/card.services';
/* eslint-disable @typescript-eslint/naming-convention */
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import {
  getSyncAllDataFromTable,
  getSyncData,
  getSyncImage,
  getSyncNotExistDataFromTable,
} from '../ApiServices/sync.services';

export class MyDatabaseService {
  maindb: any = {};
  constructor() {
    PouchDB.plugin(cordovaSqlitePlugin);
    this.maindb = [];
  }
  syncDb(): Promise<{ state: boolean; message: any }> {
    //this.maindb
    return new Promise((res, rej) => {
      getSyncData().then((resp) => {
        //console.log(resp);
        if (resp && resp.allIds && resp.allIds.length > 0) {
          if (resp.tokken) {
            sessionStorage.setItem('tokken', resp.tokken);
          }
          const TABLE_DATA = resp.allIds as any[];
          // eslint-disable-next-line prefer-const
          let NEW_ELEMENT_LISTS: any[] = [];
          // eslint-disable-next-line prefer-const
          let VOID_TABLES: any[] = [];
          this.getAllDatas(
            0,
            TABLE_DATA.length,
            TABLE_DATA,
            NEW_ELEMENT_LISTS,
            VOID_TABLES,
            res
          );
        } else {
          console.log('geting sync data error: ',resp);
          res({ state: false, message: resp });
        }
      });
    });
  }
  getAllDatas(
    i: number,
    limit: number,
    list: any[],
    value: any[],
    vtable: any[],
    resolv: (value: any) => void
  ) {
    if (i < limit) {
      this.maindb[list[i].tableName] = new PouchDB(list[i].tableName);
      this.maindb[list[i].tableName]
        .allDocs()
        .then((result) => {
          if (result && result.rows) {
            if (result.rows.length > 0) {
              value.push(result.rows);
            } else {
              vtable.push(list[i].tableName);
            }
          } else {
            resolv({ state: false, message: result });
          }
          this.getAllDatas(i + 1, limit, list, value, vtable, resolv);
        })
        .catch((err) => {
          resolv({ state: false, message: err });
        });
    } else {
      this.continueSteps(vtable, list, resolv);
    }
  }
  continueSteps(vtable: any[], newTabels: any[], resolv: (value: any) => void) {
    if (vtable && vtable.length > 0) {
      getSyncAllDataFromTable(vtable)
        .then((resultData) => {
          if (resultData && resultData.length > 0) {
            const data_list = resultData as any[];
            data_list.forEach((dbdata, tidex) => {
              this.maindb[dbdata.tableName]
                .bulkDocs(dbdata.rows)
                .then(() => {
                  //console.log('newTable', resultbulk);
                  //resolv({ state: true, message: resultbulk });
                })
                .catch((err) => {
                  console.log('bulk dock table: '+dbdata.tableName+': ', err);
                  resolv({ state: false, message: err });
                });
              if (tidex >= data_list.length - 1) {
                const tableList = Object.keys(this.maindb);
                if (tableList.length > 0) {
                  let new_values: any[];
                  this.checkDifbyTable(
                    0,
                    tableList.length,
                    tableList,
                    newTabels,
                    new_values,
                    resolv
                  );
                }
              }
            });
          } else {
            console.log('getSyncAllDataFromTable Data Error: ', resultData);
            resolv({ state: false, message: resultData });
          }
        })
        .catch((err) => {
          console.log('getSyncAllDataFromTable Data Error: ', err);
          resolv({ state: false, message: err });
        });
    } else {
      const tableList = Object.keys(this.maindb);
      if (tableList.length > 0) {
        // eslint-disable-next-line prefer-const
        let new_values: any[] = [];
        this.checkDifbyTable(
          0,
          tableList.length,
          tableList,
          newTabels,
          new_values,
          resolv
        );
      }
    }
  }
  checkDifbyTable(
    y: number,
    lim: number,
    tables: string[],
    serverTable: any[],
    nVal: any[],
    reso: (value: any) => void
  ) {
    if (y < lim) {
      this.maindb[tables[y]]
        .allDocs({
          include_docs: true,
          attachments: true,
        })
        .then((resultdoc) => {
          if (resultdoc && resultdoc.rows) {
            const all_rows = resultdoc.rows as any[];
            const ofllineIds = all_rows.map((rw) => {
              if (tables[y] === 'ig_data' || tables[y] === 'enqueteur_data') {
                return rw.doc.uid;
              } else {
                return rw.doc.id;
              }
            });
            let onlineIds = serverTable.find(
              (cel) => cel.tableName === tables[y]
            );
            if (onlineIds) {
              onlineIds = onlineIds.ids;
            }
            if (ofllineIds && onlineIds) {
              // eslint-disable-next-line prefer-const
              let new_ids = [];
              onlineIds.forEach((onid, olast) => {
                let match = false;
                ofllineIds.forEach((offid, last) => {
                  if (offid === onid) {
                    match = true;
                  }
                  if (last >= ofllineIds.length - 1 && !match) {
                    new_ids.push(onid);
                  }
                });
                if (olast >= onlineIds.length - 1) {
                  if (new_ids && new_ids.length > 0) {
                    nVal.push({ table: tables[y], ids: new_ids });
                  }
                  this.checkDifbyTable(
                    y + 1,
                    lim,
                    tables,
                    serverTable,
                    nVal,
                    reso
                  );
                }
              });
            } else {
              reso({
                state: false,
                message: { online: onlineIds, offline: ofllineIds },
              });
            }
          } else {
            reso({ state: false, message: resultdoc });
          }
        })
        .catch((err) => {
          reso({ state: false, message: err });
        });
    } else {
      this.lastSteps(nVal, reso);
      // console.log('nVal data',nVal);
      // reso({ state: false, message: nVal });
    }
  }
  lastSteps(nVal: any[], reso: (value: any) => void) {
    if (nVal && nVal.length > 0) {
      getSyncNotExistDataFromTable(nVal).then((syncRes) => {
        //console.log('last step data ==> ', syncRes);
        if (syncRes && syncRes.length > 0) {
          const new_data_sync = syncRes as any[];
          new_data_sync.forEach((tablses, itabl) => {
            this.maindb[tablses.tableName]
              .bulkDocs(tablses.rows)
              .then(() => {
                //console.log('newTableItems', resultbulk);
                //resolv({ state: true, message: resultbulk });
              })
              .catch((err) => {
                console.log('bulkDocs Error: tablses.tableName ',err);
                reso({ state: false, message: err });
              });
            if (itabl >= new_data_sync.length - 1) {
              reso({ state: true, message: 'syncronisation finished!' });
            }
          });
        } else {
          reso({ state: false, message: syncRes });
        }
        //reso({ state: false, message: syncRes });
      });
    } else {
      reso({ state: true, message: 'syncronisation finished!' });
    }
  }
  imageSynchronisation(): Promise<{ state: boolean; message: any }> {
    return new Promise((res, rej) => {
      if (this.maindb.card_item) {
        this.maindb.card_item
          .allDocs({
            include_docs: true,
            attachments: true,
          })
          .then((docs) => {
            //console.log('all docs  ...',docs);
            if (docs && docs.rows) {
              const filtered_row = docs.rows.filter((r)=>r.doc.pict);
              filtered_row.forEach((row, rindex) => {
                if (row && row.doc) {
                  //console.log(row.doc);
                  this.maindb.card_item
                    .getAttachment('profile-picture-'+row.doc.nis +'-'+ row.doc.id,
                    row.doc.nis +'-'+ row.doc.id)
                    .then(() => {
                      //console.log('Atachement log ==> ',attachement);
                      //res({ state: true, message: attachement });
                    })
                    .catch((err) => {
                      if (err.status === 404) {
                        console.log('attachement not found, adding attachement ...',row.doc.pict);
                        getSyncImage(row.doc.pict).then((responsImg) => {
                          if (responsImg) {
                            this.maindb.card_item
                              .putAttachment(
                                'profile-picture-'+row.doc.nis +'-'+ row.doc.id,
                                row.doc.nis +'-'+ row.doc.id,
                                responsImg,
                                'image/png'
                              )
                              .catch((ierr) => {
                                console.log('maindb.card_item putAttachment error:',ierr);
                                res({ state: true, message: ierr });
                              });
                          } else {
                            console.log('responsImg Error:',responsImg);
                            res({ state: true, message: responsImg });
                          }
                        });
                      } else {
                        res({ state: true, message: err });
                      }
                    });
                } else {
                  res({ state: false, message: row });
                }
                if (rindex >= filtered_row.length - 1) {
                  res({ state: true, message: 'image synchronistaion finie.' });
                }
              });
            } else {
              res({ state: false, message: docs });
            }
          });
      } else {
        res({ state: false, message: 'erreur de table.' });
      }
    });
  }
  newDataSynchronisation(): Promise<{ state: boolean; message: any }> {
    return new Promise((res, rej) => {
      const newDataOff = new PouchDB(off_db_name);
      if (newDataOff) {
        newDataOff
          .allDocs({
            include_docs: true,
          })
          .then((newEntry) => {
            const all_entry = newEntry.rows
              .map((r) => r.doc)
              .filter((r) => r._id)
              .map((r) => {
                const result = { ...r };
                delete result._id;
                delete result._rev;
                delete result.card_item;
                return result;
              }) as any[];
            this.uploadNewData(0,all_entry.length,all_entry,newDataOff,res);
            // res({
            //   state: true,
            //   message: all_entry,
            // });
          })
          .catch((err) => {
            console.log('allDocs Error: '+off_db_name+': ',err);
            res({
              state: false,
              message: err,
            });
          });
      } else {
        res({
          state: false,
          message: 'Database read error',
        });
      }
    });
  }
  uploadNewData(
    i: number,
    lim: number,
    data: any[],
    db: any,
    resolve: (value: any) => void
  ) {
    if (i < lim) {
      saveSurveyData(data[i],true).then((resp) => {
        if(!resp.error){
          this.uploadNewData(i+1,lim,data,db,resolve);
        }else{
          resolve(resp);
        }
      }).catch((err)=>{
        resolve(err);
      });
    } else {
      db.destroy().then(() => {
        resolve({ state: true, message: 'image synchronistaion terminer.' });
      }).catch((err) => {
        console.log('db.destroy Error: ',err);
        resolve({ state: false, message: err });
      });
    }
  }
}

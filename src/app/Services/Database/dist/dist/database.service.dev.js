"use strict";

exports.__esModule = true;
exports.MyDatabaseService = void 0;
/* eslint-disable @typescript-eslint/naming-convention */

var pouchdb_1 = require("pouchdb");

var pouchdb_adapter_cordova_sqlite_1 = require("pouchdb-adapter-cordova-sqlite");

var sync_services_1 = require("../ApiServices/sync.services");

var MyDatabaseService =
/** @class */
function () {
  function MyDatabaseService() {
    this.maindb = {};
    pouchdb_1["default"].plugin(pouchdb_adapter_cordova_sqlite_1["default"]);
    this.maindb = [];
  }

  MyDatabaseService.prototype.syncDb = function () {
    var _this = this; //this.maindb


    return new Promise(function (res, rej) {
      sync_services_1.getSyncData().then(function (resp) {
        //console.log(resp);
        if (resp && resp.length > 0) {
          var TABLE_DATA = resp; // eslint-disable-next-line prefer-const

          var NEW_ELEMENT_LISTS = []; // eslint-disable-next-line prefer-const

          var VOID_TABLES = [];

          _this.getAllDatas(0, TABLE_DATA.length, TABLE_DATA, NEW_ELEMENT_LISTS, VOID_TABLES, res);
        } else {
          console.log(resp);
          res({
            state: false,
            message: resp
          });
        }
      });
    });
  };

  MyDatabaseService.prototype.getAllDatas = function (i, limit, list, value, vtable, resolv) {
    var _this = this;

    if (i < limit) {
      this.maindb[list[i].tableName] = new pouchdb_1["default"](list[i].tableName);
      this.maindb[list[i].tableName].allDocs().then(function (result) {
        if (result && result.rows) {
          if (result.rows.length > 0) {
            value.push(result.rows);
          } else {
            vtable.push(list[i].tableName);
          }
        } else {
          resolv({
            state: false,
            message: result
          });
        }

        _this.getAllDatas(i + 1, limit, list, value, vtable, resolv);
      })["catch"](function (err) {
        resolv({
          state: false,
          message: err
        });
      });
    } else {
      this.continueSteps(vtable, list, resolv);
    }
  };

  MyDatabaseService.prototype.continueSteps = function (vtable, newTabels, resolv) {
    var _this = this;

    if (vtable && vtable.length > 0) {
      sync_services_1.getSyncAllDataFromTable(vtable).then(function (resultData) {
        if (resultData && resultData.length > 0) {
          var data_list_1 = resultData;
          data_list_1.forEach(function (dbdata, tidex) {
            _this.maindb[dbdata.tableName].bulkDocs(dbdata.rows).then(function (resultbulk) {//console.log('newTable', resultbulk);
              //resolv({ state: true, message: resultbulk });
            })["catch"](function (err) {
              console.log(err);
              resolv({
                state: false,
                message: err
              });
            });

            if (tidex >= data_list_1.length - 1) {
              var tableList = Object.keys(_this.maindb);

              if (tableList.length > 0) {
                var new_values = void 0;

                _this.checkDifbyTable(0, tableList.length, tableList, newTabels, new_values, resolv);
              }
            }
          });
        } else {
          console.log(resultData);
          resolv({
            state: false,
            message: resultData
          });
        }
      })["catch"](function (err) {
        console.log(err);
        resolv({
          state: false,
          message: err
        });
      });
    } else {
      var tableList = Object.keys(this.maindb);

      if (tableList.length > 0) {
        // eslint-disable-next-line prefer-const
        var new_values = [];
        this.checkDifbyTable(0, tableList.length, tableList, newTabels, new_values, resolv);
      }
    }
  };

  MyDatabaseService.prototype.checkDifbyTable = function (y, lim, tables, serverTable, nVal, reso) {
    var _this = this;

    if (y < lim) {
      this.maindb[tables[y]].allDocs({
        include_docs: true,
        attachments: true
      }).then(function (resultdoc) {
        if (resultdoc && resultdoc.rows) {
          var all_rows = resultdoc.rows;
          var ofllineIds_1 = all_rows.map(function (rw) {
            if (tables[y] === 'ig_data' || tables[y] === 'enqueteur_data') {
              return rw.doc.uid;
            } else {
              return rw.doc.id;
            }
          });
          var onlineIds_1 = serverTable.find(function (cel) {
            return cel.tableName === tables[y];
          });

          if (onlineIds_1) {
            onlineIds_1 = onlineIds_1.ids;
          }

          if (ofllineIds_1 && onlineIds_1) {
            // eslint-disable-next-line prefer-const
            var new_ids_1 = [];
            onlineIds_1.forEach(function (onid, olast) {
              var match = false;
              ofllineIds_1.forEach(function (offid, last) {
                if (offid === onid) {
                  match = true;
                }

                if (last >= ofllineIds_1.length - 1 && !match) {
                  new_ids_1.push(onid);
                }
              });

              if (olast >= onlineIds_1.length - 1) {
                if (new_ids_1 && new_ids_1.length > 0) {
                  nVal.push({
                    table: tables[y],
                    ids: new_ids_1
                  });
                }

                _this.checkDifbyTable(y + 1, lim, tables, serverTable, nVal, reso);
              }
            });
          } else {
            reso({
              state: false,
              message: {
                online: onlineIds_1,
                offline: ofllineIds_1
              }
            });
          }
        } else {
          reso({
            state: false,
            message: resultdoc
          });
        }
      })["catch"](function (err) {
        reso({
          state: false,
          message: err
        });
      });
    } else {
      this.lastSteps(nVal, reso); // console.log('nVal data',nVal);
      // reso({ state: false, message: nVal });
    }
  };

  MyDatabaseService.prototype.lastSteps = function (nVal, reso) {
    var _this = this;

    if (nVal && nVal.length > 0) {
      sync_services_1.getSyncNotExistDataFromTable(nVal).then(function (syncRes) {
        //console.log('last step data ==> ', syncRes);
        if (syncRes && syncRes.length > 0) {
          var new_data_sync_1 = syncRes;
          new_data_sync_1.forEach(function (tablses, itabl) {
            _this.maindb[tablses.tableName].bulkDocs(tablses.rows).then(function (resultbulk) {
              console.log('newTableItems', resultbulk); //resolv({ state: true, message: resultbulk });
            })["catch"](function (err) {
              console.log(err);
              reso({
                state: false,
                message: err
              });
            });

            if (itabl >= new_data_sync_1.length - 1) {
              reso({
                state: true,
                message: 'syncronisation finished!'
              });
            }
          });
        } else {
          reso({
            state: false,
            message: syncRes
          });
        } //reso({ state: false, message: syncRes });

      });
    } else {
      reso({
        state: true,
        message: 'syncronisation finished!'
      });
    }
  };

  MyDatabaseService.prototype.imageSynchronisation = function () {
    var _this = this;

    return new Promise(function (res, rej) {
      if (_this.maindb.card_item) {
        _this.maindb.card_item.allDocs({
          include_docs: true,
          attachments: true
        }).then(function (docs) {
          if (docs && docs.rows) {
            docs.rows.forEach(function (row, rindex) {
              if (row && row.doc) {
                console.log(row.doc);

                _this.maindb.card_item.getAttachment(row.doc.nis, row.doc.pict).then(function (attachement) {//console.log('Atachement log ==> ', attachement);
                  //res({ state: true, message: attachement });
                })["catch"](function (err) {
                  if (err.status === 404) {
                    sync_services_1.getSyncImage(row.doc.pict).then(function (responsImg) {
                      if (responsImg) {
                        _this.maindb.card_item.putAttachment(row.doc.nis, row.doc.pict, responsImg, 'image/png')["catch"](function (ierr) {
                          console.log(ierr);
                          res({
                            state: true,
                            message: ierr
                          });
                        });
                      } else {
                        console.log(responsImg);
                        res({
                          state: true,
                          message: responsImg
                        });
                      }
                    });
                  } else {
                    console.log(err);
                    res({
                      state: true,
                      message: err
                    });
                  }
                });
              } else {
                res({
                  state: false,
                  message: row
                });
              }

              if (rindex >= docs.rows.length - 1) {
                res({
                  state: true,
                  message: 'image synchronistaion finie.'
                });
              }
            });
          } else {
            res({
              state: false,
              message: docs
            });
          }
        });
      } else {
        res({
          state: false,
          message: 'erreur de table.'
        });
      }
    });
  };

  return MyDatabaseService;
}();

exports.MyDatabaseService = MyDatabaseService;
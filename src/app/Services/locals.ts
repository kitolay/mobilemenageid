export const no_data_response: string = "No data from request."

export const ROUT_PATH = {
  formulaire: 'formulaire',
  login: 'login',
  home: 'home',
  recover: 'recover',
  detailsId: 'detailsId/:id',
  listes: 'listes',
  main: ''
}

export const ALL_TEXT = {
  _fromBoolean: (value: boolean) => (value ? 'Oui' : 'Non'),
  _login_bt: 'Connexion',
  _login_title: 'Connexion',
  _login_uname: `Nom d'utilisateur`,
  _pass_word: `Mot de passe`,
  passForgot: 'Mot de passe oublié?',
  _login_error: `Votre nom d'utilisateur ou mot de passe est incorrect.`,
  _champ_maquant: `Remplir les champs svp.`,
  _champ_requis: `Ce champ est requis.`,
  _not_connected: `Vous n'êtes pas connecté.`,
  _sesion_expired: 'Votre session a expiré.',
  _search_keyword: `Recherche par mot clé`,
  _filter: `Filtres`,
  _name: 'Nom:',
  _prenom: 'Prénoms:',
  _nis: 'NIS:',
  _fam_head: 'Chef de famille:',
  _addr: 'Adresse:',
  _itemPerPage: 'Elément par page:',
  _list_carte: 'Liste des cartes',
  _logout: 'Deconnexion',
  _addCard: 'Ajout nouvel élément',
  _welcome: 'Bienvenue à vous!',
  _connect: 'Connectez-vous pour continuer.',
  _renewpass: 'Récupérer votre mot de passe',
  _email: 'Votre e-mail',
  _resetpass: 'Récupérer',
  _cancel: 'Annuler',
  _eincorect: 'E-mail incorrect.',
  _conn_err: 'Erreur de connexion',
  _nomail: 'L\' e-mail n\'existe pas.',
  _dataerr: 'Erreur de donnée',
  _expired_code: 'Le code est expiré.',
  _resend_mail: 'Mail non reçu? $Renvoyer!',
  _mailresent: 'Mail renvoyer.',
  _save: 'Enregistrer',
  _errcon: 'Erreur de crédenciale ou de base de données',
  _newpass: 'Nouveau mot de passe',
  _confirm_pass: 'Confirmer',
  _new_pass: 'Saisissez votre nouveau mot de passe',
  _pas_not_match: 'Les mots de passe ne corespondent pas',
  _common_err: 'Une erreur est survenue.',
  _pas_modified: 'Mot de passe modifié.',
  _complet_data: 'Compléter les données svp.',
  _Ajout: 'Ajout',
  _ecodemail: 'Un code de récuperation a été envoyé vers votre adresse éléctronique. Veuillez taper le code ici pour réinitialiser votre mot de passe.'
}

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { getUserStorageJson } from './ApiServices/storage.services';
import { ALL_TEXT, ROUT_PATH } from './locals';

@Injectable({
  providedIn: 'root',
})
export class AuthguardService implements CanActivate {
  constructor(private toastr: ToastrService, public router: Router) {}
  canActivate(): boolean {
    const userData = getUserStorageJson();
    if (userData && userData.expire) {
      const expireSession = userData.expire;
      if (new Date() < new Date(expireSession)) {
        return true;
      }
      else {
        this.toastr.error(ALL_TEXT._sesion_expired);
        this.router.navigate([ROUT_PATH.login]);
        return false;
      }
    } else {
      this.toastr.error(ALL_TEXT._not_connected);
      this.router.navigate([ROUT_PATH.login]);
      return false;
    }
  }
}

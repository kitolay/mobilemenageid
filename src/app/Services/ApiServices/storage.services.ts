/* eslint-disable no-bitwise */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { CodeDataInfo } from 'src/app/Models/common';

export function saveCodeInfo(codeData: CodeDataInfo) {
  return new Promise<boolean>((res, _rej) => {
    sessionStorage.setItem(CODE_STORAGE, encriptString(JSON.stringify(codeData), true));
    setTimeout(() => {
      res(true);
    }, 200);
  });
}

export function getCodeInfo(): CodeDataInfo | undefined {
  let result: any;
  try {
    result = JSON.parse(
      decriptString(sessionStorage.getItem(CODE_STORAGE) ?? '')
    );
  } catch (error) {
    result = undefined;
  }
  return result;
}

export function saveUser(params: any) {
  return new Promise<boolean>((res, _rej) => {
    sessionStorage.setItem(USER_STORAGE, encriptString(params, true));
    setTimeout(() => {
      res(true);
    }, 200);
  });
}

export function getUserStorage() {
  return decriptString(sessionStorage.getItem(USER_STORAGE) ?? '');
}

export function getUserStorageJson() {
  const userStorage = sessionStorage.getItem(USER_STORAGE);

  if (userStorage) {
    let result: any;
    try {
      result = JSON.parse(decriptString(userStorage));
    } catch (error) {
      result = null;
    }
    return result;
  } else {return null;}
}

export function getUserSessionEpired(){
  const userData = getUserStorageJson();
  if(userData){
    if(userData.expire && new Date(userData.expire) > new Date()) {return false;}
    else {return true;}
  }else {return true;}
}

export function deletUserStorage() {
  sessionStorage.removeItem(USER_STORAGE);
}

export function getUserData() {
  let result: any;
  try {
    result = JSON.parse(
      decriptString(sessionStorage.getItem(USER_STORAGE) ?? '')
    );
  } catch (error) {
    result = null;
  }
  return result;
}

export function encriptString(value: any, isJson?: boolean): string {
  if (isJson) {return JSON.stringify(value ?? {});} // cipher(JSON.stringify(value)).toString();
  else {return value;} // cipher(value).toString();
}

export function decriptString(value: string): any {
  let result: any;
  try {
    result = decipher(value).toString();
  } catch (error) {
    result = null;
  }

  if(value.length > 0)
  {return value;} //result;
  else {return null;}
}

const cipher = (salt: any) => {
  const textToChars = (text: any) =>
    text.split('').map((c: any) => c.charCodeAt(0));
  const byteHex = (n: any) => ('0' + Number(n).toString(16)).substr(-2);
  const applySaltToChar = (code: any) =>
    textToChars(salt).reduce((a: any, b: any) => a ^ b, code);

  return (text: any) =>
    text.split('').map(textToChars).map(applySaltToChar).map(byteHex).join('');
};

const decipher = (salt: any) => {
  const textToChars = (text: any) =>
    text.split('').map((c: any) => c.charCodeAt(0));
  const applySaltToChar = (code: any) =>
    textToChars(salt).reduce((a: any, b: any) => a ^ b, code);
  return (encoded: any) =>
    encoded
      .match(/.{1,2}/g)
      .map((hex: any) => parseInt(hex, 16))
      .map(applySaltToChar)
      .map((charCode: any) => String.fromCharCode(charCode))
      .join('');
};

export const USER_STORAGE = 'userDataStoragePath'; //cipher('userDataStoragePath').toString();
export const CODE_STORAGE = 'resetCodeData'; //cipher('resetCodeData').toString();

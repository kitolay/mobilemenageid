/* eslint-disable @typescript-eslint/naming-convention */
import Http from './Http';
import { AhthentResponse } from '../../Models/AllResponsModels';
import { deletUserStorage, getUserStorage, saveUser } from './storage.services';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserDataModel } from 'src/app/Models/DataModels';
import { authenthUserOff } from '../offline/authent.services';

export class AuthentUserService {
  http: Http;
  helper: JwtHelperService;
  userExpiration: Date | null = null;
  curentUser: UserDataModel | undefined;
  userTokken?: string;
  constructor() {
    this.http = new Http();
    this.helper = new JwtHelperService();
    const userFromStorage = getUserStorage();
    if (userFromStorage) {
      this.userExpiration = userFromStorage.expire;
      this.curentUser = userFromStorage.user_data;
      this.userTokken = userFromStorage.userTokken;
    }
  }

  login(username: string, userpass: string): Promise<AhthentResponse> {
    return authenthUserOff(userpass, username);
  }

  setSession(userTokken: string, userData: UserDataModel): Promise<boolean> {
    try{
      this.userExpiration = this.helper.getTokenExpirationDate(userTokken);
    }catch{
      // eslint-disable-next-line prefer-const
      let curent_date = new Date();
      curent_date.setDate(curent_date.getDate() + 1);
      this.userExpiration = curent_date;
    }
    this.userTokken = userTokken;
    //const decodedToken = this.helper.decodeToken(userTokken)
    return saveUser({
      expire: this.userExpiration,
      user_data: userData,
      userTokken,
    });
  }

  logout() {
    deletUserStorage();
  }

  isLogeIn(): boolean {
    const userFromStorage = getUserStorage();
    if (userFromStorage) {
      this.userExpiration = JSON.parse(userFromStorage).expire;
    }
    if (this.userExpiration) {
      return new Date() < new Date(this.userExpiration);
    }
    return false;
  }

  isLogedOut(): boolean {
    return !this.isLogeIn();
  }

  getExpiration() {
    return this.userExpiration;
  }
}

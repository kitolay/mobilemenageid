import { MyDatabaseService } from '../Database/database.service';
import Http from './Http';
const http = new Http();

export const syncDatas = (): Promise<{ statue: boolean; message: any }> =>
  new Promise((res, rej) => {
    const dbservice = new MyDatabaseService();
    dbservice.newDataSynchronisation().then((result) => {
      // console.log('new data synchronisation ===> ',result);
      // res({ statue: true, message: 'Synchronisation terminer.' });
      dbservice.syncDb().then((resp) => {
        if (resp && resp.state) {
          dbservice.imageSynchronisation().then((imsic) => {
            if (imsic && imsic.state) {
              res({ statue: true, message: 'Synchronisation terminée.' });
            } else {
              res({
                statue: false,
                message:
                  'Erreur de synchronisation: ' + imsic.message.toString(),
              });
            }
          });
        } else {
          res({
            statue: false,
            message: 'Erreur de synchronisation: ' + resp.message.toString(),
          });
        }
      });
    });
  });

export const getSyncImage = (imageUrl: string): Promise<any> => {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const splited_image = imageUrl.split('/');
  const imagename = splited_image[splited_image.length - 1];
  return http.get('image-base64?name=' + imagename).then((res) => {
    if (res && res.data) {
      return res.data;
    } else {
      return res;
    }
  });
};

export const getSyncData = (): Promise<any> =>
  http.get('syncdata-mobile').then((res) => {
    if (res && res.data) {
      return res.data;
    } else {
      return res;
    }
  });

export const getSyncAllDataFromTable = (tables: string[]): Promise<any> =>
  http.post('sync-table-data', tables).then((res) => {
    if (res && res.data) {
      return res.data;
    } else {
      return res;
    }
  });

export const getSyncNotExistDataFromTable = (
  tableDataListes: { table: string; ids: number[] }[]
): Promise<any> =>
  http.post('sync-data-by-table-by-id', tableDataListes).then((res) => {
    if (res && res.data) {
      return res.data;
    } else {
      return res;
    }
  });

export const testConnextion = (): Promise<{ state: boolean; message: any }> =>
  http
    .get('contest')
    .then((response) => {
      if (response && response.data) {
        return response.data;
      } else {
        return { state: false, message: response };
      }
    })
    .catch((err) => ({ state: false, message: err }));

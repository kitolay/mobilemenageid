"use strict";
exports.__esModule = true;
exports.testDataValidity = exports.authentUser = void 0;
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
var Http_1 = require("./Http");
var http = new Http_1["default"]();
function authentUser(username, userpass) {
    return http.post('authent', {
        uname: username,
        upass: userpass
    });
}
exports.authentUser = authentUser;
function testDataValidity(data) {
    var keys = Object.keys(data);
    var result = false;
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (data[key] && data[key].toString().length > 0) {
            result = true;
        }
        else {
            result = false;
            break;
        }
    }
    return result;
}
exports.testDataValidity = testDataValidity;

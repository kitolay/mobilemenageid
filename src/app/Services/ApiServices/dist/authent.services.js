"use strict";
exports.__esModule = true;
exports.AuthentUserService = void 0;
/* eslint-disable @typescript-eslint/naming-convention */
var Http_1 = require("./Http");
var storage_services_1 = require("./storage.services");
var angular_jwt_1 = require("@auth0/angular-jwt");
var authent_services_1 = require("../offline/authent.services");
var AuthentUserService = /** @class */ (function () {
    function AuthentUserService() {
        this.userExpiration = null;
        this.http = new Http_1["default"]();
        this.helper = new angular_jwt_1.JwtHelperService();
        var userFromStorage = storage_services_1.getUserStorage();
        if (userFromStorage) {
            this.userExpiration = userFromStorage.expire;
            this.curentUser = userFromStorage.user_data;
            this.userTokken = userFromStorage.userTokken;
        }
    }
    AuthentUserService.prototype.login = function (username, userpass) {
        return authent_services_1.authenthUserOff(userpass, username);
    };
    AuthentUserService.prototype.setSession = function (userTokken, userData) {
        try {
            this.userExpiration = this.helper.getTokenExpirationDate(userTokken);
        }
        catch (_a) {
            // eslint-disable-next-line prefer-const
            var curent_date = new Date();
            curent_date.setDate(curent_date.getDate() + 1);
            this.userExpiration = curent_date;
        }
        this.userTokken = userTokken;
        //const decodedToken = this.helper.decodeToken(userTokken)
        return storage_services_1.saveUser({
            expire: this.userExpiration,
            user_data: userData,
            userTokken: userTokken
        });
    };
    AuthentUserService.prototype.logout = function () {
        storage_services_1.deletUserStorage();
    };
    AuthentUserService.prototype.isLogeIn = function () {
        var userFromStorage = storage_services_1.getUserStorage();
        if (userFromStorage) {
            this.userExpiration = JSON.parse(userFromStorage).expire;
        }
        if (this.userExpiration) {
            return new Date() < new Date(this.userExpiration);
        }
        return false;
    };
    AuthentUserService.prototype.isLogedOut = function () {
        return !this.isLogeIn();
    };
    AuthentUserService.prototype.getExpiration = function () {
        return this.userExpiration;
    };
    return AuthentUserService;
}());
exports.AuthentUserService = AuthentUserService;

"use strict";
exports.__esModule = true;
exports.CardDataService = void 0;
var Http_1 = require("./Http");
var card_services_1 = require("../offline/card.services");
var CardDataService = /** @class */ (function () {
    function CardDataService() {
        this.http = new Http_1["default"]();
    }
    CardDataService.prototype.getAll = function (params) {
        return card_services_1.getCardListsOff(params);
    };
    CardDataService.prototype.getDetails = function (id) {
        return card_services_1.getDetailDta(id).then(function (rep) {
            if (rep) {
                var toModData_1 = rep;
                var detailKeys = Object.keys(toModData_1.details);
                detailKeys.forEach(function (tkay) {
                    var contentKeys = Object.keys(toModData_1.details[tkay]);
                    contentKeys.forEach(function (dkey) {
                        var new_value;
                        try {
                            new_value = JSON.parse(toModData_1.details[tkay][dkey]);
                        }
                        catch (error) {
                            new_value = toModData_1.details[tkay][dkey];
                        }
                        toModData_1.details[tkay][dkey] = new_value;
                    });
                });
                toModData_1.pic = '';
                return (toModData_1);
            }
            else {
                return (null);
            }
        });
    };
    return CardDataService;
}());
exports.CardDataService = CardDataService;

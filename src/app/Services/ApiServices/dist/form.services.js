"use strict";
exports.__esModule = true;
exports.saveSurveyData = void 0;
var Http_1 = require("./Http");
var http = new Http_1["default"]();
function saveSurveyData(survey, sync) {
    var formDataWithImage = new FormData();
    if (!survey.id_data.pict.name) {
        survey.id_data.pict = new File([survey.id_data.pict], survey.id_data.nom + '-' + survey.id_data.cin + '.jpg');
    }
    formDataWithImage.append('profile_pic', survey.id_data.pict);
    formDataWithImage.append('formData', JSON.stringify(survey));
    return http
        .postWithFile(sync ? 'saveData' : 'sync-saveData', formDataWithImage)
        .then(function (rep) {
        if (rep) {
            //console.log('repdata ===> ', rep.data);
            var toModData_1 = rep.data;
            if (!sync) {
                var detailKeys = Object.keys(toModData_1.details);
                detailKeys.forEach(function (tkay) {
                    var contentKeys = Object.keys(toModData_1.details[tkay]);
                    contentKeys.forEach(function (dkey) {
                        var new_value;
                        try {
                            new_value = JSON.parse(toModData_1.details[tkay][dkey]);
                        }
                        catch (error) {
                            new_value = toModData_1.details[tkay][dkey];
                        }
                        toModData_1.details[tkay][dkey] = new_value;
                    });
                });
                toModData_1.pic = '';
            }
            return toModData_1;
        }
        else {
            return null;
        }
    });
}
exports.saveSurveyData = saveSurveyData;

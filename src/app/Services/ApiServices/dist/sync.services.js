"use strict";
exports.__esModule = true;
exports.testConnextion = exports.getSyncNotExistDataFromTable = exports.getSyncAllDataFromTable = exports.getSyncData = exports.getSyncImage = exports.syncDatas = void 0;
var database_service_1 = require("../Database/database.service");
var Http_1 = require("./Http");
var http = new Http_1["default"]();
exports.syncDatas = function () {
    return new Promise(function (res, rej) {
        var dbservice = new database_service_1.MyDatabaseService();
        dbservice.newDataSynchronisation().then(function (result) {
            // console.log('new data synchronisation ===> ',result);
            // res({ statue: true, message: 'Synchronisation terminer.' });
            dbservice.syncDb().then(function (resp) {
                if (resp && resp.state) {
                    dbservice.imageSynchronisation().then(function (imsic) {
                        if (imsic && imsic.state) {
                            res({ statue: true, message: 'Synchronisation terminer.' });
                        }
                        else {
                            res({
                                statue: false,
                                message: 'Errure de syncronisation: ' + imsic.message.toString()
                            });
                        }
                    });
                }
                else {
                    res({
                        statue: false,
                        message: 'Errure de syncronisation: ' + resp.message.toString()
                    });
                }
            });
        });
    });
};
exports.getSyncImage = function (imageUrl) {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    var splited_image = imageUrl.split('/');
    var imagename = splited_image[splited_image.length - 1];
    return http.get('image-base64?name=' + imagename).then(function (res) {
        if (res && res.data) {
            return res.data;
        }
        else {
            return res;
        }
    });
};
exports.getSyncData = function () {
    return http.get('syncdata-mobile').then(function (res) {
        if (res && res.data) {
            return res.data;
        }
        else {
            return res;
        }
    });
};
exports.getSyncAllDataFromTable = function (tables) {
    return http.post('sync-table-data', tables).then(function (res) {
        if (res && res.data) {
            return res.data;
        }
        else {
            return res;
        }
    });
};
exports.getSyncNotExistDataFromTable = function (tableDataListes) {
    return http.post('sync-data-by-table-by-id', tableDataListes).then(function (res) {
        if (res && res.data) {
            return res.data;
        }
        else {
            return res;
        }
    });
};
exports.testConnextion = function () {
    return http
        .get('contest')
        .then(function (response) {
        if (response && response.data) {
            return response.data;
        }
        else {
            return { state: false, message: response };
        }
    })["catch"](function (err) { return ({ state: false, message: err }); });
};

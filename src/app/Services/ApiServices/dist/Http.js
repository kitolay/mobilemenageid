"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
/* eslint-disable @typescript-eslint/naming-convention */
var axios_1 = require("axios");
var storage_services_1 = require("./storage.services");
var base_api_url = 'https://coursta.mg/datacollect-server-test-api/'; //'http://localhost:3024/datacollect-server-test-api/'
var Http = /** @class */ (function () {
    function Http() {
        this.user_data = null;
        this.user_data = storage_services_1.getUserStorageJson();
        if (!this.user_data) {
            this.user_data = { userTokken: '' };
        }
    }
    Http.prototype.getImage = function (iurl) {
        return axios_1["default"].get(iurl, {
            responseType: 'arraybuffer'
        });
    };
    Http.prototype.get = function (url, data, config) {
        var tokken = sessionStorage.getItem('tokken');
        return axios_1["default"].get(base_api_url + url, __assign(__assign({}, config), { params: data, headers: { Authorization: 'Bearer ' + tokken } }));
    };
    Http.prototype.post = function (url, data, config) {
        var tokken = sessionStorage.getItem('tokken');
        return axios_1["default"].post(base_api_url + url, data, __assign(__assign({}, config), { headers: { Authorization: 'Bearer ' + tokken } }));
    };
    Http.prototype.postWithFile = function (url, data, config) {
        var tokken = sessionStorage.getItem('tokken');
        // eslint-disable-next-line max-len
        return axios_1["default"].post(base_api_url + url, data, __assign(__assign({}, config), { headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: 'Bearer ' + tokken
            } }));
    };
    return Http;
}());
exports["default"] = Http;

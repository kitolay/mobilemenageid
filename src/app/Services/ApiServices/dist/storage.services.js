"use strict";
exports.__esModule = true;
exports.CODE_STORAGE = exports.USER_STORAGE = exports.decriptString = exports.encriptString = exports.getUserData = exports.deletUserStorage = exports.getUserSessionEpired = exports.getUserStorageJson = exports.getUserStorage = exports.saveUser = exports.getCodeInfo = exports.saveCodeInfo = void 0;
function saveCodeInfo(codeData) {
    return new Promise(function (res, _rej) {
        sessionStorage.setItem(exports.CODE_STORAGE, encriptString(JSON.stringify(codeData), true));
        setTimeout(function () {
            res(true);
        }, 200);
    });
}
exports.saveCodeInfo = saveCodeInfo;
function getCodeInfo() {
    var _a;
    var result;
    try {
        result = JSON.parse(decriptString((_a = sessionStorage.getItem(exports.CODE_STORAGE)) !== null && _a !== void 0 ? _a : ''));
    }
    catch (error) {
        result = undefined;
    }
    return result;
}
exports.getCodeInfo = getCodeInfo;
function saveUser(params) {
    return new Promise(function (res, _rej) {
        sessionStorage.setItem(exports.USER_STORAGE, encriptString(params, true));
        setTimeout(function () {
            res(true);
        }, 200);
    });
}
exports.saveUser = saveUser;
function getUserStorage() {
    var _a;
    return decriptString((_a = sessionStorage.getItem(exports.USER_STORAGE)) !== null && _a !== void 0 ? _a : '');
}
exports.getUserStorage = getUserStorage;
function getUserStorageJson() {
    var userStorage = sessionStorage.getItem(exports.USER_STORAGE);
    if (userStorage) {
        var result = void 0;
        try {
            result = JSON.parse(decriptString(userStorage));
        }
        catch (error) {
            result = null;
        }
        return result;
    }
    else {
        return null;
    }
}
exports.getUserStorageJson = getUserStorageJson;
function getUserSessionEpired() {
    var userData = getUserStorageJson();
    if (userData) {
        if (userData.expire && new Date(userData.expire) > new Date()) {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return true;
    }
}
exports.getUserSessionEpired = getUserSessionEpired;
function deletUserStorage() {
    sessionStorage.removeItem(exports.USER_STORAGE);
}
exports.deletUserStorage = deletUserStorage;
function getUserData() {
    var _a;
    var result;
    try {
        result = JSON.parse(decriptString((_a = sessionStorage.getItem(exports.USER_STORAGE)) !== null && _a !== void 0 ? _a : ''));
    }
    catch (error) {
        result = null;
    }
    return result;
}
exports.getUserData = getUserData;
function encriptString(value, isJson) {
    if (isJson) {
        return JSON.stringify(value !== null && value !== void 0 ? value : {});
    } // cipher(JSON.stringify(value)).toString();
    else {
        return value;
    } // cipher(value).toString();
}
exports.encriptString = encriptString;
function decriptString(value) {
    var result;
    try {
        result = decipher(value).toString();
    }
    catch (error) {
        result = null;
    }
    if (value.length > 0) {
        return value;
    } //result;
    else {
        return null;
    }
}
exports.decriptString = decriptString;
var cipher = function (salt) {
    var textToChars = function (text) {
        return text.split('').map(function (c) { return c.charCodeAt(0); });
    };
    var byteHex = function (n) { return ('0' + Number(n).toString(16)).substr(-2); };
    var applySaltToChar = function (code) {
        return textToChars(salt).reduce(function (a, b) { return a ^ b; }, code);
    };
    return function (text) {
        return text.split('').map(textToChars).map(applySaltToChar).map(byteHex).join('');
    };
};
var decipher = function (salt) {
    var textToChars = function (text) {
        return text.split('').map(function (c) { return c.charCodeAt(0); });
    };
    var applySaltToChar = function (code) {
        return textToChars(salt).reduce(function (a, b) { return a ^ b; }, code);
    };
    return function (encoded) {
        return encoded
            .match(/.{1,2}/g)
            .map(function (hex) { return parseInt(hex, 16); })
            .map(applySaltToChar)
            .map(function (charCode) { return String.fromCharCode(charCode); })
            .join('');
    };
};
exports.USER_STORAGE = 'userDataStoragePath'; //cipher('userDataStoragePath').toString();
exports.CODE_STORAGE = 'resetCodeData'; //cipher('resetCodeData').toString();

import { getStateByIdOff } from '../offline/stats.services';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export const getStateById = (id: number): Promise<any> => getStateByIdOff(id);

import { getProvinceOff, getRegionOff, getDistrictOff, getComunneOff, getFokontanyOff } from './../offline/place.service';
//import { ProvinceData, RegionData, DistrictData, CommuneData, FokontanyData } from './../../Models/AllResponsModels';
import Http from './Http';
const http = new Http();

export const getProvince = (): Promise<any> => getProvinceOff();

export const getRegion = (regionfilter: any): Promise<any> => getRegionOff(regionfilter);

export const getDistrict = (districtfilter: any): Promise<any> => getDistrictOff(districtfilter);

export const getComunne = (comunfilter: any): Promise<any> => getComunneOff(comunfilter);

export const getFokontany = (fokotanyfilter: any): Promise<any> => getFokontanyOff(fokotanyfilter);

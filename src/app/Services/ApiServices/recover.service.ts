import Http from './Http';
import { getCodeInfo } from './storage.services';
const http = new Http();

export function recoverByEmail(email: string):Promise<any> {
  return http.post('recover', { email: email });
}

export function chekCode(code:string):Promise<any> {
  const code_info = getCodeInfo();
  if(code_info){
    return http.post('checkReqPass',{
      user: code_info.user,
      ref: code_info.ref,
      code: code
    })
  }
  return new Promise((res, rej)=>res(null))
}

export function savePass(newPass:string):Promise<any> {
  const code_info = getCodeInfo();
  if(code_info){
    return http.post('resetpass',{
      user: code_info.user,
      ref: code_info.ref,
      code: code_info.code,
      newpass: newPass
    })
  }
  return new Promise((res, rej)=>res(null))
}

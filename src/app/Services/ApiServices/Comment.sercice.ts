/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import Http from './Http';
import { AhthentResponse } from '../../Models/AllResponsModels';

const http = new Http();

export function authentUser(
  username: string,
  userpass: string
): Promise<AhthentResponse> {
  return http.post('authent', {
    uname: username,
    upass: userpass,
  });
}

export function testDataValidity(data: any): boolean {
  const keys = Object.keys(data);
  let result = false;
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    if (data[key] && data[key].toString().length > 0) {result = true;}
    else {
      result = false;
      break;
    }
  }
  return result;
}

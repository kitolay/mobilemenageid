/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { CardFormDataDetail } from 'src/app/Models/AllResponsModels';
import Http from './Http';

const http = new Http();

export function saveSurveyData(
  survey: any,
  sync?: boolean
): Promise<CardFormDataDetail> {
  const formDataWithImage: FormData = new FormData();
  if(!survey.id_data.pict.name){
    survey.id_data.pict = new File([survey.id_data.pict], survey.id_data.nom+'-'+survey.id_data.cin+'.jpg');
  }
  formDataWithImage.append('profile_pic', survey.id_data.pict);
  formDataWithImage.append('formData', JSON.stringify(survey));
  return http
    .postWithFile(sync ? 'saveData' : 'sync-saveData', formDataWithImage)
    .then((rep) => {
      if (rep) {
        //console.log('repdata ===> ', rep.data);
        const toModData = rep.data;
        if (!sync) {
          const detailKeys = Object.keys(toModData.details);
          detailKeys.forEach((tkay) => {
            const contentKeys = Object.keys(toModData.details[tkay]);
            contentKeys.forEach((dkey) => {
              let new_value: any;
              try {
                new_value = JSON.parse(toModData.details[tkay][dkey]);
              } catch (error) {
                new_value = toModData.details[tkay][dkey];
              }
              toModData.details[tkay][dkey] = new_value;
            });
          });
          toModData.pic = '';
        }
        return toModData;
      } else {
        return null;
      }
    });
}

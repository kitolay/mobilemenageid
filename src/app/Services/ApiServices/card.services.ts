/* eslint-disable @typescript-eslint/naming-convention */
//import { Injectable } from '@angular/core';
import { testConnextion } from './sync.services';
import {
  CardDataResponse,
  CardFormDataDetail,
} from 'src/app/Models/AllResponsModels';
import Http from './Http';
import { getCardListsOff, getDetailDta } from '../offline/card.services';

export class CardDataService {
  http: Http;
  constructor() {
    this.http = new Http();
  }
  getAll(params: {
    page: number;
    lim: number;
    filter: any;
  }): Promise<CardDataResponse> {
    return getCardListsOff(params);
  }

  getDetails(id: number): Promise<CardFormDataDetail | null> {
    return getDetailDta(id).then((rep) => {
      if (rep) {
        const toModData = rep;
        const detailKeys = Object.keys(toModData.details);
        detailKeys.forEach((tkay) => {
          const contentKeys = Object.keys(toModData.details[tkay]);
          contentKeys.forEach((dkey) => {
            let new_value: any;
            try {
              new_value = JSON.parse(toModData.details[tkay][dkey]);
            } catch (error) {
              new_value = toModData.details[tkay][dkey];
            }
            toModData.details[tkay][dkey] = new_value;
          });
        });
        toModData.pic = '';
        return (toModData);
      } else {
        return (null);
      }
    });
  }
}

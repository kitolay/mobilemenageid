/* eslint-disable @typescript-eslint/naming-convention */
import axios, { AxiosRequestConfig } from 'axios';
import { getUserStorageJson } from './storage.services';

const base_api_url = 'https://coursta.mg/datacollect-server-test-api/'; //'http://localhost:3024/datacollect-server-test-api/'

export default class Http {
  user_data: any = null;
  constructor() {
    this.user_data = getUserStorageJson();
    if (!this.user_data) {
      this.user_data = { userTokken: '' };
    }
  }

  getImage(iurl): Promise<any> {
    return axios.get(iurl, {
      responseType: 'arraybuffer',
    });
  }

  get(url: string, data?: any, config?: AxiosRequestConfig): Promise<any> {
    const tokken = sessionStorage.getItem('tokken');
    return axios.get(base_api_url + url, {
      ...config,
      params: data,
      headers: { Authorization: 'Bearer ' + tokken },
    });
  }

  post(url: string, data?: any, config?: AxiosRequestConfig): Promise<any> {
    const tokken = sessionStorage.getItem('tokken');
    return axios.post(base_api_url + url, data, {
      ...config,
      headers: { Authorization: 'Bearer ' + tokken },
    });
  }

  postWithFile(
    url: string,
    data?: any,
    config?: AxiosRequestConfig
  ): Promise<any> {
    const tokken = sessionStorage.getItem('tokken');
    // eslint-disable-next-line max-len
    return axios.post(base_api_url + url, data, {
      ...config,
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: 'Bearer ' + tokken,
      },
    });
  }
}

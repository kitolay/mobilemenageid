export function getScreenSize(): 'XS' | 'S' | 'M' | 'L' | 'XL' | 'XXL' {
  const windowSize = window.innerWidth;
  //console.log("data value ===> ", windowSize)
  if(windowSize <= 414) {return 'XS';}
  if(windowSize >= 415 && windowSize <= 600) {return 'XS';}
  if(windowSize >= 601 && windowSize <= 800) {return 'XS';}
  if(windowSize >= 801 && windowSize <= 1024) {return 'L';}
  if(windowSize >= 1025 && windowSize <= 1440) {return 'XL';}
  if(windowSize >= 1441) {return 'S';}
  return 'XS';
}

export function getScreenValue() {
  return ({
    x: window.innerWidth,
    y: window.innerHeight
  })
}

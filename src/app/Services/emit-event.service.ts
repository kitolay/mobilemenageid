import { Injectable, EventEmitter } from '@angular/core';

export const SYNC_SUCCESS = 'sync-sucsses';

@Injectable({
  providedIn: 'root'
})
export class EmitEventService {
  eventEmitter = new EventEmitter<string>();
  constructor() { }
}

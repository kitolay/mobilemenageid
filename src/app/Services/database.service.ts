import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';

@Injectable({
  providedIn: 'root',
})
export class DatabaseService {
  public pdb;

  constructor() {}
  createPouchDB() {
    PouchDB.plugin(cordovaSqlitePlugin);
    this.pdb = new PouchDB('employees.db', { adapter: 'cordova-sqlite' });
    console.log('testData',this.pdb);
  }
}

import { Component, Input, OnInit, SimpleChange, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-steps-selector',
  templateUrl: './steps-selector.component.html',
  styleUrls: ['./steps-selector.component.css'],
})
export class StepsSelectorComponent implements OnInit {
  @Input() index: number = 1;
  @Input() all_data: any;
  @Input() disable_next: any;
  @Input() onViewInit?: (value:boolean)=>void;
  contry_list:any = contry_list;
  constructor() {}
  ngOnChanges(changes: SimpleChanges): void {
    const currentItem: SimpleChange = changes.index;
    //console.log('prev value: ', currentItem);
    // console.log('got item: ', currentItem.currentValue);
  }
  ngOnInit(): void {}
}

var contry_list = {
  province_list: undefined,
  region_list: undefined,
  district_list: undefined,
  commune_list: undefined,
  fokontany_list: undefined
}

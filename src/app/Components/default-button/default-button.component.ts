import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-default-button',
  templateUrl: './default-button.component.html',
  styleUrls: ['./default-button.component.css'],
})
export class DefaultButtonComponent implements OnInit {
  constructor() {}
  @Input('dlabel') dLabel: string = '';
  @Input('dDisabled') dDisabled?: boolean;
  @Input('preIcon') preIcon?: string;
  @Input('postIcon') postIcon?: string;
  @Input('isLoading') isLoading?: boolean;
  @Input() onClick?: () => void;
  ngOnInit(): void {}

  handleClick(){
    if(this.onClick) this.onClick()
  }
}

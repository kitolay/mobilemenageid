import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROUT_PATH } from 'src/app/Services/locals';
import { deletUserStorage } from 'src/app/Services/ApiServices/storage.services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  route_path = ROUT_PATH
  constructor(private toast: ToastrService, public router: Router) { }

  ngOnInit(): void {
  }

  handleHome(){
    this.router.navigate([ROUT_PATH.home]);
  }

  handleListes(){
    this.router.navigate([ROUT_PATH.listes]);
  }

  handleDetailsId(){
    this.router.navigate([ROUT_PATH.detailsId]);
  }

  handleLogOut() {
    deletUserStorage();
    this.router.navigate([ROUT_PATH.login]);
  }

  handleOnAdd(){
    this.router.navigate([ROUT_PATH.formulaire]);
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-default-autocomplet',
  templateUrl: './default-autocomplet.component.html',
  styleUrls: ['./default-autocomplet.component.css'],
})
export class DefaultAutocompletComponent implements OnInit {
  @Input() fullWidth: boolean = false;
  @Input() disabled: boolean = false;
  @Input() label: string = 'default label';
  @Input() placeholder: string = '';
  @Input() options: string[] = ['No data'];
  @Input() onChange?: (selected: string) => void;
  @Input() model: any;
  myControl = new FormControl();
  filteredOptions?: Observable<string[]>;

  constructor() {}

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map((value) => this._filter(value))
    );
  }

  getStyle() {
    if (this.fullWidth) return `width: 100%`;
    else return '';
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }
}

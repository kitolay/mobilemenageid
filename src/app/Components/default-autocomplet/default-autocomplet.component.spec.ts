import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultAutocompletComponent } from './default-autocomplet.component';

describe('DefaultAutocompletComponent', () => {
  let component: DefaultAutocompletComponent;
  let fixture: ComponentFixture<DefaultAutocompletComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefaultAutocompletComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultAutocompletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ROUT_PATH } from 'src/app/Services/locals';
import { deletUserStorage } from 'src/app/Services/ApiServices/storage.services';

@Component({
  selector: 'app-mobile-tab',
  templateUrl: './mobile-tab.component.html',
  styleUrls: ['./mobile-tab.component.css']
})
export class MobileTabComponent implements OnInit {
  @Input() IsConnected?: boolean;
  @Input() IsSyncronising?: number;
  route_path = ROUT_PATH;

  constructor(public router: Router) { }

  ngOnInit(): void {
  }
  getStatueText(state: number){
    if(state === 0) {return 'Vous êtes en ligne';}
    if(state === 1) {return 'Vous êtes en mode "offline"';}
    if(state === 2) {return 'Synchronisation en cours ...';}
  }

  handleChangePath(path: string){
    this.router.navigate([path]);
  }
  handleLogOut() {
    deletUserStorage();
    this.router.navigate([ROUT_PATH.login]);
  }

}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MobileTabComponent = void 0;
/* eslint-disable @typescript-eslint/naming-convention */
var core_1 = require("@angular/core");
var locals_1 = require("src/app/Services/locals");
var storage_services_1 = require("src/app/Services/ApiServices/storage.services");
var MobileTabComponent = /** @class */ (function () {
    function MobileTabComponent(router) {
        this.router = router;
        this.route_path = locals_1.ROUT_PATH;
    }
    MobileTabComponent.prototype.ngOnInit = function () {
    };
    MobileTabComponent.prototype.getStatueText = function (state) {
        if (state === 0) {
            return 'Vous êtes en ligne';
        }
        if (state === 1) {
            return 'Vous êtes en mode "offline"';
        }
        if (state === 2) {
            return 'Synchronisation en cours ...';
        }
    };
    MobileTabComponent.prototype.handleChangePath = function (path) {
        this.router.navigate([path]);
    };
    MobileTabComponent.prototype.handleLogOut = function () {
        storage_services_1.deletUserStorage();
        this.router.navigate([locals_1.ROUT_PATH.login]);
    };
    __decorate([
        core_1.Input()
    ], MobileTabComponent.prototype, "IsConnected");
    __decorate([
        core_1.Input()
    ], MobileTabComponent.prototype, "IsSyncronising");
    MobileTabComponent = __decorate([
        core_1.Component({
            selector: 'app-mobile-tab',
            templateUrl: './mobile-tab.component.html',
            styleUrls: ['./mobile-tab.component.css']
        })
    ], MobileTabComponent);
    return MobileTabComponent;
}());
exports.MobileTabComponent = MobileTabComponent;

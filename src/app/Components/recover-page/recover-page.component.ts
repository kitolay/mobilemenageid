import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ALL_TEXT, no_data_response, ROUT_PATH } from 'src/app/Services/locals';
import { FormControl, Validators } from '@angular/forms';
import {
  chekCode,
  recoverByEmail,
  savePass,
} from 'src/app/Services/ApiServices/recover.service';
import { saveCodeInfo } from 'src/app/Services/ApiServices/storage.services';

@Component({
  selector: 'app-recover-page',
  templateUrl: './recover-page.component.html',
  styleUrls: ['./recover-page.component.css'],
})
export class RecoverPageComponent implements OnInit {
  all_text: any = ALL_TEXT;
  is_loading: boolean = false;
  check_code_loading: boolean = false;
  save_loading: boolean = false;
  mail_resent: boolean = false;
  uemail = new FormControl('', [Validators.required, Validators.email]);
  upass = new FormControl('', [Validators.required]);
  user_mail: string = '';
  recover_step: number = 1;
  user_code: string = '';
  new_pass: string = '';
  confirm_pass: string = '';
  pass_hide: boolean = true;
  pass_hide1: boolean = true;

  constructor(public router: Router, public toast: ToastrService) {}

  ngOnInit(): void {}

  handleResetPass = (): void => {
    if (this.user_mail) {
      this.is_loading = true;
      recoverByEmail(this.user_mail).then((respons) => {
        if (respons && respons.data) {
          if (respons.data.error) {
            if (respons.data.message === no_data_response)
              this.toast.error(this.all_text._nomail);
            else this.toast.error(respons.data.errMessage);
          } else {
            if (respons.data.expire) {
              const expire_date = new Date(respons.data.expire);
              if (new Date() < expire_date) {
                saveCodeInfo(respons.data);
                this.recover_step = 2;
              } else this.toast.error(this.all_text._expired_code);
            } else this.toast.error(this.all_text._dataerr);
          }
        } else {
          this.toast.error(this.all_text._conn_err);
        }
        this.is_loading = false;
      }).catch(()=>this.toast.error(ALL_TEXT._errcon));
    }
  };

  handleCheckCode = (): void => {
    this.check_code_loading = true;
    if (this.user_code.length > 0) {
      chekCode(this.user_code).then((response) => {
        if (response && response.data) {
          if (response.data.error || response.data.message) {
            this.toast.error(response.data.message);
            this.check_code_loading = false;
          } else {
            saveCodeInfo(response.data);
            this.recover_step = 3;
            this.check_code_loading = false;
          }
        } else {
          this.toast.error(this.all_text._common_err);
          this.check_code_loading = false;
        }
      }).catch(()=>this.toast.error(ALL_TEXT._errcon));
    } else {
      this.toast.error(this.all_text._champ_maquant);
      this.check_code_loading = false;
    }
  };

  handleSavePass = (): void => {
    this.save_loading = true;
    if (this.upass.valid && this.new_pass === this.confirm_pass) {
      savePass(this.new_pass).then((response) => {
        if (response && response.data) {
          if (response.data.error || response.data.message) {
            this.save_loading = false;
            this.toast.error(response.data.message);
          } else {
            this.toast.success(this.all_text._pas_modified);
            this.handleCancel();
            this.save_loading = false;
          }
        } else {
          this.toast.error(this.all_text._common_err);
          this.save_loading = false;
        }
      });
    } else {
      this.toast.error(this.all_text._pas_not_match);
      this.save_loading = false;
    }
  };

  handleResendMail = (): void => {
    this.handleResetPass();
    this.mail_resent = true;
  };

  handleCancel() {
    this.router.navigate([ROUT_PATH.login]);
  }
}

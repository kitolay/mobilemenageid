import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
export interface DeFaultItemSelect{
  id: number,
  name: string,
  data: any,
}

@Component({
  selector: 'app-default-select',
  templateUrl: './default-select.component.html',
  styleUrls: ['./default-select.component.css']
})
export class DefaultSelectComponent implements OnInit {
  @Input() label:string = "";
  @Input() options: DeFaultItemSelect[] = []
  @Output() onValueChange:EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }
  onValueChangeFun(event:any){
    this.onValueChange.emit(event)
  }

}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CardItemComponent = void 0;
/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
var core_1 = require("@angular/core");
var screen_service_1 = require("src/app/Services/ApiServices/screen.service");
var locals_1 = require("src/app/Services/locals");
var CardItemComponent = /** @class */ (function () {
    function CardItemComponent(routs) {
        this.routs = routs;
        this.image_url = '';
        this.image_url_filtered = '';
        this.all_text = locals_1.ALL_TEXT;
        this.screen = screen_service_1.getScreenValue();
    }
    CardItemComponent.prototype.ngOnInit = function () {
        this.screen = screen_service_1.getScreenValue();
        this.image_url_filtered = this.image_url;
    };
    CardItemComponent.prototype.onGoDetail = function () {
        //console.log("cardData ===>", this.cardDetails)
        if (this.cardDetails && !this.cardDetails.offline) {
            this.routs.navigate([
                locals_1.ROUT_PATH.detailsId.replace(':id', this.cardDetails.id.toString()),
            ]);
        }
        else {
            //console.log((this.cardDetails.coderef+'-'+this.cardDetails.adress).replace(/\//g,'&'));
            this.routs.navigate([
                locals_1.ROUT_PATH.detailsId.replace(':id', (this.cardDetails.coderef + '-' + this.cardDetails.adress).replace(/\//g, '&')),
            ]);
        }
    };
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "image_url");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "name");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "prename");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "nis");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "fam_head");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "address");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "qrdata");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "barData");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "cardDetails");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "isMobile");
    __decorate([
        core_1.Input()
    ], CardItemComponent.prototype, "isConnected");
    CardItemComponent = __decorate([
        core_1.Component({
            selector: 'app-card-item',
            templateUrl: './card-item.component.html',
            styleUrls: ['./card-item.component.css']
        })
    ], CardItemComponent);
    return CardItemComponent;
}());
exports.CardItemComponent = CardItemComponent;

/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getScreenValue } from 'src/app/Services/ApiServices/screen.service';
import { ALL_TEXT, ROUT_PATH } from 'src/app/Services/locals';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.css'],
})
export class CardItemComponent implements OnInit {
  @Input() image_url?: string = '';
  @Input() name?: string;
  @Input() prename?: string;
  @Input() nis?: string;
  @Input() fam_head?: boolean;
  @Input() address?: string;
  @Input() qrdata?: string;
  @Input() barData?: string;
  @Input() cardDetails?: any;
  @Input() isMobile?: boolean;
  @Input() isConnected?: boolean;
  image_url_filtered = '';

  all_text: any = ALL_TEXT;
  screen = getScreenValue();
  constructor(public routs: Router) {}

  ngOnInit(): void {
    this.screen = getScreenValue();
    this.image_url_filtered = this.image_url;
  }

  onGoDetail() {
    //console.log("cardData ===>", this.cardDetails)
    if (this.cardDetails && !this.cardDetails.offline) {
      this.routs.navigate([
        ROUT_PATH.detailsId.replace(':id', this.cardDetails.id.toString()),
      ]);
    } else {
      //console.log((this.cardDetails.coderef+'-'+this.cardDetails.adress).replace(/\//g,'&'));
      this.routs.navigate([
        ROUT_PATH.detailsId.replace(':id', (this.cardDetails.coderef+'-'+this.cardDetails.adress).replace(/\//g,'&')),
      ]);
    }
  }
}

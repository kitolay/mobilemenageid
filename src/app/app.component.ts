import { EmitEventService, SYNC_SUCCESS } from './Services/emit-event.service';
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @angular-eslint/use-lifecycle-interface */
/* eslint-disable @typescript-eslint/naming-convention */
import { ToastrService } from 'ngx-toastr';
import { HostListener, Component, OnInit, SimpleChange } from '@angular/core';
import { getScreenSize } from './Services/ApiServices/screen.service';
import {
  getUserSessionEpired,
  getUserStorageJson,
} from './Services/ApiServices/storage.services';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ROUT_PATH } from 'src/app/Services/locals';
import { MyDatabaseService } from './Services/Database/database.service';
import { ConnectionService } from 'ng-connection-service';
import {
  syncDatas,
  testConnextion,
} from './Services/ApiServices/sync.services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  route_path = ROUT_PATH;
  user: any = { user_data: { user_name: '' } };
  dbservice: MyDatabaseService;
  isConnected = false;
  noInternetConnection: boolean;
  syncState = 0;

  title = 'MenageID';
  bpObs = 'L';
  isNotConected = getUserSessionEpired();

  constructor(
    public location: Location,
    public router: Router,
    public toaster: ToastrService,
    private connectionService: ConnectionService,
    private eventEmiter: EmitEventService
  ) {
    this.connectionService.monitor().subscribe((isConnected) => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.syncState = 2;
        syncDatas().then((result) => {
          if (result.statue) {
            this.syncState = 0;
            this.toaster.success(result.message);
            this.eventEmiter.eventEmitter.emit(SYNC_SUCCESS);
          } else {
            this.syncState = 0;
            this.toaster.error(result.message);
          }
        });
      } else {
        this.noInternetConnection = true;
        this.syncState = 1;
      }
    });
    router.events.subscribe((val) => {
      this.bpObs = getScreenSize();
      this.isNotConected = getUserSessionEpired();
    });
  }

  ngOnInit(): void {
    this.bpObs = getScreenSize();
    this.isNotConected = getUserSessionEpired();

    testConnextion().then((connex) => {
      if (connex.state) {
        this.isConnected = true;
        this.syncState = 2;
        syncDatas().then((result) => {
          if (result.statue) {
            this.syncState = 0;
            this.toaster.success(result.message);
          } else {
            this.syncState = 0;
            this.toaster.error(result.message);
          }
        });
      }else{
        this.isConnected = false;
        this.syncState = 1;
        console.log(connex.message);
      }
    });
  }

  ngOnChanges(changes: SimpleChange): void {
    const udata = getUserStorageJson();
    if (udata) {
      this.user = udata;
    } else {
      this.user = { user_data: { user_name: '' } };
    }
  }

  getStatueText(state: number){
    if(state === 0) {return 'Vous êtes en ligne';}
    if(state === 1) {return 'Vous êtes en mode "offline"';}
    if(state === 2) {return 'Synchronisation en cours ...';}
  }

  handleChangePath(path: string) {
    this.router.navigate([path]);
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.bpObs = getScreenSize();
    this.isNotConected = getUserSessionEpired();
  }
}

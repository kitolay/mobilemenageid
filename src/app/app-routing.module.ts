import { RecoverPageComponent } from './Components/recover-page/recover-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { FormulaireComponent } from './pages/formulaire/formulaire.component';
import { ROUT_PATH } from './Services/locals';
import { AuthguardService } from './Services/authguard.service';

// import { FooterComponent } from './Components/footer/footer.component';
import { ListesComponent } from './pages/listes/listes.component';
import { DetailsIdComponent } from './pages/details-id/details-id.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: ROUT_PATH.home },
  { path: ROUT_PATH.recover, component: RecoverPageComponent},
  // { path: 'footer', component: FooterComponent},
  { path: ROUT_PATH.login, component: LoginComponent },
  { path: ROUT_PATH.listes, component: ListesComponent, canActivate: [AuthguardService] },
  { path: ROUT_PATH.detailsId, component: DetailsIdComponent, canActivate: [AuthguardService] },
  { path: ROUT_PATH.formulaire, component: FormulaireComponent, canActivate: [AuthguardService]},
  { path: ROUT_PATH.home, component: HomeComponent, canActivate: [AuthguardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

/* eslint-disable @typescript-eslint/naming-convention */
import { BaseModel } from './common';
import { UserDataModel } from './DataModels';

export interface AhthentResponse extends BaseModel {
  data?: {
    user?: UserDataModel;
    tokken?: string;
  };
}

export interface CardData extends BaseModel {
  detailId: number;
  nom: string;
  prenom: string;
  nis: string;
  cdf: boolean;
  adress: string;
  pict: string;
  coderef: string;
  barref: string;
  offline?: boolean;
}

export interface CardDataResponse extends BaseModel {
  data: {
    page: number;
    lim: number;
    count: number;
    data: CardData[];
  };
}

export interface ProvinceData {
  id: number;
  refId: number;
  lib: string;
}

export interface RegionData extends ProvinceData {
  prov_id: number;
}

export interface DistrictData extends RegionData {
  reg_id: number;
}

export interface CommuneData extends DistrictData {
  dis_id: number;
}

export interface FokontanyData extends CommuneData {
  com_id: number;
}

export interface ErrorMessage {
  message?: string;
  query?: string;
}

export interface Id1Data {
  nom?: any;
  prenom?: any;
  surnom?: any;
  resstat?: any;
  sex?: any;
  age?: any;
  cpan?: any;
  cin?: { statue: any; value: any };
  enfu?: any;
  idenfu?: any;
  lienCDM?: any;
  statueM?: any;
  menageMere?: any;
  menageMereId?: any;
  orphan?: any;
  childMenage?: any;
  pregnante?: any;
}
export interface FormDataData {
  enqueteur_data: {
    name?: any;
    prename?: any;
    cin?: any;
    id?: any;
    sup_name?: any;
    sup_prename?: any;
    start_date?: Date;
  };
  ig_data: {
    lotis?: any;
    province?: any;
    region?: any;
    district?: any;
    commune?: any;
    fokontany?: any;
    milieu?: any;
    numero?: any;
  };
  id_data: {
    cdfm: boolean;
    pict?: any;
    nom?: any;
    prenom?: any;
    surnom?: any;
    cin?: any;
    datedelcin?: any;
    cincon?: any;
    datedelcincon?: any;
  };
  ch_data: {
    housetype?: any;
    extwall?: { au?: boolean; lib?: any };
    plaf?: { au?: boolean; lib?: any };
    npiech?: any;
    npiecedor?: any;
    piecsurf?: any;
    p1?: any;
    p2?: any;
    p3?: any;
    p4?: any;
    plancer?: { au?: boolean; lib?: any };
    elect?: { au?: boolean; lib?: any };
    eau?: { au?: boolean; lib?: any };
    toilet?: { au?: boolean; lib?: any };
  };
  ap_data: {
    'Chaise (avec pied et dossier)': { state: boolean; qte?: any };
    'Table (avec pied, bien droit)': { state: boolean; qte?: any };
    Natte: { state: boolean; qte?: any };
    'Lampe pétrole': { state: boolean; qte?: any };
    'Poste radio': { state: boolean; qte?: any };
    'Radio avec cassette, cd, mp3..': { state: boolean; qte?: any };
    Téléviseur: { state: boolean; qte?: any };
    'Lecteur CD DVD': { state: boolean; qte?: any };
    Bicyclette: { state: boolean; qte?: any };
    Puits: { state: boolean; qte?: any };
    Irrigation: { state: boolean; qte?: any };
    'Téléphone portable': { state: boolean; qte?: any };
    'Charrue (traction animale)': { state: boolean; qte?: any };
    'Charrette (traction animale)': { state: boolean; qte?: any };
    'Herse (traction animale)': { state: boolean; qte?: any };
    Bèche: { state: boolean; qte?: any };
    Stockage: { state: boolean; qte?: any };
    'Champ ou rizière (non métayage)': { state: boolean; qte?: any };
    Zébu: { state: boolean; qte?: any };
    Porc: { state: boolean; qte?: any };
    'Chèvre/mouton': { state: boolean; qte?: any };
    'Poulet gasy': { state: boolean; qte?: any };
    Rûche: { state: boolean; qte?: any };
    'Matériel de pêche: pirogue, filet': { state: boolean; qte?: any };
  };
  srpt_data: {
    sr: {
      'Salaire de la fonction publique'?: any;
      'Salaire du secteur privé'?: any;
      'Autre emploi'?: any;
      'Revenus agricoles'?: any;
      'Revenus d\'artisanat'?: any;
      'Revenus du petit commerce'?: any;
      'Transfert social'?: any;
      'Envoi de fonds'?: any;
      Don?: any;
    };
    qcmrar?: any;
    pt: {
      'Superficie possédée': any;
      'Superficie louée ou empruntée'?: any;
      'Superficie cultivée'?: any;
    };
    tc1: { type?: any; qte?: any };
    tc2: { type?: any; qte?: any };
    tc3: { type?: any; qte?: any };
  };
  da_data: {
    main: {
      Semences: { mantant?: any; fourniseur?: any };
      'Main d’œuvre (0 si main d\'œuvre gratuit)': {
        mantant?: any;
        fourniseur?: any;
      };
      Pesticides: { mantant?: any; fourniseur?: any };
      Fertilisants: { mantant?: any; fourniseur?: any };
      'Achat de matériel et équipements': { mantant?: any; fourniseur?: any };
      'Location de matériel et équipements': {
        mantant?: any;
        fourniseur?: any;
      };
    };
    autre: [{ name?: any; mantant?: any; fourniseur?: any }];
  };
  acaa_data: {
    aac: boolean;
    aacname: { AA02: any; AA03: any; AA04: any };
    afil: boolean;
    imf: boolean;
    imfname?: any;
    imfmontant?: any;
    imfprop?: any;
    notimf?: any;
    authercred?: any;
    commercant: boolean;
    autre?: any;
  };
  id1_data: Id1Data[];
  educres_data: {
    primary?: any;
    nombredanner?: any;
    goscool?: any;
    malagasy?: any;
    malagasyecrit?: any;
    sixmouth?: any;
    annee?: any;
  };
  ep_data: {
    principa_empl?: any;
  };
  hmc_data: {
    lunette?: any;
    audition?: any;
    walking?: any;
    brain?: any;
    selfcare?: any;
    maternallng?: any;
    caring?: any;
    chronicdeaseas?: any;
    chronicdeaseasauth?: any;
    diffic?: any;
  };
  validatiaon_data: {
    chef?: any;
    persone?: any;
    quality?: any;
  };
}

export interface CardFormDataDetail extends CardData {
  details: FormDataData;
}

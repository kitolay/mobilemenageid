/* eslint-disable @typescript-eslint/naming-convention */
import { CommonModel } from './common';

export interface SuperviseurData extends CommonModel {
  name?: string;
  prename?: string;
  email?: string;
  phone?: string;
}
export interface UserDataModel extends CommonModel {
  name?: number;
  prename?: number;
  user_name?: string;
  email?: string;
  phone?: string;
  cin?: string;
  superviseur?: SuperviseurData;
}

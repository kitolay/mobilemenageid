export interface BaseModel {
  id?: number;
  createdAt?: Date;
  updatedAt?: Date;
  error?: boolean;
  message?: string;
  valide?: boolean;
}
export interface CommonModel {
  id?: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface CodeDataInfo {
  expire: Date;
  mail: string;
  ref: string;
  user: string;
  code?: string;
}

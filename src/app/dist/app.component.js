"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppComponent = void 0;
var emit_event_service_1 = require("./Services/emit-event.service");
var core_1 = require("@angular/core");
var screen_service_1 = require("./Services/ApiServices/screen.service");
var storage_services_1 = require("./Services/ApiServices/storage.services");
var locals_1 = require("src/app/Services/locals");
var sync_services_1 = require("./Services/ApiServices/sync.services");
var AppComponent = /** @class */ (function () {
    function AppComponent(location, router, toaster, connectionService, eventEmiter) {
        var _this = this;
        this.location = location;
        this.router = router;
        this.toaster = toaster;
        this.connectionService = connectionService;
        this.eventEmiter = eventEmiter;
        this.route_path = locals_1.ROUT_PATH;
        this.user = { user_data: { user_name: '' } };
        this.isConnected = false;
        this.syncState = 0;
        this.title = 'MenageID';
        this.bpObs = 'L';
        this.isNotConected = storage_services_1.getUserSessionEpired();
        this.connectionService.monitor().subscribe(function (isConnected) {
            _this.isConnected = isConnected;
            if (_this.isConnected) {
                _this.syncState = 2;
                sync_services_1.syncDatas().then(function (result) {
                    if (result.statue) {
                        _this.syncState = 0;
                        _this.toaster.success(result.message);
                        _this.eventEmiter.eventEmitter.emit(emit_event_service_1.SYNC_SUCCESS);
                    }
                    else {
                        _this.syncState = 0;
                        _this.toaster.error(result.message);
                    }
                });
            }
            else {
                _this.noInternetConnection = true;
                _this.syncState = 1;
            }
        });
        router.events.subscribe(function (val) {
            _this.bpObs = screen_service_1.getScreenSize();
            _this.isNotConected = storage_services_1.getUserSessionEpired();
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.bpObs = screen_service_1.getScreenSize();
        this.isNotConected = storage_services_1.getUserSessionEpired();
        sync_services_1.testConnextion().then(function (connex) {
            if (connex.state) {
                _this.isConnected = true;
                _this.syncState = 2;
                sync_services_1.syncDatas().then(function (result) {
                    if (result.statue) {
                        _this.syncState = 0;
                        _this.toaster.success(result.message);
                    }
                    else {
                        _this.syncState = 0;
                        _this.toaster.error(result.message);
                    }
                });
            }
            else {
                _this.isConnected = false;
                _this.syncState = 1;
                console.log(connex.message);
            }
        });
    };
    AppComponent.prototype.ngOnChanges = function (changes) {
        var udata = storage_services_1.getUserStorageJson();
        if (udata) {
            this.user = udata;
        }
        else {
            this.user = { user_data: { user_name: '' } };
        }
    };
    AppComponent.prototype.getStatueText = function (state) {
        if (state === 0) {
            return 'Vous êtes en ligne';
        }
        if (state === 1) {
            return 'Vous êtes en mode "offline"';
        }
        if (state === 2) {
            return 'Synchronisation en cours ...';
        }
    };
    AppComponent.prototype.handleChangePath = function (path) {
        this.router.navigate([path]);
    };
    AppComponent.prototype.onResize = function () {
        this.bpObs = screen_service_1.getScreenSize();
        this.isNotConected = storage_services_1.getUserSessionEpired();
    };
    __decorate([
        core_1.HostListener('window:resize', ['$event'])
    ], AppComponent.prototype, "onResize");
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;

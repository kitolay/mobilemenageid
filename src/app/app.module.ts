import { NgModule } from '@angular/core';
import {ConnectionServiceModule} from 'ng-connection-service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { IonicModule } from '@ionic/angular';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { MaterialModule } from './material/material.module';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { DefaultButtonComponent } from './Components/default-button/default-button.component';
import { FormulaireComponent } from './pages/formulaire/formulaire.component';
import { IdEnqueteurComponent } from './pages/formulaire/id-enqueteur/id-enqueteur.component';
import { ValidationComponent } from './pages/formulaire/validation/validation.component';
import { NguCarouselModule } from '@ngu/carousel';
import { QrCodeModule } from 'ng-qrcode';
import { IgComponent } from './pages/formulaire/idMenage/ig/ig.component';
import { IdComponent } from './pages/formulaire/idMenage/id/id.component';
import { ChComponent } from './pages/formulaire/idMenage/ch/ch.component';
import { ApComponent } from './pages/formulaire/idMenage/ap/ap.component';
import { SrptComponent } from './pages/formulaire/idMenage/srpt/srpt.component';
import { DaComponent } from './pages/formulaire/idMenage/da/da.component';
import { AcaaComponent } from './pages/formulaire/idMenage/acaa/acaa.component';
import { Id1Component } from './pages/formulaire/idMenage/id1/id1.component';
import { EducresComponent } from './pages/formulaire/idMenage/educres/educres.component';
import { EpComponent } from './pages/formulaire/idMenage/ep/ep.component';
import { HmcComponent } from './pages/formulaire/idMenage/hmc/hmc.component';
import { NgxEchartsModule } from 'ngx-echarts';
import {
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
} from '@danielmoncada/angular-datetime-picker';
import { NgxBarcodeModule } from 'ngx-barcode';
import { DefaultAutocompletComponent } from './Components/default-autocomplet/default-autocomplet.component';
import {
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule,
} from '@angular-material-components/datetime-picker';
import { StepsSelectorComponent } from './Components/steps-selector/steps-selector.component';
import { DefaultSelectComponent } from './Components/default-select/default-select.component';
import { CardItemComponent } from './Components/card-item/card-item.component';
import { RecoverPageComponent } from './Components/recover-page/recover-page.component';
import { DetailsIdComponent } from './pages/details-id/details-id.component';
import { HeaderComponent } from './Components/header/header.component';
import { ListesComponent } from './pages/listes/listes.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { MatPaginatorIntlCro } from './Custom/Config/Custom.pagination.config';
import { MobileTabComponent } from './Components/mobile-tab/mobile-tab.component';
import { FooterComponent } from './Components/footer/footer.component';
import { SQLite } from '@ionic-native/sqlite/ngx';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DefaultButtonComponent,
    FormulaireComponent,
    IdEnqueteurComponent,
    ValidationComponent,
    IgComponent,
    IdComponent,
    ChComponent,
    ApComponent,
    SrptComponent,
    DaComponent,
    AcaaComponent,
    Id1Component,
    EducresComponent,
    EpComponent,
    HmcComponent,
    DefaultAutocompletComponent,
    StepsSelectorComponent,
    DefaultSelectComponent,
    CardItemComponent,
    RecoverPageComponent,
    DetailsIdComponent,
    HeaderComponent,
    ListesComponent,
    MobileTabComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    MatDatepickerModule,
    NguCarouselModule,
    QrCodeModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxMatNativeDateModule,
    MatNativeDateModule,
    NgxBarcodeModule,
    ToastrModule.forRoot(),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxUsefulSwiperModule,
    ConnectionServiceModule,
    IonicModule.forRoot(),
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    })
  ],
  providers: [MatDatepickerModule, MatNativeDateModule,{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro},SQLite],
  bootstrap: [AppComponent],
})
export class AppModule {}
